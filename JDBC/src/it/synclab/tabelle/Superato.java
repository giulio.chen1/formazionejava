package it.synclab.tabelle;

public class Superato {
	private int idStudente;
	private String idCorso;
	
	public Superato(int idStudente, String idCorso) {
		this.idStudente = idStudente;
		this.idCorso = idCorso;
	}

	public int getIdStudente() {
		return idStudente;
	}

	public void setIdStudente(int idStudente) {
		this.idStudente = idStudente;
	}

	public String getIdCorso() {
		return idCorso;
	}

	public void setIdCorso(String idCorso) {
		this.idCorso = idCorso;
	}
	
	@Override
	public String toString() {
		return "idDocente: " + this.getIdStudente() + ", idCorso: " + this.getIdCorso();
	}
	
}
