package it.synclab.tabelle;

public class Studente {
	private int matricola, annoIscr;
	private String nome, cognome, idFacolta;
	
	public Studente(int matricola, String nome, String cognome, String idFacolta, int annoIscr) {
		this.matricola = matricola;
		this.cognome = cognome;
		this.nome = nome;
		this.idFacolta = idFacolta;
		this.annoIscr = annoIscr;
	}

	public int getMatricola() {
		return matricola;
	}

	public void setMatricola(int matricola) {
		this.matricola = matricola;
	}

	public int getAnnoIscr() {
		return annoIscr;
	}

	public void setAnnoIscr(int annoIscr) {
		this.annoIscr = annoIscr;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getIdFacolta() {
		return idFacolta;
	}

	public void setIdFacolta(String idFacolta) {
		this.idFacolta = idFacolta;
	}
	
	@Override
	public String toString() {
		return "Matricola: " + this.getMatricola() + ", nome: " + this.getNome() + ", cognome: " + this.getCognome() + ", idFacolt�: " 
				+ this.getIdFacolta() + ", anno iscrizione: " + this.getAnnoIscr(); 
	}
	
}
