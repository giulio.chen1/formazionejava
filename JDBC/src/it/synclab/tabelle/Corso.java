package it.synclab.tabelle;

public class Corso {
	private String idCorso, nome;
	
	public Corso(String idCorso, String nome) {
		this.idCorso = idCorso;
		this.nome = nome;
	}

	public String getIdCorso() {
		return idCorso;
	}

	public void setIdCorso(String idCorso) {
		this.idCorso = idCorso;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "idCorso: " + this.getIdCorso() + ", nome: " + this.getNome();
	}
}
