package it.synclab.tabelle;

public class Insegna {
	private int idDocente;
	private String idCorso;
	
	public Insegna(int idDocente, String idCorso) {
		this.idDocente = idDocente;
		this.idCorso = idCorso;
	}

	public int getIdDocente() {
		return idDocente;
	}

	public void setIdDocente(int idDocente) {
		this.idDocente = idDocente;
	}

	public String getIdCorso() {
		return idCorso;
	}

	public void setIdCorso(String idCorso) {
		this.idCorso = idCorso;
	}
	
	@Override
	public String toString() {
		return "idDocente: " + this.getIdDocente() + ", idCorso: " + this.getIdCorso();
	}
}
