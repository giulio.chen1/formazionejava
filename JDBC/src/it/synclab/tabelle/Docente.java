package it.synclab.tabelle;

public class Docente {
	private int idDocente;
	private String nome, cognome;
	private double stipendio;
	
	public Docente(int idDocente, String nome, String cognome, double stipendio) {
		this.idDocente = idDocente;
		this.nome = nome;
		this.cognome = cognome;
		this.stipendio = stipendio;
	}

	public int getIdDocente() {
		return idDocente;
	}

	public void setIdDocente(int idDocente) {
		this.idDocente = idDocente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public double getStipendio() {
		return stipendio;
	}

	public void setStipendio(double stipendio) {
		this.stipendio = stipendio;
	}
	
	@Override
	public String toString() {
		return "idDocente: " + this.getIdDocente() + ", nome: " + this.getNome() + ", cognome: " + this.getCognome() + ", stipendio: " + this.getStipendio();
	}
}
