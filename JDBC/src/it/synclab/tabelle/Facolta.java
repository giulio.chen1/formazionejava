package it.synclab.tabelle;

public class Facolta {
	private String idFacolta, nome;
	
	public Facolta(String idFacolta, String nome) {
		this.idFacolta = idFacolta;
		this.nome = nome;
	}

	public String getIdFacolta() {
		return idFacolta;
	}

	public void setIdFacolta(String idFacolta) {
		this.idFacolta = idFacolta;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return "idFacolta: " + this.getIdFacolta() + ", nome: " + this.getNome();
	}
}
