package it.synclab.tabelle;

public class Comprende {
	private String idFacolta, idCorso;
	
	public Comprende(String idFacolta, String idCorso) {
		this.idFacolta = idFacolta;
		this.idCorso = idCorso;
	}

	public String getIdFacolta() {
		return idFacolta;
	}

	public void setIdFacolta(String idFacolta) {
		this.idFacolta = idFacolta;
	}

	public String getIdCorso() {
		return idCorso;
	}

	public void setIdCorso(String idCorso) {
		this.idCorso = idCorso;
	}
	
	@Override
	public String toString() {
		return "idFacolta: " + this.getIdFacolta() + ", idCorso: " + this.getIdCorso();
	}
}
