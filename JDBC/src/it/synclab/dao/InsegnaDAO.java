package it.synclab.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import it.synclab.interfacce.Crud;
import it.synclab.tabelle.Insegna;
import it.synclab.test.ConnessioneDB;

public class InsegnaDAO implements Crud{
	
	private static String query;
	@Override
	public void create(Object insegna) throws Exception{
		
		Insegna ins = (Insegna) insegna;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			 
			query = "insert into Insegna(idDocente, idCorso) values('" + ins.getIdDocente() +"','"+ ins.getIdCorso() +"') ";
			
			//execute query  
			stmt.executeQuery(query); 
			  
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}

	@Override
	public void getAll() throws Exception{
		
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			  
			//execute query  
			ResultSet rs = stmt.executeQuery("select * from Insegna order by idDocente");  
			while(rs.next())  
			System.out.println(rs.getString(1)+"  "+rs.getString(2));  
			
			//close the connection object  
			connection.close();  
			
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}
	}

	@Override
	public void get(Object insegna) throws Exception{
		
		Insegna ins = (Insegna) insegna;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			
			//execute query
			ResultSet rs = stmt.executeQuery("select * from Insegna where idDocente = '"+ ins.getIdDocente() +"' AND idCorso = '" + ins.getIdCorso() + "'");  

			while(rs.next())  
			System.out.println(rs.getString(1)+"  "+rs.getString(2));  
			
			//close the connection object  
			connection.close();  
			
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}
		
	}

	@Override
	public void update(Object insegna, Object colonna, Object valore) throws Exception{
		
		Insegna ins = (Insegna) insegna;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			//execute query  
			query = "UPDATE Insegna SET " + colonna + " = '" + valore + "' WHERE idDocente = '" + ins.getIdDocente() + "' AND idCorso = '" + ins.getIdCorso() +"'";
			stmt.executeQuery(query); 
			
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}
	
	@Override
	public void delete(Object insegna) throws Exception{
		
		Insegna ins = (Insegna) insegna;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			  
			//execute query  
			stmt.executeQuery("delete from Insegna where idDocente = '" + ins.getIdDocente() + "' AND idCorso = '" + ins.getIdCorso() +"'"); 
			  
			
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}
}
