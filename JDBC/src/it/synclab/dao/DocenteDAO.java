package it.synclab.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import it.synclab.interfacce.*;
import it.synclab.tabelle.Docente;
import it.synclab.test.ConnessioneDB;

public class DocenteDAO implements Crud {
	
	private static String query;
	@Override
	public void create(Object docente) throws Exception{
		
		Docente doc = (Docente) docente;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			 
			query = "insert into docente(iddocente, nome, cognome, stipendio) "
					+ "values(" + doc.getIdDocente() +",'"+ doc.getNome() +"','"+ doc.getCognome()
					+"', "+ doc.getStipendio() +") ";
			
			//execute query  
			stmt.executeQuery(query); 
			  
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}

	@Override
	public void getAll() throws Exception{
		
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			  
			//execute query  
			ResultSet rs = stmt.executeQuery("select * from docente order by idDocente");  
			while(rs.next())  
			System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3)+"  "+rs.getString(4));  
			
			//close the connection object  
			connection.close();  
			
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}
	}

	@Override
	public void get(Object id) throws Exception{
		
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			  
			//execute query  
			ResultSet rs = stmt.executeQuery("select * from Docente where idDocente = '"+ id +"'");  
			while(rs.next())  
			System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3)+"  "+rs.getString(4));  
			
			//close the connection object  
			connection.close();  
			
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}
		
	}

	@Override
	public void update(Object id, Object colonna, Object valore) throws Exception{
		
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			//execute query  
			query = "UPDATE Docente SET " + colonna + " = '" + valore + "' WHERE idDocente = " + id;
			stmt.executeQuery(query); 
			  
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}

	@Override
	public void delete(Object id) throws Exception{
		
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			  
			//execute query  
			stmt.executeQuery("delete from docente where idDocente = '" + id + "'"); 
			  
			
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}
}
