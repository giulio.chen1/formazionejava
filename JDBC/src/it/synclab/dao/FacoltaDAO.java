package it.synclab.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import it.synclab.interfacce.Crud;
import it.synclab.tabelle.Facolta;
import it.synclab.test.ConnessioneDB;

public class FacoltaDAO implements Crud{
	
	private static String query;
	@Override
	public void create(Object facolta) throws Exception{
		
		Facolta fac = (Facolta) facolta;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			 
			query = "insert into Facolta(idFacolta, nome) values('" + fac.getIdFacolta() +"','"+ fac.getNome() +"') ";
			
			//execute query  
			stmt.executeQuery(query); 
			  
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}

	@Override
	public void getAll() throws Exception{
		
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			  
			//execute query  
			ResultSet rs = stmt.executeQuery("select * from Facolta order by idFacolta");  
			while(rs.next())  
			System.out.println(rs.getString(1)+"  "+rs.getString(2));  
			
			//close the connection object  
			connection.close();  
			
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}
	}

	@Override
	public void get(Object id) throws Exception{
		
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			  
			//execute query  
			ResultSet rs = stmt.executeQuery("select * from Facolta where idFacolta = '"+ id +"'");  
			while(rs.next())  
			System.out.println(rs.getString(1)+"  "+rs.getString(2));  
			
			//close the connection object  
			connection.close();  
			
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}
		
	}

	@Override
	public void update(Object id, Object colonna, Object valore) throws Exception{
		
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			//execute query  
			query = "UPDATE Facolta SET " + colonna + " = '" + valore + "' WHERE idFacolta = '" + id + "'";
			stmt.executeQuery(query); 
			  
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}

	@Override
	public void delete(Object id) throws Exception{
		
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			  
			//execute query  
			stmt.executeQuery("delete from Facolta where idFacolta = '" + id + "'"); 
			  
			
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}
}
