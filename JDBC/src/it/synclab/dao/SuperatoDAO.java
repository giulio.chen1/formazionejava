package it.synclab.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import it.synclab.interfacce.Crud;
import it.synclab.tabelle.Superato;
import it.synclab.test.ConnessioneDB;

public class SuperatoDAO implements Crud{
	
	private static String query;
	@Override
	public void create(Object superato) throws Exception{
		
		Superato sup = (Superato) superato;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			 
			query = "insert into Superato(idStudente, idCorso) values('" + sup.getIdStudente() +"','"+ sup.getIdCorso() +"') ";
			
			//execute query  
			stmt.executeQuery(query); 
			  
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}

	@Override
	public void getAll() throws Exception{
		
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			  
			//execute query  
			ResultSet rs = stmt.executeQuery("select * from Superato order by idStudente");  
			while(rs.next())  
			System.out.println(rs.getString(1)+"  "+rs.getString(2));  
			
			//close the connection object  
			connection.close();  
			
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}
	}

	@Override
	public void get(Object superato) throws Exception{
		
		Superato sup = (Superato) superato;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			
			//execute query
			ResultSet rs = stmt.executeQuery("select * from Superato where idStudente = '"+ sup.getIdStudente() +"' AND idCorso = '" + sup.getIdCorso() + "'");  
			int colCount = rs.getMetaData().getColumnCount();
			while(rs.next())  
			System.out.println(rs.getString(1)+"  "+rs.getString(2));  
			
			//close the connection object  
			connection.close();  
			
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}
		
	}

	@Override
	public void update(Object superato, Object colonna, Object valore) throws Exception{
		
		Superato sup = (Superato) superato;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			//execute query  
			query = "UPDATE Superato SET " + colonna + " = '" + valore + "' WHERE idStudente = '" + sup.getIdStudente() + "' AND idCorso = '" + sup.getIdCorso() +"'";
			stmt.executeQuery(query); 
			
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}
	
	@Override
	public void delete(Object superato) throws Exception{
		
		Superato sup = (Superato) superato;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			  
			//execute query  
			stmt.executeQuery("delete from Superato where idStudente = '" + sup.getIdStudente() + "' AND idCorso = '" + sup.getIdCorso() +"'"); 
			  
			
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}
}
