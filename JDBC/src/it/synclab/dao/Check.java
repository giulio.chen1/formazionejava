package it.synclab.dao;
	
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import it.synclab.tabelle.*;
import it.synclab.test.ConnessioneDB;

public class Check {
	
	private int count;
	
	
	public int getCount() {
		return count;
	}


	public void setCount(int count) {
		this.count = count;
	}

	public void checkCount(String tab) {
		try {
			Connection connection = ConnessioneDB.getConnection();
			
			Statement stmt = connection.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM " + tab);
			while(rs.next())
				this.setCount(rs.getInt(1));
			connection.close();
			
		} catch (Exception e) {
		}
	}
	
	public void checkCountAppoggio(Object tabella) {
		
		String nomeTab = String.valueOf(tabella.getClass()).substring(25, 30);
		if(nomeTab.equals("Comprende")) {
			try {
				Connection connection = ConnessioneDB.getConnection();
				
				Statement stmt = connection.createStatement();
				
				ResultSet rs = stmt.executeQuery("SELECT COUNT(idFacolta) FROM (SELECT idFacolta, COUNT(idCorso) FROM Comprende group by idFacolta)");
				while(rs.next())
					this.setCount(rs.getInt(1));
				connection.close();
				
			} catch (Exception e) {
			}
		}else if(nomeTab.equals("Superato")) {
			try {
				Connection connection = ConnessioneDB.getConnection();
				
				Statement stmt = connection.createStatement();
				
				ResultSet rs = stmt.executeQuery("SELECT COUNT(idStudente) FROM (SELECT idStudente, COUNT(idCorso) FROM Superato group by idStudente)");
				while(rs.next())
					this.setCount(rs.getInt(1));
				connection.close();
				
			} catch (Exception e) {
			}
		}else if(nomeTab.equals("Insegna")) {
			try {
				Connection connection = ConnessioneDB.getConnection();
				
				Statement stmt = connection.createStatement();
				
				ResultSet rs = stmt.executeQuery("SELECT COUNT(idDocente) FROM (SELECT idDocente, COUNT(idCorso) FROM Insegna group by idDocente)");
				while(rs.next())
					this.setCount(rs.getInt(1));
				connection.close();
				
			} catch (Exception e) {
			}
		}
			
		
	}
}
