package it.synclab.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import it.synclab.interfacce.Crud;
import it.synclab.tabelle.Comprende;
import it.synclab.test.ConnessioneDB;

public class ComprendeDAO implements Crud{
	
	private static String query;
	@Override
	public void create(Object comprende) throws Exception{
		
		Comprende comp = (Comprende) comprende;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			 
			query = "insert into Comprende(idFacolta, idCorso) values('" + comp.getIdFacolta() +"','"+ comp.getIdCorso() +"') ";
			
			//execute query  
			stmt.executeQuery(query); 
			  
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}

	@Override
	public void getAll() throws Exception{
		
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			  
			//execute query  
			ResultSet rs = stmt.executeQuery("select * from Comprende order by idFacolta");  
			while(rs.next())  
			System.out.println(rs.getString(1)+"  "+rs.getString(2));  
			
			//close the connection object  
			connection.close();  
			
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}
	}

	@Override
	public void get(Object comprende) throws Exception{
		
		Comprende comp = (Comprende) comprende;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			
			//execute query
			ResultSet rs = stmt.executeQuery("select * from Comprende where idFacolta = '"+ comp.getIdFacolta() +"' AND idCorso = '" + comp.getIdCorso() + "'");  
	
			while(rs.next())  
			System.out.println(rs.getString(1)+"  "+rs.getString(2));  
				
			//close the connection object  
			connection.close();  
			
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}
		
	}

	@Override
	public void update(Object comprende, Object colonna, Object valore) throws Exception{
		
		Comprende comp = (Comprende) comprende;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			//execute query  
			query = "UPDATE Comprende SET " + colonna + " = '" + valore + "' WHERE idFacolta = '" + comp.getIdFacolta() + "' AND idCorso = '" + comp.getIdCorso() +"'";
			stmt.executeQuery(query); 
			
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}
	
	@Override
	public void delete(Object comprende) throws Exception{
		
		Comprende comp = (Comprende) comprende;
		try{  
			Connection connection = ConnessioneDB.getConnection();
			//create the statement object  
			Statement stmt = connection.createStatement();  
			  
			//execute query  
			stmt.executeQuery("delete from Comprende where idFacolta = '" + comp.getIdFacolta() + "' AND idCorso = '" + comp.getIdCorso() +"'"); 
			  
			
			//close the connection object  
			connection.close();  
			System.out.println("Operazione avvenuta con successo");
			  
		}catch(Exception e){
			System.out.println(e);
			throw new Exception();
		}  
		
	}
}
