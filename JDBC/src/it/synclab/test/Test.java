package it.synclab.test;

import it.synclab.dao.*;
import it.synclab.tabelle.*;
import java.util.Scanner;

public class Test {
	public static void main(String args[]){  
		Scanner scanner = new Scanner(System.in);
		
		boolean continua = true;
		try {
			while(continua) {
				System.out.println("Operazioni CRUD su: \n1 - Studente\n2 - Corso\n3 - Facolta\n4 - Docente\n5 - Comprende (Facolta comprende Corso)"
						+ "\n6 - Superato (Studente supera Corso)\n7 - Insegna (Docente insegna Corso)");	
				String input = scanner.nextLine().trim();
				String input2, input3;
				switch(input) {
					case "1":
						StudenteDAO stDao = new StudenteDAO();
						System.out.println("Effettuare: \n1 - Create\n2 - Read specifico\n3 - Read All\n4 - Update\n5 - Delete");
						input2 = scanner.nextLine().trim();
						switch(input2) {
							case "1": System.out.println("Inserire matricola, nome, cognome, idFacolta, anno iscrizione");
								input3 = scanner.nextLine().trim();
								String[] parametri = input3.split("\\s+");
								//Input check
								if(parametri.length != 5) throw new NumberFormatException();
								int testInput = Integer.valueOf(parametri[0]); //Se non sono interi, viene lanciata un'eccezione
								int testInput2 = Integer.valueOf(parametri[4]);
								Studente studente = new Studente(testInput, parametri[1], parametri[2], parametri[3], testInput2);
								stDao.create(studente);
								break;
							case "2": System.out.println("Inserire id dello studente");
								input3 = scanner.nextLine().trim();
								testInput = Integer.valueOf(input3); //Se non � un intero, viene lanciata un'eccezione
								stDao.get(input3);
								break;
							case "3": stDao.getAll();
								break;
							case "4": System.out.println("Inserire id, colonna e nuovo valore");
								input3 = scanner.nextLine().trim();
								String[] parametri2 = input3.split("\\s+");
								//Input check
								if(parametri2.length != 3) throw new NumberFormatException();
								testInput = Integer.valueOf(parametri2[0]); //Se non � un intero, viene lanciata un'eccezione
								stDao.update(parametri2[0], parametri2[1], parametri2[2]);
								break;
							case "5": System.out.println("Inserire id dello studente da rimuovere");
								input3 = scanner.nextLine().trim();
								testInput = Integer.valueOf(input3); //Se non � un intero, viene lanciata un'eccezione
								stDao.delete(input3);
								break;
							default: System.out.println("Input errato");
								break;
						}
						break;
					case "2":
						CorsoDAO corDao = new CorsoDAO();
						System.out.println("Effettuare: \n1 - Create\n2 - Read specifico\n3 - Read All\n4 - Update\n5 - Delete");
						input2 = scanner.nextLine().trim();
						switch(input2) {
							case "1": System.out.println("Inserire idCorso, nome");
								input3 = scanner.nextLine().trim();
								String[] parametri = input3.split("\\s+");
								//Input check
								if(parametri.length != 2) throw new NumberFormatException();
								Corso corso = new Corso(parametri[0], parametri[1]);
								corDao.create(corso);
								break;
							case "2": System.out.println("Inserire id del corso");
								input3 = scanner.nextLine().trim();
								corDao.get(input3);
								break;
							case "3": corDao.getAll();
								break;
							case "4": System.out.println("Inserire id, colonna e nuovo valore");
								input3 = scanner.nextLine().trim();
								String[] parametri2 = input3.split("\\s+");
								//Input check
								if(parametri2.length != 3) throw new NumberFormatException();
								corDao.update(parametri2[0], parametri2[1], parametri2[2]);
								break;
							case "5": System.out.println("Inserire id del corso da rimuovere");
								input3 = scanner.nextLine().trim();
								corDao.delete(input3);
								break;
							default: System.out.println("Input errato");
								break;
						}
						break; 
					case "3":
						FacoltaDAO facDao = new FacoltaDAO();
						System.out.println("Effettuare: \n1 - Create\n2 - Read specifico\n3 - Read All\n4 - Update\n5 - Delete");
						input2 = scanner.nextLine().trim();
						switch(input2) {
							case "1": System.out.println("Inserire idFacolta, nome");
								input3 = scanner.nextLine().trim();
								String[] parametri = input3.split("\\s+");
								//Input check
								if(parametri.length != 2) throw new NumberFormatException();
								Facolta fac = new Facolta(parametri[0], parametri[1]);
								facDao.create(fac);
								break;
							case "2": System.out.println("Inserire id della facolt�");
								input3 = scanner.nextLine().trim();
								facDao.get(input3);
								break;
							case "3": facDao.getAll();
								break;
							case "4": System.out.println("Inserire id, colonna e nuovo valore");
								input3 = scanner.nextLine().trim();
								String[] parametri2 = input3.split("\\s+");
								//Input check
								if(parametri2.length != 3) throw new NumberFormatException();
								facDao.update(parametri2[0], parametri2[1], parametri2[2]);
								break;
							case "5": System.out.println("Inserire id della facolt� da rimuovere");
								input3 = scanner.nextLine().trim();
								facDao.delete(input3);
								break;
							default: System.out.println("Input errato");
								break;
						}
						break;
					case "4":
						DocenteDAO docDao = new DocenteDAO();
						System.out.println("Effettuare: \n1 - Create\n2 - Read specifico\n3 - Read All\n4 - Update\n5 - Delete");
						input2 = scanner.nextLine().trim();
						switch(input2) {
							case "1": System.out.println("Inserire idDocente, nome, cognome, stipendio");
								input3 = scanner.nextLine().trim();
								String[] parametri = input3.split("\\s+");
								//Input check
								if(parametri.length != 4) throw new NumberFormatException();
								int testInput = Integer.valueOf(parametri[0]); //Se non sono interi, viene lanciata un'eccezione
								int testInput2 = Integer.valueOf(parametri[4]);
								Docente doc = new Docente(testInput, parametri[1], parametri[2], testInput2);
								docDao.create(doc);
								break;
							case "2": System.out.println("Inserire id del docente");
								input3 = scanner.nextLine().trim();
								testInput = Integer.valueOf(input3); //Se non � un intero, viene lanciata un'eccezione
								docDao.get(input3);
								break;
							case "3": docDao.getAll();
								break;
							case "4": System.out.println("Inserire id, colonna e nuovo valore");
								input3 = scanner.nextLine().trim();
								String[] parametri2 = input3.split("\\s+");
								//Input check
								if(parametri2.length != 3) throw new NumberFormatException();
								testInput = Integer.valueOf(parametri2[0]); //Se non � un intero, viene lanciata un'eccezione
								docDao.update(parametri2[0], parametri2[1], parametri2[2]);
								break;
							case "5": System.out.println("Inserire id del docente da rimuovere");
								input3 = scanner.nextLine().trim();
								testInput = Integer.valueOf(input3); //Se non � un intero, viene lanciata un'eccezione
								docDao.delete(input3);
								break;
							default: System.out.println("Input errato");
								break;
						}
						break;
					case "5":
						ComprendeDAO compDao = new ComprendeDAO();
						System.out.println("Effettuare: \n1 - Create\n2 - Read specifico\n3 - Read All\n4 - Update\n5 - Delete");
						input2 = scanner.nextLine().trim();
						switch(input2) {
							case "1": System.out.println("Inserire idFacolta, idCorso");
								input3 = scanner.nextLine().trim();
								String[] parametri = input3.split("\\s+");
								//Input check
								if(parametri.length != 2) throw new NumberFormatException();
								Comprende comp = new Comprende(parametri[0], parametri[1]);
								compDao.create(comp);
								break;
							case "2": System.out.println("Inserire idFacolta, idCorso");
								input3 = scanner.nextLine().trim();
								parametri = input3.split("\\s+");
								//Input check
								if(parametri.length != 2) throw new NumberFormatException();
								Comprende comp2 = new Comprende(parametri[0], parametri[1]);
								compDao.get(comp2);
								break;
							case "3": compDao.getAll();
								break;
							case "4": System.out.println("Inserire idFacolta, idCorso, colonna da modificare e nuovo valore");
								input3 = scanner.nextLine().trim();
								String[] parametri2 = input3.split("\\s+");
								//Input check
								if(parametri2.length != 4) throw new NumberFormatException();
								Comprende comp3 = new Comprende(parametri2[0], parametri2[1]);
								compDao.update(comp3, parametri2[2], parametri2[3]);
								break;
							case "5": System.out.println("Inserire idFacolt� e idCorso da rimuovere");
								input3 = scanner.nextLine().trim();
								parametri = input3.split("\\s+");
								//Input check
								if(parametri.length != 2) throw new NumberFormatException();
								Comprende comp4 = new Comprende(parametri[0], parametri[1]);
								compDao.delete(comp4);
								break;
							default: System.out.println("Input errato");
								break;
						}
						break;
					case "6":
						SuperatoDAO supDao = new SuperatoDAO();
						System.out.println("Effettuare: \n1 - Create\n2 - Read specifico\n3 - Read All\n4 - Update\n5 - Delete");
						input2 = scanner.nextLine().trim();
						switch(input2) {
							case "1": System.out.println("Inserire idStudente, idCorso");
								input3 = scanner.nextLine().trim();
								String[] parametri = input3.split("\\s+");
								//Input check
								int testInput = Integer.valueOf(parametri[0]);
								if(parametri.length != 2) throw new NumberFormatException();
								Superato sup = new Superato(testInput, parametri[1]);
								supDao.create(sup);
								break;
							case "2": System.out.println("Inserire idStudente, idCorso");
								input3 = scanner.nextLine().trim();
								parametri = input3.split("\\s+");
								//Input check
								testInput = Integer.valueOf(parametri[0]);
								if(parametri.length != 2) throw new NumberFormatException();
								Superato sup2 = new Superato(testInput, parametri[1]);
								supDao.get(sup2);
								break;
							case "3": supDao.getAll();
								break;
							case "4": System.out.println("Inserire idStudente, idCorso, colonna da modificare e nuovo valore");
								input3 = scanner.nextLine().trim();
								String[] parametri2 = input3.split("\\s+");
								//Input check
								testInput = Integer.valueOf(parametri2[0]);
								if(parametri2.length != 4) throw new NumberFormatException();
								Superato sup3 = new Superato(testInput, parametri2[1]);
								supDao.update(sup3, parametri2[2], parametri2[3]);
								break;
							case "5": System.out.println("Inserire idStudente e idCorso da rimuovere");
								input3 = scanner.nextLine().trim();
								parametri = input3.split("\\s+");
								//Input check
								testInput = Integer.valueOf(parametri[0]);
								if(parametri.length != 2) throw new NumberFormatException();
								Superato sup4 = new Superato(testInput, parametri[1]);
								supDao.delete(sup4);
								break;
							default: System.out.println("Input errato");
								break;
						}
						break;
					case "7":
						InsegnaDAO insDao = new InsegnaDAO();
						System.out.println("Effettuare: \n1 - Create\n2 - Read specifico\n3 - Read All\n4 - Update\n5 - Delete");
						input2 = scanner.nextLine().trim();
						switch(input2) {
							case "1": System.out.println("Inserire idDocente, idCorso");
								input3 = scanner.nextLine().trim();
								String[] parametri = input3.split("\\s+");
								//Input check
								int testInput = Integer.valueOf(parametri[0]);
								if(parametri.length != 2) throw new NumberFormatException();
								Insegna ins = new Insegna(testInput, parametri[1]);
								insDao.create(ins);
								break;
							case "2": System.out.println("Inserire idDocente, idCorso");
								input3 = scanner.nextLine().trim();
								parametri = input3.split("\\s+");
								//Input check
								testInput = Integer.valueOf(parametri[0]);
								if(parametri.length != 2) throw new NumberFormatException();
								Insegna ins2 = new Insegna(testInput, parametri[1]);
								insDao.get(ins2);
								break;
							case "3": insDao.getAll();
								break;
							case "4": System.out.println("Inserire idDocente, idCorso, colonna da modificare e nuovo valore");
								input3 = scanner.nextLine().trim();
								String[] parametri2 = input3.split("\\s+");
								//Input check
								testInput = Integer.valueOf(parametri2[0]);
								if(parametri2.length != 4) throw new NumberFormatException();
								Insegna ins3 = new Insegna(testInput, parametri2[1]);
								insDao.update(ins3, parametri2[2], parametri2[3]);
								break;
							case "5": System.out.println("Inserire idDocente e idCorso da rimuovere");
								input3 = scanner.nextLine().trim();
								parametri = input3.split("\\s+");
								//Input check
								testInput = Integer.valueOf(parametri[0]);
								if(parametri.length != 2) throw new NumberFormatException();
								Insegna ins4 = new Insegna(testInput, parametri[1]);
								insDao.delete(ins4);
								break;
							default: System.out.println("Input errato");
								break;
						}
						break;
					default: System.out.println("Input errato esterno");
						break;
				}
				System.out.println("Effettuare operazioni su altre tabelle? Inserire si per ripetere");
				String repeat = scanner.nextLine().trim();
				if(!repeat.equalsIgnoreCase("si"))
					continua = false;
			}
			
		}catch(NumberFormatException e) {
			System.out.println("Parametri errati");
		}catch(Exception e) {}
		
		
	}
 
}
