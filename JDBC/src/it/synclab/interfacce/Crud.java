package it.synclab.interfacce;
import java.sql.*;
import java.util.ArrayList;

public interface Crud {
	
	public abstract void create(Object obj) throws Exception;
	
	public abstract void getAll() throws Exception;
	
	public abstract void get(Object id) throws Exception;
	
	public abstract void update(Object id, Object colonna, Object valore) throws Exception;
	
	public abstract void delete(Object id) throws Exception;
}
