package com.achievents.demo.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.achievents.demo.entity.Studente;
import com.achievents.demo.service.StudenteService;

@Controller
@RequestMapping("/studenti")
public class StudenteController {

	private StudenteService studenteService;

	public StudenteController(StudenteService studenteService) {
		super();
		this.studenteService = studenteService;
	}

	@GetMapping("/list")
	public String list(Model model) {

		List<Studente> studenti = studenteService.findAll();
		model.addAttribute("studenti", studenti);

		return "studenti/lista-studenti";
	}

	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model model) {

		Studente studente = new Studente();

		model.addAttribute("studente", studente);

		return "studenti/studente-form";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("studente") Studente studente) {

		studenteService.save(studente);

		return "redirect:/";
	}

	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("idStudente") int id, Model model) {

		Studente studente = studenteService.findById(id);

		model.addAttribute("studente", studente);

		return "studenti/studente-form";
	}

	@GetMapping("/delete")
	public String delete(@RequestParam("idStudente") int id, Model model) {

		studenteService.deleteById(id);;

		return "redirect:/studenti/list";
	}
}
