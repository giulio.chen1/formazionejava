package com.achievents.demo.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.achievents.demo.entity.Universita;
import com.achievents.demo.service.UniversitaService;

@Controller
@RequestMapping("/universita")
public class UniversitaController {

	private UniversitaService uniService;

	public UniversitaController(UniversitaService uniService) {
		super();
		this.uniService = uniService;
	}

	@GetMapping("/list")
	public String list(Model model) {

		List<Universita> universita = uniService.findAll();
		model.addAttribute("universita", universita);

		return "universita/lista-universita";
	}

	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model model) {

		Universita uni = new Universita();

		model.addAttribute("uni", uni);

		return "universita/uni-form";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("uni") Universita uni) {

		uniService.save(uni);

		return "redirect:/";
	}

	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("idUni") String id, Model model) {

		Universita uni = uniService.findById(id);

		model.addAttribute("uni", uni);

		return "universita/uni-form";
	}

	@GetMapping("/delete")
	public String delete(@RequestParam("idUni") String id, Model model) {

		uniService.deleteById(id);;

		return "redirect:/universita/list";
	}
}
