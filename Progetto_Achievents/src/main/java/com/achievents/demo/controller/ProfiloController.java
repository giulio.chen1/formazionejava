package com.achievents.demo.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.achievents.demo.service.*;
import com.achievents.demo.entity.*;


@Controller
public class ProfiloController {
	
	private AziendaService aziendaService;
	private UniversitaService uniService;
	private StudenteService studenteService;
	private UtenzaService utenzaService;

	public ProfiloController(AziendaService aziendaService, UniversitaService uniService,
			StudenteService studenteService, UtenzaService utenzaService) {
		super();
		this.aziendaService = aziendaService;
		this.uniService = uniService;
		this.studenteService = studenteService;
		this.utenzaService = utenzaService;
	}


	@GetMapping("/profilo")
	public String profilo(@RequestParam("username") String username, Model model) {
		
		Utenza user = utenzaService.findById(username);
		
		if(user.getTipo().equals("azienda")) {
			Azienda azienda = aziendaService.findByUsername(username);
			model.addAttribute("azienda", azienda);
			return "profilo-azienda";
			
		}else if(user.getTipo().equals("universita")) {
			Universita uni = uniService.findByUsername(username);
			model.addAttribute("uni", uni);
			return "profilo-uni";
			
		}else {
			Studente studente = studenteService.findByUsername(username);
			model.addAttribute("studente", studente);
			return "profilo-studente";
		}
	}
}
