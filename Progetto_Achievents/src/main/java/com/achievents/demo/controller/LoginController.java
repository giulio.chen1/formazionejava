package com.achievents.demo.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.achievents.demo.dao.AuthoritiesRepository;
import com.achievents.demo.entity.Authorities;
import com.achievents.demo.entity.Azienda;
import com.achievents.demo.entity.Studente;
import com.achievents.demo.entity.Universita;
import com.achievents.demo.entity.Utenza;
import com.achievents.demo.service.AuthoritiesService;
import com.achievents.demo.service.AziendaService;
import com.achievents.demo.service.StudenteService;
import com.achievents.demo.service.UniversitaService;
import com.achievents.demo.service.UtenzaService;

@Controller
public class LoginController {
	
	private UtenzaService userService;
	private AziendaService aziendaService;
	private UniversitaService uniService;
	private StudenteService studenteService;
	private AuthoritiesService authService;

	public LoginController(UtenzaService userService, AziendaService aziendaService, UniversitaService uniService,
			StudenteService studenteService, AuthoritiesService authService) {
		super();
		this.userService = userService;
		this.aziendaService = aziendaService;
		this.uniService = uniService;
		this.studenteService = studenteService;
		this.authService = authService;
	}

	@GetMapping("/showLogin")
	public String showMyLoginPage() {
		
		return "login";
		
	}
	
	// add request mapping for /access-denied
	
	@GetMapping("/access-denied")
	public String showAccessDenied() {
		
		return "access-denied";
		
	}
	
	@GetMapping("/registration")
	public String registration(Model model){
		
		Utenza user = new Utenza();
		
		model.addAttribute("user", user);
		
		return "registration-form";
	}
	
	@PostMapping("/registrati")
	public String registrati(@ModelAttribute("user") Utenza user, Model model){
		
		if(user.getTipo().equals("azienda")) {
			Azienda azienda = new Azienda();
			model.addAttribute("azienda", azienda);
			model.addAttribute("user", user);
			return "/aziende/azienda-form";
		}else if(user.getTipo().equals("universita")) {
			Universita uni = new Universita();
			model.addAttribute("uni", uni);
			model.addAttribute("user", user);
			return "/universita/uni-form";
		}
		Studente studente = new Studente();
		model.addAttribute("studente", studente);
		model.addAttribute("user", user);
		return "/studenti/studente-form";
	}
	
	@PostMapping("/azienda-info")
	public String aziendaInfo(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("tipo") String tipo, @ModelAttribute("azienda") Azienda azienda) {
		
		Utenza user = new Utenza(username, "{noop}" + password, tipo);
		azienda.setUtenzaAz(user);
		userService.save(user);
		aziendaService.save(azienda);
		authService.save(new Authorities(user.getUsername(), "ROLE_AZIENDA"));
		
		return "redirect:/registrazione-successo";
	}
	
	@PostMapping("/uni-info")
	public String uniInfo(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("tipo") String tipo, @ModelAttribute("uni") Universita uni) {
		
		Utenza user = new Utenza(username, "{noop}" + password, tipo);
		uni.setUtenzaUni(user);
		userService.save(user);
		uniService.save(uni);
		authService.save(new Authorities(user.getUsername(), "ROLE_UNIVERSITA"));
		
		return "redirect:/registrazione-successo";
	}
	
	@PostMapping("/studente-info")
	public String studenteInfo(@RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("tipo") String tipo, @ModelAttribute("studente") Studente studente) {
		
		Utenza user = new Utenza(username, "{noop}" + password, tipo);
		studente.setUtenzaStud(user);
		Universita uni = uniService.findByNome(studente.getUniStud().getNome());
		uni.addStudente(studente);
		studente.setUniStud(uni);
		
		studenteService.save(studente);
		authService.save(new Authorities(user.getUsername(), "ROLE_STUDENTE"));
		
		return "redirect:/registrazione-successo";
	}
	
	@GetMapping("/registrazione-successo")
	public String registrazioneSuccesso(){
		
		return "registrazione-successo";
	}
	
}