package com.achievents.demo.controller;

import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.achievents.demo.entity.Azienda;
import com.achievents.demo.entity.Dipendente;
import com.achievents.demo.entity.Offerta;
import com.achievents.demo.entity.Stage;
import com.achievents.demo.entity.Studente;
import com.achievents.demo.entity.Universita;
import com.achievents.demo.entity.Utenza;
import com.achievents.demo.service.*;

@Controller
@RequestMapping("/offerte")
public class OffertaController {

	private OffertaService offertaService;
	private AziendaService aziendaService;
	private UniversitaService uniService;
	private UtenzaService utenzaService;
	private StudenteService studenteService;
	private DipendenteService dipService;
	
	public OffertaController(OffertaService offertaService, AziendaService aziendaService, UniversitaService uniService,
			UtenzaService utenzaService, StudenteService studenteService, DipendenteService dipService) {
		super();
		this.offertaService = offertaService;
		this.aziendaService = aziendaService;
		this.uniService = uniService;
		this.utenzaService = utenzaService;
		this.studenteService = studenteService;
		this.dipService = dipService;
	}

	@GetMapping("/full-list")
	public String fullList(Model model) {

		List<Offerta> offerte = offertaService.findAll();
		
		if(offerte == null || offerte.size() == 0) {
			return "offerte/offerta-non-trovata";
		}
		model.addAttribute("offerte", offerte);

		return "offerte/lista-completa-offerte";
	}
	
	@GetMapping("/list")
	public String list(@RequestParam("username") String username, Model model) {
		
		Utenza user = utenzaService.findById(username);
		List<Offerta> offerte = null;
		
		if(user.getTipo().equals("azienda")) {
			
			Azienda azienda = aziendaService.findByUsername(username);
			offerte = offertaService.findByAziendaId(azienda.getPartitaiva());
			
		}else if(user.getTipo().equals("studente")) {
			
			Studente studente = studenteService.findByUsername(username);
			offerte = offertaService.findByUniId(studente.getUniStud().getIduniversita());
			
		}else if(user.getTipo().equals("universita")) {
			
			Universita uni = uniService.findByUsername(username);
			offerte = offertaService.findByUniId(uni.getIduniversita());
		}
		
		if(offerte == null || offerte.size() == 0) {
			return "offerte/offerta-non-trovata";
		}
		
		model.addAttribute("offerte", offerte);
		
		return "offerte/lista-offerte";
	}
	
	@GetMapping("/candidature")
	public String listaCandidature(@RequestParam("username") String username, Model model) {

		Studente studente = studenteService.findByUsername(username);
		List<Offerta> offerte = offertaService.findByStudenteId(studente.getId());
		
		if(offerte == null || offerte.size() == 0) {
			return "offerte/offerta-non-trovata";
		}
		
		model.addAttribute("offerte", offerte);

		return "offerte/lista-offerte-candidate";
	}

	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model model) {

		Offerta offerta = new Offerta();

		model.addAttribute("offerta", offerta);

		return "offerte/creazione-offerta-form";
	}
	
	@PostMapping("/create")
	public String create(@ModelAttribute("offerta") Offerta offerta, @RequestParam("username") String username, RedirectAttributes redirectAttrs) {
		
		Azienda azienda = aziendaService.findByUsername(username);
		
		offerta.setAziendaOffer(azienda);
		
		Stage stage = offerta.getStageOffer();
		
		if(stage.getDatainizio() == null || stage.getDurata() == 0 || stage.getTipo() == null) {
			offerta.setStageOffer(null);
		}
		
		offertaService.save(offerta);
		
		redirectAttrs.addAttribute("username", username);

		return "redirect:/offerte/list/{username}";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("offerta") Offerta offerta, RedirectAttributes redirectAttrs) {
		
		if(offerta.getStageOffer().getId() == null) {
			offerta.setStageOffer(null);
		}
		
		offertaService.save(offerta);
		
		redirectAttrs.addAttribute("username", offerta.getAziendaOffer().getUtenzaAz().getUsername());
		
		return "redirect:/offerte/list/{username}";
	}
	
	@GetMapping("/list/{username}")
	public String listByUsername(@PathVariable("username") String username, Model model) {
		
		Utenza user = utenzaService.findById(username);
		List<Offerta> offerte = null;
		
		if(user.getTipo().equals("azienda")) {
			
			Azienda azienda = aziendaService.findByUsername(username);
			offerte = offertaService.findByAziendaId(azienda.getPartitaiva());
			
		}else if(user.getTipo().equals("studente")) {

			Studente studente = studenteService.findByUsername(username);
			offerte = offertaService.findByUniId(studente.getUniStud().getIduniversita());
			
			
		}else if(user.getTipo().equals("universita")) {
			
			Universita uni = uniService.findByUsername(username);
			offerte = offertaService.findByUniId(uni.getIduniversita());
		}
		
		if(offerte == null || offerte.size() == 0) {
			return "offerte/offerta-non-trovata";
		}
		
		model.addAttribute("offerte", offerte);
		
		return "offerte/lista-offerte";
	}

	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("idOfferta") int id, Model model) {
		
		boolean isStage = false;
		Offerta offerta = offertaService.findById(id);
		
		if(offerta.getStageOffer() != null) {
			isStage = true;
		}
		
		model.addAttribute("offerta", offerta);
		model.addAttribute("isStage", isStage);
		
		return "offerte/offerta-form";
	}
	
	@GetMapping("/info")
	public String showInfo(@RequestParam("idOfferta") int id, @RequestParam("username") String username, Model model) {
		
		boolean studenteCandidato = false;
		String noStage = "";
		List<Offerta> offerte = null;
		boolean proprietario = false;
		List<Studente> studentiCandidati = null;
		List<Dipendente> dipendenti = null;
		boolean noTutorAziendale = false;
		boolean noTutorUni = false;
		
		Offerta offerta = offertaService.findById(id);
		Utenza user = utenzaService.findById(username);
		
		if(offerta.getStageOffer() == null) {
			noStage = "noStage";
		}
		
		if(noStage != "noStage") {
			dipendenti = dipService.findStageTutor(offerta.getStageOffer().getId());
			if(offerta.getStageOffer().getTipo().equals("Curriculare")) {
				model.addAttribute("tutorUni", dipendenti);
				noTutorAziendale = true;
			}else if(offerta.getStageOffer().getTipo().equals("Extracurriculare")) {
				model.addAttribute("tutorAz", dipendenti);
				noTutorUni = true;
			}
		}
		
		if(user.getTipo().equals("studente")) {
			Studente studente = studenteService.findByUsername(username);
			offerte = offertaService.findByStudenteId(studente.getId());
			
		}else if(user.getTipo().equals("azienda")) {
			Azienda azienda = aziendaService.findByUsername(username);
			if(offerta.getAziendaOffer().getPartitaiva().equals(azienda.getPartitaiva())) {
				proprietario = true;
			}
			studentiCandidati = studenteService.studentiCandidati(id);
			model.addAttribute("proprietario", proprietario);
			model.addAttribute("studentiCandidati", studentiCandidati);
			model.addAttribute("numeroCandidati", studentiCandidati.size());
			
		}else if(user.getTipo().equals("universita")) {
			Universita uni = uniService.findByUsername(username);
			if(offerta.getUniOffer() != null && offerta.getUniOffer().getIduniversita().equals(uni.getIduniversita())) {
				proprietario = true;
			}
			
			studentiCandidati = studenteService.studentiCandidati(id);
			model.addAttribute("proprietario", proprietario);
			model.addAttribute("numeroCandidati", studentiCandidati.size());
		}
		
		if(offerte != null && offerte.contains(offerta)) {
			studenteCandidato = true;
		}
		model.addAttribute("offerta", offerta);
		model.addAttribute("noStage", noStage);
		model.addAttribute("studenteCandidato", studenteCandidato);
		model.addAttribute("noTutorAz", noTutorAziendale);
		model.addAttribute("noTutorUni", noTutorUni);

		return "offerte/info-offerta";
	}

	@GetMapping("/delete")
	public String delete(@RequestParam("IdOfferta") int id, RedirectAttributes redirectAttrs) {
		
		Offerta offerta = offertaService.findById(id);
		
		redirectAttrs.addAttribute("username", offerta.getAziendaOffer().getUtenzaAz().getUsername());
		
		offertaService.deleteById(id);;

		return "redirect:/offerte/list/{username}";
	}
	
	@GetMapping("/valida")
	public String valida(@RequestParam("idOfferta") int id, @RequestParam("username") String username) {
		
		Offerta offerta = offertaService.findById(id);
		
		Universita uni = uniService.findByUsername(username);
		
		offerta.setUniOffer(uni);
		
		offerta.setValido(true);
		
		offertaService.save(offerta);

		return "redirect:/offerte/full-list";
	}
	
	@GetMapping("/invalida")
	public String invalida(@RequestParam("idOfferta") int id) {
		
		Offerta offerta = offertaService.findById(id);
		
		offerta.setUniOffer(null);
		
		if(offerta.getStageOffer() != null) {
			Stage stage = offerta.getStageOffer();
			
			stage.removeReferences();
			
			offerta.setStageOffer(stage);
			
			offerta.removeReferences();
		}
		
		offerta.setValido(false);
		
		offertaService.save(offerta);

		return "redirect:/offerte/full-list";
	}
	
	@GetMapping("/candidati")
	public String candidati(@RequestParam("idOfferta") int id, @RequestParam("username") String username, RedirectAttributes redirectAttrs) {

		Studente studente = studenteService.findByUsername(username);
		Offerta offerta = offertaService.findById(id);
		
		studente.addOfferta(offerta);
		
		studenteService.save(studente);
		
		offertaService.save(offerta);
		
		redirectAttrs.addAttribute("username", username);

		return "redirect:/offerte/list/{username}";
	}
	
	@GetMapping("/disdire")
	public String disdire(@RequestParam("idOfferta") int id, @RequestParam("username") String username, RedirectAttributes redirectAttrs) {

		Studente studente = studenteService.findByUsername(username);
		Offerta offerta = offertaService.findById(id);
		
		studente.removeOfferta(offerta);
		
		studenteService.save(studente);
		
		offertaService.save(offerta);
		
		redirectAttrs.addAttribute("username", username);

		return "redirect:/offerte/list/{username}";
	}
	
	@GetMapping("/showTutorForm")
	public String showTutorForm(@RequestParam("idOfferta") int id, @RequestParam("username") String username, Model model) {

		Offerta offerta = offertaService.findById(id);
		Utenza user = utenzaService.findById(username);
		Dipendente dip = new Dipendente();
		List<Dipendente> dipendenti = null;
		List<Dipendente> tutors = null;
		
		if(user.getTipo().equals("azienda") && offerta.getStageOffer().getTipo().equals("Extracurriculare")) {
			dipendenti = dipService.findTutorByAzienda(username);
			tutors = dipService.findStageTutor(offerta.getStageOffer().getId());
			if(dipendenti != null && tutors != null) {
				for(Iterator<Dipendente> it = dipendenti.iterator(); it.hasNext();) {
					Dipendente d = it.next();
					if(tutors.contains(d)) {
						it.remove();
					}
				}
			}
			
		}else if(user.getTipo().equals("universita") && offerta.getStageOffer().getTipo().equals("Curriculare")) {
			dipendenti = dipService.findTutorByUni(username);
			tutors = dipService.findStageTutor(offerta.getStageOffer().getId());
			if(dipendenti != null && tutors != null) {
				for(Iterator<Dipendente> it = dipendenti.iterator(); it.hasNext();) {
					Dipendente d = it.next();
					if(tutors.contains(d)) {
						it.remove();
					}
				}
			}
					
		}
		
		if(dipendenti.size() != 0) {
			model.addAttribute("tutors", dipendenti);
		}
		
		model.addAttribute("numeroTutor", dipendenti.size());
		model.addAttribute("idOfferta", offerta.getId());
		model.addAttribute("tutor", dip);

		return "offerte/tutor-form";
	}
	
	@PostMapping("/salvaTutor")
	public String salvaTutor(@ModelAttribute("tutor") Dipendente tutor, @RequestParam("username") String username, RedirectAttributes redirectAttrs) {
		
		Utenza user = utenzaService.findById(username);
		
		if(user.getTipo().equals("azienda")) {
			Azienda azienda = aziendaService.findByUsername(username);
			tutor.setAziendaDip(azienda);
			
		}else if(user.getTipo().equals("universita")) {
			Universita uni = uniService.findByUsername(username);
			tutor.setUniDip(uni);
		}
		
		dipService.save(tutor);
		
		redirectAttrs.addAttribute("username", username);

		return "redirect:/offerte/list/{username}";
	}
	
	@GetMapping("/assegnaTutor")
	public String assegnaTutor(@RequestParam("idOfferta") int id, @RequestParam("cf") String cf, @RequestParam("username") String username, RedirectAttributes redirectAttrs) {
		
		Dipendente tutor = dipService.findById(cf);
		Offerta offerta = offertaService.findById(id);
		Stage stage = offerta.getStageOffer();
		List<Dipendente> tutors = stage.getTutorStage();
		
		tutor.addStage(stage);
		tutors.add(tutor);
		stage.setTutorStage(tutors);
		offerta.setStageOffer(stage);
		
		offertaService.save(offerta);
		
		redirectAttrs.addAttribute("username", username);

		return "redirect:/offerte/list/{username}";
	}
	
	@GetMapping("/ricerca-form")
	public String ricercaForm(Model model) {
		
		Offerta offerta = new Offerta();

		model.addAttribute("offerta", offerta);

		return "offerte/ricerca-offerta";
	}
	
	@GetMapping("/ricerca")
	public String ricerca(@ModelAttribute("offerta") Offerta offerta, Model model) {
		
		List<Offerta> offerte = null;
		String valido = offerta.isValido() ? "1" : "0";
		
		if(offerta.getStageOffer().getDatainizio().length() == 0 && offerta.getStageOffer().getTipo().length() == 0 && offerta.getStageOffer().getDurata() == 0) {
			offerte = offertaService.findByLavoro(offerta.getTitolo(), offerta.getSettore(), valido);
		}else if(offerta.getStageOffer().getDurata() == 0){
			offerte = offertaService.findByStage(offerta.getTitolo(), offerta.getSettore(), valido, offerta.getStageOffer().getTipo(), offerta.getStageOffer().getDatainizio(), "");
		}else {
			offerte = offertaService.findByStage(offerta.getTitolo(), offerta.getSettore(), valido, offerta.getStageOffer().getTipo(), offerta.getStageOffer().getDatainizio(), String.valueOf(offerta.getStageOffer().getDurata()));
		}
		
		if(offerte.size() == 0 || offerte == null) {
			return "/offerte/offerta-non-trovata";
		}
		
		model.addAttribute("offerte", offerte);
		return "/offerte/lista-ricerca";
	}
}
