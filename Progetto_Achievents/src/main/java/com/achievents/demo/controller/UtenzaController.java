package com.achievents.demo.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.achievents.demo.entity.Utenza;
import com.achievents.demo.service.UtenzaService;

@Controller
@RequestMapping("/users")
public class UtenzaController {

	private UtenzaService userService;

	// Since only one constructor, autowire is optional
	public UtenzaController(UtenzaService userService) {
		super();
		this.userService = userService;
	}

	// Add mapping for "/list"
	@GetMapping("/list")
	public String list(Model model) {

		// Get users from db
		List<Utenza> utenti = userService.findAll();
		// Add to spring model
		model.addAttribute("users", utenti);

		return "users/list-users";
	}

	// Show form for add user
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model model) {

		// Create model attribute to bind form data
		Utenza utente = new Utenza();

		model.addAttribute("user", utente);

		return "users/user-form";
	}

	// Add mapping for save user
	@PostMapping("/save")
	public String save(@ModelAttribute("user") Utenza utente) {

		// Save user
		userService.save(utente);

		// Use redirect to prevent duplicate submissions
		return "redirect:/users/list";
	}

	// Add mapping for update user
	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("userId") String username, Model model) {

		// Get user from service
		Utenza utente = userService.findById(username);

		// Set user as model attribute to prepopulate the form
		model.addAttribute("user", utente);

		// Send over to our form
		return "users/user-form";
	}

	// Add mapping for delete user
	@GetMapping("/delete")
	public String delete(@RequestParam("userId") String username, Model model) {

		// Delete user
		userService.deleteById(username);;

		// Use redirect to prevent duplicate submissions
		return "redirect:/users/list";
	}
}
