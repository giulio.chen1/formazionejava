package com.achievents.demo.controller;

import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.achievents.demo.entity.Azienda;
import com.achievents.demo.entity.Evento;
import com.achievents.demo.entity.Studente;
import com.achievents.demo.entity.Universita;
import com.achievents.demo.entity.Utenza;
import com.achievents.demo.service.AziendaService;
import com.achievents.demo.service.EventoService;
import com.achievents.demo.service.StudenteService;
import com.achievents.demo.service.UniversitaService;
import com.achievents.demo.service.UtenzaService;

@Controller
@RequestMapping("/eventi")
public class EventoController {

	private EventoService eventoService;
	private AziendaService aziendaService;
	private UniversitaService uniService;
	private StudenteService studenteService;
	private UtenzaService utenzaService;

	public EventoController(EventoService eventoService, AziendaService aziendaService, UniversitaService uniService,
			UtenzaService utenzaService, StudenteService studenteService) {
		super();
		this.eventoService = eventoService;
		this.aziendaService = aziendaService;
		this.uniService = uniService;
		this.utenzaService = utenzaService;
		this.studenteService = studenteService;
	}

	@GetMapping("/full-list")
	public String fullList(Model model) {

		List<Evento> eventi = eventoService.findAll();
		
		if(eventi == null || eventi.size() == 0) {
			return "eventi/evento-non-trovato";
		}
		
		model.addAttribute("eventi", eventi);

		return "eventi/lista-completa-eventi";
	}

	@GetMapping("/list")
	public String list(@RequestParam("username") String username, Model model) {

		Utenza user = utenzaService.findById(username);
		List<Evento> eventi = null;

		if (user.getTipo().equals("azienda")) {

			eventi = eventoService.findByAzienda(username);

		} else if (user.getTipo().equals("studente")) {

			Studente studente = studenteService.findByUsername(username);
			eventi = eventoService.findByUniId(studente.getUniStud().getIduniversita());

		} else if (user.getTipo().equals("universita")) {

			Universita uni = uniService.findByUsername(username);
			eventi = eventoService.findByUniId(uni.getIduniversita());
		}
		
		if(eventi == null || eventi.size() == 0) {
			return "eventi/evento-non-trovato";
		}
		
		model.addAttribute("eventi", eventi);

		return "eventi/lista-eventi";
	}

	@GetMapping("/partecipazioni")
	public String partecipazioni(@RequestParam("username") String username, Model model) {

		Utenza user = utenzaService.findById(username);
		List<Evento> eventi = null;

		if (user.getTipo().equals("studente")) {
			eventi = eventoService.findByStudente(username);
		} else if (user.getTipo().equals("azienda")) {
			eventi = eventoService.findByAzienda(username);
		}
		
		if(eventi == null || eventi.size() == 0) {
			return "eventi/evento-non-trovato";
		}

		model.addAttribute("eventi", eventi);

		return "eventi/lista-eventi-partecipati";
	}

	@GetMapping("/list/{username}")
	public String listByUsername(@PathVariable("username") String username, Model model) {

		Utenza user = utenzaService.findById(username);
		List<Evento> eventi = null;

		if (user.getTipo().equals("azienda")) {

			eventi = eventoService.findByAzienda(username);

		} else if (user.getTipo().equals("studente")) {

			Studente studente = studenteService.findByUsername(username);
			eventi = eventoService.findByUniId(studente.getUniStud().getIduniversita());

		} else if (user.getTipo().equals("universita")) {

			Universita uni = uniService.findByUsername(username);
			eventi = eventoService.findByUniId(uni.getIduniversita());
		}
		
		if(eventi == null || eventi.size() == 0) {
			return "eventi/evento-non-trovato";
		}

		model.addAttribute("eventi", eventi);

		return "eventi/lista-eventi";
	}

	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model model) {

		Evento evento = new Evento();

		model.addAttribute("evento", evento);

		return "eventi/creazione-evento-form";
	}
	
	@PostMapping("/create")
	public String create(@ModelAttribute("evento") Evento evento, @RequestParam("username") String username, RedirectAttributes redirectAttrs) {
		
		Universita uni = uniService.findByUsername(username);
		
		evento.setUniEv(uni);
		
		eventoService.save(evento);
		
		redirectAttrs.addAttribute("username", username);

		return "redirect:/eventi/list/{username}";
	}
	
	@PostMapping("/save")
	public String save(@ModelAttribute("evento") Evento evento, RedirectAttributes redirectAttrs) {

		eventoService.save(evento);

		redirectAttrs.addAttribute("username", evento.getUniEv().getUtenzaUni().getUsername());

		return "redirect:/eventi/list/{username}";
	}

	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("idEvento") int id, Model model) {

		Evento evento = eventoService.findById(id);

		model.addAttribute("evento", evento);

		return "eventi/evento-form";
	}

	@GetMapping("/info")
	public String showInfo(@RequestParam("idEvento") int id, @RequestParam("username") String username, Model model) {

		boolean iscritto = false;
		List<Evento> eventi = null;

		Evento evento = eventoService.findById(id);
		Utenza user = utenzaService.findById(username);

		if (user.getTipo().equals("studente")) {
			eventi = eventoService.findByStudente(username);
		} else if (user.getTipo().equals("azienda")) {
			eventi = eventoService.findByAzienda(username);
		}

		if (eventi != null && eventi.contains(evento)) {
			iscritto = true;
		}
		model.addAttribute("evento", evento);
		model.addAttribute("iscritto", iscritto);

		return "eventi/info-evento";
	}

	@GetMapping("/delete")
	public String delete(@RequestParam("idEvento") int id, RedirectAttributes redirectAttrs) {

		Evento evento = eventoService.findById(id);

		redirectAttrs.addAttribute("username", evento.getUniEv().getUtenzaUni().getUsername());

		eventoService.deleteById(id);

		return "redirect:/eventi/list/{username}";
	}
	
	@GetMapping("/iscrizione")
	public String iscrizione(@RequestParam("idEvento") int id, @RequestParam("username") String username, RedirectAttributes redirectAttrs) {

		Evento evento = eventoService.findById(id);
		Utenza user = utenzaService.findById(username);
		
		if(user.getTipo().equals("studente")) {
			
			Studente studente = studenteService.findByUsername(username);
			studente.addEvento(evento);
			studenteService.save(studente);
			
		}else if(user.getTipo().equals("azienda")) {
			
			Azienda azienda = aziendaService.findByUsername(username);
			azienda.addEvento(evento);
			aziendaService.save(azienda);
		}

		redirectAttrs.addAttribute("username", username);

		eventoService.save(evento);

		return "redirect:/eventi/list/{username}";
	}
	
	@GetMapping("/disiscrizione")
	public String disiscrizione(@RequestParam("idEvento") int id, @RequestParam("username") String username, RedirectAttributes redirectAttrs) {

		Evento evento = eventoService.findById(id);
		Utenza user = utenzaService.findById(username);
		
		if(user.getTipo().equals("studente")) {
			
			Studente studente = studenteService.findByUsername(username);
			studente.removeEvento(evento);
			studenteService.save(studente);
			
		}else if(user.getTipo().equals("azienda")) {
			
			Azienda azienda = aziendaService.findByUsername(username);
			azienda.removeEvento(evento);
			aziendaService.save(azienda);
		}

		redirectAttrs.addAttribute("username", username);

		eventoService.save(evento);

		return "redirect:/eventi/list/{username}";
	}
	
	@GetMapping("/ricerca-form")
	public String ricercaForm(Model model) {
		
		Evento evento = new Evento();

		model.addAttribute("evento", evento);

		return "eventi/ricerca-evento";
	}
	
	@GetMapping("/ricerca")
	public String ricerca(@ModelAttribute("evento") Evento evento, Model model) {
		
		List<Evento> eventi = null;
		eventi = eventoService.findByFields(evento.getTitolo(), evento.getSettore(), evento.getLuogo(), evento.getData());
		
		if(eventi.size() == 0 || eventi == null) {
			return "/eventi/evento-non-trovato";
		}
		
		model.addAttribute("eventi", eventi);
		return "/eventi/lista-ricerca";
	}
}
