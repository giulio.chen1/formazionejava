package com.achievents.demo.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.achievents.demo.entity.Azienda;
import com.achievents.demo.service.AziendaService;

@Controller
@RequestMapping("/aziende")
public class AziendaController {

	private AziendaService aziendaService;

	// Since only one constructor, autowire is optional
	public AziendaController(AziendaService aziendaService) {
		super();
		this.aziendaService = aziendaService;
	}

	// Add mapping for "/list"
	@GetMapping("/list")
	public String list(Model model) {

		// Get aziende from db
		List<Azienda> aziende = aziendaService.findAll();
		// Add to spring model
		model.addAttribute("aziende", aziende);

		return "aziende/lista-aziende";
	}

	// Show form for add azienda
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model model) {

		// Create model attribute to bind form data
		Azienda azienda = new Azienda();

		model.addAttribute("azienda", azienda);

		return "aziende/azienda-form";
	}

	// Add mapping for save azienda
	@PostMapping("/save")
	public String save(@ModelAttribute("azienda") Azienda azienda) {

		// Save azienda
		aziendaService.save(azienda);

		// Use redirect to prevent duplicate submissions
		return "redirect:/";
	}

	// Add mapping for update azienda
	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("aziendaId") String id, Model model) {

		// Get azienda from service
		Azienda azienda = aziendaService.findById(id);

		// Set azienda as model attribute to prepopulate the form
		model.addAttribute("azienda", azienda);

		// Send over to our form
		return "aziende/azienda-form";
	}

	// Add mapping for delete azienda
	@GetMapping("/delete")
	public String delete(@RequestParam("aziendaId") String id, Model model) {

		// Delete azienda
		aziendaService.deleteById(id);;

		// Use redirect to prevent duplicate submissions
		return "redirect:/aziende/list";
	}
}
