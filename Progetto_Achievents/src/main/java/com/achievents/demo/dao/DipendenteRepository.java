package com.achievents.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.achievents.demo.entity.Dipendente;


public interface DipendenteRepository extends JpaRepository<Dipendente, String> {
	
	//No need to write code
	
	//Add sort method
	
	@Query("select d from Dipendente d join d.stageDip s where s.id = :idstage")
	public List<Dipendente> findStageTutor(@Param("idstage") Integer idstage);
	
	@Query("select d from Dipendente d where d.aziendaDip.utenzaAz.username = :username")
	public List<Dipendente> findTutorByAzienda(@Param("username") String username);
	
	@Query("select d from Dipendente d where d.uniDip.utenzaUni.username = :username")
	public List<Dipendente> findTutorByUni(@Param("username") String username);
}
