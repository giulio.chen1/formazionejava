package com.achievents.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.achievents.demo.entity.Evento;


public interface EventoRepository extends JpaRepository<Evento, Integer> {
	
	//No need to write code
	
	//Add sort method
	public List<Evento> findAll();
	
	@Query("select e from Evento e join e.aziendeEv a where a.utenzaAz.username = :aziendaUser")
	public List<Evento> findByAzienda(@Param("aziendaUser") String aziendaUser);
	
	@Query("select e from Evento e join e.studentiEv s where s.utenzaStud.username = :studenteUser")
	public List<Evento> findByStudente(@Param("studenteUser") String studenteUser);
	
	@Query("select e from Evento e where e.uniEv.iduniversita = :iduniversita")
	public List<Evento> findByUniId(@Param("iduniversita") String iduniversita);
	
	@Query("select e from Evento e where e.titolo LIKE CONCAT('%',:titolo,'%') and e.settore LIKE CONCAT('%',:settore,'%') and e.luogo LIKE CONCAT('%',:luogo,'%') and e.data LIKE CONCAT('%',:data,'%')")
	public List<Evento> findByFields(@Param("titolo") String titolo, @Param("settore") String settore, @Param("luogo") String luogo, @Param("data") String data);
}
