package com.achievents.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.achievents.demo.entity.Universita;


public interface UniversitaRepository extends JpaRepository<Universita, String> {
	
	//No need to write code
	
	//Add sort method
	public List<Universita> findAll();
	
	@Query("select u from Universita u where u.utenzaUni.username = :username")
	public Universita findByUsername(@Param("username") String username);
	
	@Query("select u from Universita u where u.nome = :nome")
	public Universita findByNome(@Param("nome") String nome);
}
