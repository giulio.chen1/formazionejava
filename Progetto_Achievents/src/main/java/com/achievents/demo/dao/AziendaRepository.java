package com.achievents.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.achievents.demo.entity.Azienda;
import com.achievents.demo.entity.Offerta;


public interface AziendaRepository extends JpaRepository<Azienda, String> {
	
	//No need to write code
	
	//Add sort method
	public List<Azienda> findAll();
	
	@Query("select a from Azienda a where a.utenzaAz.username = :username")
	public Azienda findByUsername(@Param("username") String username);
}
