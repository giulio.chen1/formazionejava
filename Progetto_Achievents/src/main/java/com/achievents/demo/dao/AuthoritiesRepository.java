package com.achievents.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.achievents.demo.entity.Authorities;
import com.achievents.demo.entity.Utenza;


public interface AuthoritiesRepository extends JpaRepository<Authorities, String> {
	
	//No need to write code
	
	//Add sort method
	public List<Authorities> findAll();
}
