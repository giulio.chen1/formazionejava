package com.achievents.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.achievents.demo.entity.Studente;
import com.achievents.demo.entity.Universita;


public interface StudenteRepository extends JpaRepository<Studente, Integer> {
	
	//No need to write code
	
	//Add sort method
	public List<Studente> findAll();
	
	@Query("select s from Studente s where s.utenzaStud.username = :username")
	public Studente findByUsername(@Param("username") String username);
	
	@Query("select s from Studente s join s.offerteStud o where o.id = :idofferta")
	public List<Studente> studentiCandidati(@Param("idofferta") int idofferta);
}
