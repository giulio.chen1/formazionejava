package com.achievents.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.achievents.demo.entity.Dipendente;
import com.achievents.demo.entity.Offerta;
import com.achievents.demo.entity.Studente;


public interface OffertaRepository extends JpaRepository<Offerta, Integer> {
	
	//No need to write code
	
	@Query("select o from Offerta o where o.aziendaOffer.partitaiva = :idazienda")
	public List<Offerta> findByAziendaId(@Param("idazienda") String idazienda);
	
	@Query("select o from Offerta o where o.uniOffer.iduniversita = :iduniversita")
	public List<Offerta> findByUniId(@Param("iduniversita") String iduniversita);
	
	@Query("select o from Offerta o join o.studentiOffer s where s.id = :idstudente")
	public List<Offerta> findByStudenteId(@Param("idstudente") int idstudente);
	
	@Query("select o from Offerta o where o.titolo LIKE CONCAT('%',:titolo,'%') and o.settore LIKE CONCAT('%',:settore,'%') and o.valido LIKE CONCAT('%',:valido,'%')")
	public List<Offerta> findByLavoro(@Param("titolo") String titolo, @Param("settore") String settore, @Param("valido") String valido);
	
	@Query("select o from Offerta o join o.stageOffer s where o.titolo LIKE CONCAT('%',:titolo,'%') and o.settore LIKE CONCAT('%',:settore,'%') and o.valido LIKE CONCAT('%',:valido,'%') and s.tipo LIKE CONCAT(:tipo,'%') and s.datainizio LIKE CONCAT('%',:datainizio,'%') and s.durata LIKE CONCAT('%',:durata,'%')")
	public List<Offerta> findByStage(@Param("titolo") String titolo, @Param("settore") String settore, @Param("valido") String valido, @Param("tipo") String tipo, @Param("datainizio") String datainizio, @Param("durata") String durata);
}
