package com.achievents.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.achievents.demo.entity.Utenza;


public interface UtenzaRepository extends JpaRepository<Utenza, String> {
	
	//No need to write code
	
	//Add sort method
	public List<Utenza> findAll();
}
