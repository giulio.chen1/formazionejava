package com.achievents.demo.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.achievents.demo.entity.Evento;


public interface IEventoService {
	
	public List<Evento> findAll();
	
	public Evento findById(int id);
	
	public List<Evento> findByAzienda(String aziendaUser);
	
	public List<Evento> findByStudente(String studenteUser);
	
	public List<Evento> findByUniId(String iduniversita);
	
	public void save(Evento evento);
	
	public void deleteById(int id);
	
	public List<Evento> findByFields(String titolo, String settore, String luogo, String data);

}
