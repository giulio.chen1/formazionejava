package com.achievents.demo.service;

import java.util.List;

import com.achievents.demo.entity.Dipendente;


public interface IDipendenteService {
	
	public List<Dipendente> findAll();
	
	public Dipendente findById(String cf);
	
	public void save(Dipendente d);
	
	public void deleteById(String cf);
	
	public List<Dipendente> findStageTutor(int idstage);
	
	public List<Dipendente> findTutorByAzienda(String username);
	
	public List<Dipendente> findTutorByUni(String username);
}
