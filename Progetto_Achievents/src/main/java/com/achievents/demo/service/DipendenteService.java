package com.achievents.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.achievents.demo.dao.DipendenteRepository;
import com.achievents.demo.entity.Dipendente;


@Service
public class DipendenteService implements IDipendenteService {
	
	private DipendenteRepository dipRepo;
	
	@Autowired
	public DipendenteService(DipendenteRepository dipRepo) {
		this.dipRepo = dipRepo;
	}

	@Override
	public List<Dipendente> findAll() {
		return dipRepo.findAll();
	}

	@Override
	@Transactional
	public Dipendente findById(String cf) {
		Optional<Dipendente> result = dipRepo.findById(cf);
		
		Dipendente dipendente = null;
		
		if(result.isPresent()) {
			dipendente = result.get();
		}else {
			throw new RuntimeException("Did not find cf - " + cf);
		}
		return dipendente ;
	}

	@Override
	public void save(Dipendente dipendente) {
		dipRepo.save(dipendente);
	}

	@Override
	public void deleteById(String cf) {
		dipRepo.deleteById(cf);
	}

	@Override
	public List<Dipendente> findStageTutor(int idstage) {
		return dipRepo.findStageTutor(idstage);
	}

	@Override
	public List<Dipendente> findTutorByAzienda(String username) {
		return dipRepo.findTutorByAzienda(username);
	}

	@Override
	public List<Dipendente> findTutorByUni(String username) {
		return dipRepo.findTutorByUni(username);
	}

}
