package com.achievents.demo.service;

import java.util.List;

import com.achievents.demo.entity.Azienda;
import com.achievents.demo.entity.Universita;


public interface IUniversitaService {
	
	public List<Universita> findAll();
	
	public Universita findById(String id);
	
	public Universita findByUsername(String username);
	
	public void save(Universita uni);
	
	public void deleteById(String id);
	
	public Universita findByNome(String nome);
}
