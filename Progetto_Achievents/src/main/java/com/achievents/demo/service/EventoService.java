package com.achievents.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.achievents.demo.dao.EventoRepository;
import com.achievents.demo.entity.Evento;


@Service
public class EventoService implements IEventoService {
	
	private EventoRepository eventoRepo;
	
	@Autowired
	public EventoService(EventoRepository eventoRepo) {
		this.eventoRepo = eventoRepo;
	}

	@Override
	public List<Evento> findAll() {
		return eventoRepo.findAll();
	}

	@Override
	@Transactional
	public Evento findById(int id) {
		Optional<Evento> result = eventoRepo.findById(id);
		
		Evento evento = null;
		
		if(result.isPresent()) {
			evento = result.get();
		}else {
			throw new RuntimeException("Did not find event id - " + id);
		}
		return evento ;
	}

	@Override
	public void save(Evento evento) {
		eventoRepo.save(evento);
	}

	@Override
	public void deleteById(int id) {
		eventoRepo.deleteById(id);
	}

	@Override
	public List<Evento> findByAzienda(String aziendaUser) {
		return eventoRepo.findByAzienda(aziendaUser);
	}

	@Override
	public List<Evento> findByStudente(String studenteUser) {
		return eventoRepo.findByStudente(studenteUser);
	}

	@Override
	public List<Evento> findByUniId(String iduniversita) {
		return eventoRepo.findByUniId(iduniversita);
	}

	@Override
	public List<Evento> findByFields(String titolo, String settore, String luogo, String data) {
		return eventoRepo.findByFields(titolo, settore, luogo, data);
	}

}
