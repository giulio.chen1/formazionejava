package com.achievents.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.achievents.demo.dao.AuthoritiesRepository;
import com.achievents.demo.entity.Authorities;


@Service
public class AuthoritiesService implements IAuthoritiesService {
	
	private AuthoritiesRepository authRepo;
	
	@Autowired
	public AuthoritiesService(AuthoritiesRepository authRepo) {
		this.authRepo = authRepo;
	}

	@Override
	public List<Authorities> findAll() {
		return authRepo.findAll();
	}

	@Override
	@Transactional
	public Authorities findById(String username) {
		Optional<Authorities> result = authRepo.findById(username);
		
		Authorities auth = null;
		
		if(result.isPresent()) {
			auth = result.get();
		}else {
			throw new RuntimeException("Did not find username - " + username);
		}
		return auth ;
	}

	@Override
	public void save(Authorities auth) {
		authRepo.save(auth);
	}

	@Override
	public void deleteById(String username) {
		authRepo.deleteById(username);
	}

}
