package com.achievents.demo.service;

import java.util.List;

import com.achievents.demo.entity.Studente;


public interface IStudenteService {
	
	public List<Studente> findAll();
	
	public Studente findById(int id);
	
	public Studente findByUsername(String username);
	
	public List<Studente> studentiCandidati(int idofferta);
	
	public void save(Studente studente);
	
	public void deleteById(int id);
}
