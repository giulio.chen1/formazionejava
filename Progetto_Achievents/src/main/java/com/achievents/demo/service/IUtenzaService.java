package com.achievents.demo.service;

import java.util.List;

import com.achievents.demo.entity.Utenza;


public interface IUtenzaService {
	
	public List<Utenza> findAll();
	
	public Utenza findById(String username);
	
	public void save(Utenza ut);
	
	public void deleteById(String username);
}
