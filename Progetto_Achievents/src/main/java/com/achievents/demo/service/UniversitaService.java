package com.achievents.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.achievents.demo.dao.UniversitaRepository;
import com.achievents.demo.entity.Azienda;
import com.achievents.demo.entity.Universita;


@Service
public class UniversitaService implements IUniversitaService {
	
	private UniversitaRepository uniRepo;
	
	@Autowired
	public UniversitaService(UniversitaRepository uniRepo) {
		this.uniRepo = uniRepo;
	}

	@Override
	public List<Universita> findAll() {
		return uniRepo.findAll();
	}

	@Override
	@Transactional
	public Universita findById(String id) {
		Optional<Universita> result = uniRepo.findById(id);
		
		Universita universita = null;
		
		if(result.isPresent()) {
			universita = result.get();
		}else {
			throw new RuntimeException("Did not find university id - " + id);
		}
		return universita ;
	}

	@Override
	public void save(Universita universita) {
		uniRepo.save(universita);
	}

	@Override
	public void deleteById(String id) {
		uniRepo.deleteById(id);
	}

	@Override
	public Universita findByUsername(String username) {
		return uniRepo.findByUsername(username);
	}

	@Override
	public Universita findByNome(String nome) {
		return uniRepo.findByNome(nome);
	}

}
