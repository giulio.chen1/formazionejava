package com.achievents.demo.service;

import java.util.List;

import com.achievents.demo.entity.Azienda;


public interface IAziendaService {
	
	public List<Azienda> findAll();
	
	public Azienda findById(String id);
	
	public Azienda findByUsername(String username);
	
	public void save(Azienda azienda);
	
	public void deleteById(String id);
}
