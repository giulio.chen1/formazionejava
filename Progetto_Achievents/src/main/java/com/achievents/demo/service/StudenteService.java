package com.achievents.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.achievents.demo.dao.StudenteRepository;
import com.achievents.demo.entity.Studente;


@Service
public class StudenteService implements IStudenteService {
	
	private StudenteRepository studenteRepo;
	
	@Autowired
	public StudenteService(StudenteRepository studenteRepo) {
		this.studenteRepo = studenteRepo;
	}

	@Override
	public List<Studente> findAll() {
		return studenteRepo.findAll();
	}

	@Override
	@Transactional
	public Studente findById(int id) {
		Optional<Studente> result = studenteRepo.findById(id);
		
		Studente studente = null;
		
		if(result.isPresent()) {
			studente = result.get();
		}else {
			throw new RuntimeException("Did not find student id - " + id);
		}
		return studente ;
	}

	@Override
	public void save(Studente studente) {
		studenteRepo.save(studente);
	}

	@Override
	public void deleteById(int id) {
		studenteRepo.deleteById(id);
	}

	@Override
	public Studente findByUsername(String username) {
		return studenteRepo.findByUsername(username);
	}

	@Override
	public List<Studente> studentiCandidati(int idofferta) {
		return studenteRepo.studentiCandidati(idofferta);
	}

}
