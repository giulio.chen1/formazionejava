package com.achievents.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.achievents.demo.dao.OffertaRepository;
import com.achievents.demo.entity.Offerta;
import com.achievents.demo.entity.Studente;


@Service
public class OffertaService implements IOffertaService {
	
	private OffertaRepository offertaRepo;
	
	@Autowired
	public OffertaService(OffertaRepository offertaRepo) {
		this.offertaRepo = offertaRepo;
	}

	@Override
	public List<Offerta> findAll() {
		return offertaRepo.findAll();
	}

	@Override
	@Transactional
	public Offerta findById(int id) {
		Optional<Offerta> result = offertaRepo.findById(id);
		
		Offerta offerta = null;
		
		if(result.isPresent()) {
			offerta = result.get();
		}else {
			throw new RuntimeException("Did not find offer id - " + id);
		}
		return offerta ;
	}

	@Override
	public void save(Offerta offerta) {
		offertaRepo.save(offerta);
	}

	@Override
	public void deleteById(int id) {
		offertaRepo.deleteById(id);
	}

	@Override
	public List<Offerta> findByAziendaId(String idazienda) {
		return offertaRepo.findByAziendaId(idazienda);
		
	}

	@Override
	public List<Offerta> findByUniId(String iduniversita) {
		return offertaRepo.findByUniId(iduniversita);
	}

	@Override
	public List<Offerta> findByStudenteId(int idstudente) {
		return offertaRepo.findByStudenteId(idstudente);
	}

	@Override
	public List<Offerta> findByStage(String titolo, String settore, String valido, String tipo, String datainizio, String durata) {
		return offertaRepo.findByStage(titolo, settore, valido, tipo, datainizio, durata);
	}

	@Override
	public List<Offerta> findByLavoro(String titolo, String settore, String valido) {
		return offertaRepo.findByLavoro(titolo, settore, valido);
	}

}
