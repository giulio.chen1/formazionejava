package com.achievents.demo.service;

import java.util.List;

import com.achievents.demo.entity.Authorities;


public interface IAuthoritiesService {
	
	public List<Authorities> findAll();
	
	public Authorities findById(String username);
	
	public void save(Authorities auth);
	
	public void deleteById(String username);
}
