package com.achievents.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.achievents.demo.dao.UtenzaRepository;
import com.achievents.demo.entity.Utenza;


@Service
public class UtenzaService implements IUtenzaService {
	
	private UtenzaRepository userRepo;
	
	@Autowired
	public UtenzaService(UtenzaRepository userRepo) {
		this.userRepo = userRepo;
	}

	@Override
	public List<Utenza> findAll() {
		return userRepo.findAll();
	}

	@Override
	@Transactional
	public Utenza findById(String username) {
		Optional<Utenza> result = userRepo.findById(username);
		
		Utenza utente = null;
		
		if(result.isPresent()) {
			utente = result.get();
		}else {
			throw new RuntimeException("Did not find username - " + username);
		}
		return utente ;
	}

	@Override
	public void save(Utenza utente) {
		userRepo.save(utente);
	}

	@Override
	public void deleteById(String username) {
		userRepo.deleteById(username);
	}

}
