package com.achievents.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.achievents.demo.dao.AziendaRepository;
import com.achievents.demo.entity.Azienda;


@Service
public class AziendaService implements IAziendaService {
	
	private AziendaRepository aziendaRepo;
	
	@Autowired
	public AziendaService(AziendaRepository aziendaRepo) {
		this.aziendaRepo = aziendaRepo;
	}

	@Override
	public List<Azienda> findAll() {
		return aziendaRepo.findAll();
	}

	@Override
	@Transactional
	public Azienda findById(String id) {
		Optional<Azienda> result = aziendaRepo.findById(id);
		
		Azienda azienda = null;
		
		if(result.isPresent()) {
			azienda = result.get();
		}else {
			throw new RuntimeException("Did not find employee id - " + id);
		}
		return azienda ;
	}

	@Override
	public void save(Azienda azienda) {
		aziendaRepo.save(azienda);
	}

	@Override
	public void deleteById(String id) {
		aziendaRepo.deleteById(id);
	}

	@Override
	public Azienda findByUsername(String username) {
		return aziendaRepo.findByUsername(username);
	}

}
