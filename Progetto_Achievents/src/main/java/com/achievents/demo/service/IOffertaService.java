package com.achievents.demo.service;

import java.util.List;

import com.achievents.demo.entity.Offerta;
import com.achievents.demo.entity.Studente;


public interface IOffertaService {
	
	public List<Offerta> findAll();
	
	public List<Offerta> findByAziendaId(String idazienda);
	
	public List<Offerta> findByUniId(String iduniversita);
	
	public List<Offerta> findByStudenteId(int idstudente);
	
	public Offerta findById(int id);
	
	public void save(Offerta offerta);
	
	public void deleteById(int id);
	
	public List<Offerta> findByLavoro(String titolo, String settore, String valido);
	
	public List<Offerta> findByStage(String titolo, String settore, String valido, String tipo, String datainizio, String durata);
}
