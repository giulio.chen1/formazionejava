package com.achievents.demo.entity;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;

@Entity
public class Offerta {
	@Id
	@Column(name="idofferta")
	private int id;
	private String titolo, settore;
	@Column(name="descrizione")
	private String descr;
	private boolean valido;
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="idazienda")
	private Azienda aziendaOffer;
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="iduniversita", nullable=true)
	private Universita uniOffer;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="idstage", nullable=true)
	private Stage stageOffer;
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="offerteStud")
	List<Studente> studentiOffer = new LinkedList<Studente>();
	
	public Offerta() {
		this.valido = false;
	}
	
	public Offerta(int id, String titolo, String settore, String descr) {
		super();
		this.id = id;
		this.titolo = titolo;
		this.settore = settore;
		this.descr = descr;
		this.valido = false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getSettore() {
		return settore;
	}

	public void setSettore(String settore) {
		this.settore = settore;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public boolean isValido() {
		return valido;
	}

	public void setValido(boolean valido) {
		this.valido = valido;
	}

	public Azienda getAziendaOffer() {
		return aziendaOffer;
	}

	public void setAziendaOffer(Azienda aziendaOffer) {
		this.aziendaOffer = aziendaOffer;
	}
	
	public Universita getUniOffer() {
		return uniOffer;
	}

	public void setUniOffer(Universita uniOffer) {
		this.uniOffer = uniOffer;
	}
	
	public Stage getStageOffer() {
		return stageOffer;
	}

	public void setStageOffer(Stage stageOffer) {
		this.stageOffer = stageOffer;
	}

	public List<Studente> getStudentiOffer() {
		return studentiOffer;
	}

	public void setStudentiOffer(List<Studente> studentiOffer) {
		this.studentiOffer = studentiOffer;
	}
	
	@PreRemove
	public void removeReferences() {
		for(Studente s: studentiOffer) {
			s.removeOffertaReferences(this);
		}
	}
	

	
	
	
	
}
