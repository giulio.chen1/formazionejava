package com.achievents.demo.entity;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Studente {
	
	@Id
	@Column(name="idstudente")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String nome, cognome, facolta, cv;
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="IDUNIVERSITA")
	private Universita uniStud;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="USERNAME")
	private Utenza utenzaStud;
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="STUDENTE_EVENTO", joinColumns= @JoinColumn(name="IDSTUDENTE", referencedColumnName="IDSTUDENTE"),
            	inverseJoinColumns= @JoinColumn(name="IDEVENTO", referencedColumnName="IDEVENTO"))
	List<Evento> eventiStud = new LinkedList<Evento>();
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="STUDENTE_OFFERTA", inverseJoinColumns= @JoinColumn(name="IDOFFERTA", referencedColumnName="IDOFFERTA"),
            	joinColumns= @JoinColumn(name="IDSTUDENTE", referencedColumnName="IDSTUDENTE"))
	private List<Offerta> offerteStud = new LinkedList<Offerta>();
	
	public Studente() {
		this.cv = "cv.pdf";
	}

	public Studente(String nome, String cognome, String facolta) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.facolta = facolta;
		this.cv = "cv.pdf";
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getFacolta() {
		return facolta;
	}

	public void setFacolta(String facolta) {
		this.facolta = facolta;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	@Override
	public String toString() {
		return "Studente [id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", facolta=" + facolta + ", cv=" + cv
				+ "]";
	}

	public Universita getUniStud() {
		return uniStud;
	}

	public void setUniStud(Universita uniStud) {
		this.uniStud = uniStud;
	}

	public Utenza getUtenzaStud() {
		return utenzaStud;
	}

	public void setUtenzaStud(Utenza utenzaStud) {
		this.utenzaStud = utenzaStud;
	}
	
	public List<Evento> getEventiStud() {
		return eventiStud;
	}

	public void setEventiStud(List<Evento> eventiStud) {
		this.eventiStud = eventiStud;
	}
	
	public List<Offerta> getOfferteStud() {
		return offerteStud;
	}

	public void setOfferteStud(List<Offerta> offerteStud) {
		this.offerteStud = offerteStud;
	}
	
	public void removeEventoReferences(Evento evento) {
		for (Iterator<Evento> it = eventiStud.iterator(); it.hasNext();) {
			Evento e = it.next();
            if(e.getId() == evento.getId()){
                it.remove();
            }
        }
	}
	
	public void removeOffertaReferences(Offerta offerta) {
		for (Iterator<Offerta> it = offerteStud.iterator(); it.hasNext();) {
			Offerta o = it.next();
            if(o.getId() == offerta.getId()){
                it.remove();
            }
        }
	}
	
	public void addEvento(Evento evento) {
		this.eventiStud.add(evento);
	}
	
	public void addOfferta(Offerta offerta) {
		this.offerteStud.add(offerta);
	}
	
	public void removeEvento(Evento evento) {
		this.eventiStud.remove(evento);
	}
	
	public void removeOfferta(Offerta offerta) {
		this.offerteStud.remove(offerta);
	}
	
}
