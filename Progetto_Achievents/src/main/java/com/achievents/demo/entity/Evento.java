package com.achievents.demo.entity;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;

@Entity
public class Evento {
	@Id
	@Column(name="idevento")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String titolo;
	@Column(name="descrizione")
	private String desc;
	private String luogo;
	private String data;
	private String settore;
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="eventiAz",  cascade = CascadeType.PERSIST)
	private List<Azienda> aziendeEv = new LinkedList<Azienda>();
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="eventiStud",  cascade = CascadeType.PERSIST)
	private List<Studente> studentiEv = new LinkedList<Studente>();
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="IDUNIVERSITA")
	private Universita uniEv;
	
	public Evento() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getLuogo() {
		return luogo;
	}

	public void setLuogo(String luogo) {
		this.luogo = luogo;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getSettore() {
		return settore;
	}

	public void setSettore(String settore) {
		this.settore = settore;
	}

	public List<Azienda> getAziendeEv() {
		return aziendeEv;
	}

	public void setAziendeEv(List<Azienda> aziendeEv) {
		this.aziendeEv = aziendeEv;
	}

	public Universita getUniEv() {
		return uniEv;
	}

	public void setUniEv(Universita uniEv) {
		this.uniEv = uniEv;
	}
	
	public List<Studente> getStudentiEv() {
		return studentiEv;
	}

	public void setStudentiEv(List<Studente> studentiEv) {
		this.studentiEv = studentiEv;
	}
	
	@PreRemove
	public void removeReferences() {
		for(Studente s: studentiEv) {
			s.removeEventoReferences(this);
		}
		for(Azienda a: aziendeEv) {
			a.removeEventoReferences(this);
		}
	}
	
	public void addStudente(Studente studente) {
		studentiEv.add(studente);
	}
	
	public void addAzienda(Azienda azienda) {
		aziendeEv.add(azienda);
	}
}
