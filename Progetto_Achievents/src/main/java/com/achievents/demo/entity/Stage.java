package com.achievents.demo.entity;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PreRemove;

@Entity
public class Stage {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idstage")
	private Integer id;
	private int durata;
	private String datainizio;
	private String tipo;
	@OneToOne(mappedBy="stageOffer", cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	private Offerta offertaStage;
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="stageDip")
	private List<Dipendente> tutorStage = new LinkedList<Dipendente>();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getDurata() {
		return durata;
	}

	public void setDurata(int durata) {
		this.durata = durata;
	}

	public String getDatainizio() {
		return datainizio;
	}

	public void setDatainizio(String datainizio) {
		this.datainizio = datainizio;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Offerta getOffertaStage() {
		return offertaStage;
	}

	public void setOffertaStage(Offerta offertaStage) {
		this.offertaStage = offertaStage;
	}

	public List<Dipendente> getTutorStage() {
		return tutorStage;
	}

	public void setTutorStage(List<Dipendente> tutorStage) {
		this.tutorStage = tutorStage;
	}
	
	@PreRemove
	public void removeReferences() {
		for(Dipendente d: tutorStage) {
			d.removeStageReferences(this);
		}
	}
}
