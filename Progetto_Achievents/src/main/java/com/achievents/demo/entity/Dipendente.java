package com.achievents.demo.entity;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Dipendente {
	@Id
	private String cf;
	private String nome, cognome;
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "IDUNIVERSITA")
	private Universita uniDip;
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "IDAZIENDA", referencedColumnName="PARTITAIVA")
	private Azienda aziendaDip;
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "TUTORAGGIO", joinColumns = @JoinColumn(name = "CF_TUTOR", referencedColumnName = "CF"), inverseJoinColumns = {
			@JoinColumn(name = "IDSTAGE", referencedColumnName = "IDSTAGE")})
	List<Stage> stageDip = new LinkedList<Stage>();

	public Dipendente() {
	}

	public Dipendente(String cf, String nome, String cognome) {
		super();
		this.cf = cf;
		this.nome = nome;
		this.cognome = cognome;
	}

	public String getCf() {
		return cf;
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Universita getUniDip() {
		return uniDip;
	}

	public void setUniDip(Universita uniDip) {
		this.uniDip = uniDip;
	}

	public Azienda getAziendaDip() {
		return aziendaDip;
	}

	public void setAziendaDip(Azienda aziendaDip) {
		this.aziendaDip = aziendaDip;
	}

	public List<Stage> getStageDip() {
		return stageDip;
	}

	public void setStageDip(List<Stage> stageDip) {
		this.stageDip = stageDip;
	}

	public void removeStageReferences(Stage stage) {
		for (Iterator<Stage> it = stageDip.iterator(); it.hasNext();) {
			Stage s = it.next();
            if(s.getId() == stage.getId()){
                it.remove();
            }
        }
	}
	
	public void addStage(Stage stage) {
		this.stageDip.add(stage);
	}
	
	public void removeStage(Stage stage) {
		this.stageDip.remove(stage);
	}

}
