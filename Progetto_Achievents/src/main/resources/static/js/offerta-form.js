function validateDate(inputText){
	let dateformat = /^\d{4}[\-](0?[1-9]|1[012])[\-](0[1-9]|[12][0-9]|3[01])$/;
	
	if(inputText.value.match(dateformat)){
		return true;
	}
  	else{
  		return false;
  	}
}

function validateForm(){
	
	let inputs = document.getElementsByClassName("inputs");
	for(let i=0;i<inputs.length;i++){
		inputs[i].style.removeProperty('border');
	}
	
	let empty = false;
	
	let errorText = document.createElement('p');
	errorText.style.color = 'red';
	errorText.innerHTML = "Compilare tutti i campi."
	
	let form = document.getElementById("form");
	form.removeChild(form.lastChild);
	
	for(let i=0;i<inputs.length;i++){
		if(inputs[i].value !== ""){
			continue;
		}else{
			inputs[i].style.border = "1px solid red";
			empty = true;
		}
	}
	
	if(((document.getElementById("durata").value === "0") || (document.getElementById("durata").value === "")) 
			&& (document.getElementById("datainizio").value !== "")){
		
		document.getElementById("durata").style.border = "1px solid red";
		
		if(document.getElementById("datainizio").value === ""){
			document.getElementById("datainizio").style.border = "1px solid red";
		}
		
		form.appendChild(errorText);
		return false;
	}
	else if((document.getElementById("datainizio").value !== "") && (!validateDate(document.getElementById("datainizio")))){
		
		errorText.innerHTML = "Data non valida."
		form.appendChild(errorText);
		document.getElementById("datainizio").style.border = "1px solid red";
		return false;
		
	}else{
		if(document.getElementById("durata").value !== "0"){
			if(document.getElementById("datainizio").value === ""){
				document.getElementById("datainizio").style.border = "1px solid red";
				empty = true;
			}
		}
	}
	
	if(empty){
		form.appendChild(errorText);
		return false;
	}
	
			
	
}