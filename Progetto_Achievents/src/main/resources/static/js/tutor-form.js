function show() {
    var x = document.getElementById("tutors");
    var y = document.getElementById("nascondiTutor");
    var z = document.getElementById("mostraTutor");
    
    x.style.display = "block";
    y.style.display = "block";
    z.style.display = "none";
}

function hide() {
    var x = document.getElementById("tutors");
    var y = document.getElementById("mostraTutor");
    var z = document.getElementById("nascondiTutor");
    
    x.style.display = "none";
    y.style.display = "block";
    z.style.display = "none";
}

function validateForm(){
	
	let empty = false;
	let errorText = document.createElement('p');
	errorText.style.color = 'red';
	
	let form = document.getElementById("form");
	form.removeChild(form.lastChild);
	
	let inputs = document.getElementsByClassName("form-text");
	
	for(let i=0;i<inputs.length;i++){
		if(inputs[i].value !== ""){
			continue;
		}else{
			inputs[i].style.border = "1px solid red";
			empty = true;
		}
	}
	
	if(empty){
		errorText.innerHTML = "Compilare tutti i campi."
		form.appendChild(errorText);
		return false;
	}
	if(document.getElementById("cf").value.length !== 16){
		
		errorText.innerHTML = "CF non valido."
		form.appendChild(errorText);
		document.getElementById("cf").style.border = "1px solid red";
	    return false;
	}
}