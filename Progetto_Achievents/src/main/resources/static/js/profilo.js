function validateForm(){
	
	let empty = false;
	
	let piva = document.getElementById("partitaiva");
	
	let inputs = document.getElementsByClassName("inputs");
	for(let i=0;i<inputs.length;i++){
		inputs[i].style.removeProperty('border');
	}
	
	let form = document.getElementById("form");
	form.removeChild(form.lastChild);
	
	let errorText = document.createElement('p');
	errorText.style.color = 'red';
	errorText.innerHTML = "Compilare tutti i campi."
		
	for(let i=0;i<inputs.length;i++){
		if(inputs[i].value === ""){
			inputs[i].style.border = "1px solid red";
			empty = true;
		}
	}
	
	if(empty){
		form.appendChild(errorText);
		return false;
	}
	
	if((piva === null) || (piva.value.length !== 11)){
		errorText.innerHTML = "Partita IVA non valida."
		form.appendChild(errorText);
		return false;
	}
}