function validateForm(){
	
	let empty = false;
	
	let inputs = document.getElementsByClassName("form-text");
	
	for(let i=0;i<inputs.length;i++){
		if(inputs[i].value !== ""){
			continue;
		}else{
			inputs[i].style.border = "1px solid red";
			empty = true;
		}
	}
	
	if(empty){
		document.getElementById("errorMessage").innerHTML = "Compilare tutti i campi.";
		document.getElementById("errore").style.display = "block";
		return false;
	}
	
	if(document.getElementById("password").value !== document.getElementById("confirmPass").value){
		
		document.getElementById("errorMessage").innerHTML = "Le due password non coincidono.";
		document.getElementById("errore").style.display = "block";
		document.getElementById("password").style.border = "1px solid red";
		document.getElementById("password").style.border = "1px solid red";
	    return false;
	    
	}
	
	if(document.getElementById("tipo").value === ""){
		
		document.getElementById("errorMessage").innerHTML = "Scegliere il tipo di utenza.";
		document.getElementById("errore").style.display = "block";
		document.getElementById("tipo").style.border = "1px solid red";
		return false;
		
	}
}