function validateDate(inputText){
	let dateformat = /^\d{4}[\-](0?[1-9]|1[012])[\-](0[1-9]|[12][0-9]|3[01])$/;
	
	if(inputText.value.match(dateformat)){
		return true;
	}
  	else{
  		return false;
  	}
}

function validateForm(){
	let empty = false;
	
	let errorText = document.createElement('p');
	errorText.style.color = 'red';
	
	let form = document.getElementById("form");
	form.removeChild(form.lastChild);
	let inputs = document.getElementsByClassName("form-text");
	
	for(let i=0;i<inputs.length;i++){
		inputs[i].style.removeProperty('border');
	}
	
	for(let i=0;i<inputs.length;i++){
		if(inputs[i].value !== ""){
			continue;
		}else{
			inputs[i].style.border = "1px solid red";
			empty = true;
		}
	}
	
	if(empty){
		errorText.innerHTML = "Compilare tutti i campi."
		form.appendChild(errorText);
		return false;
	}
	
	if(!validateDate(document.getElementById("data"))){
		errorText.innerHTML = "Data non valida."
		form.appendChild(errorText);
		document.getElementById("data").style.border = "1px solid red";
		return false;
	}
}