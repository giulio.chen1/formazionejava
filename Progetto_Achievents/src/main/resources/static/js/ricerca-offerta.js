function validateDate(inputText){
	let dateformat = /^\d{4}[\-](0?[1-9]|1[012])[\-](0[1-9]|[12][0-9]|3[01])$/;
	
	if(inputText.value.match(dateformat)){
		return true;
	}
  	else{
  		return false;
  	}
}

function validateForm(){
	
	document.getElementById("durata").style.removeProperty('border');
	
	let errorText = document.createElement('p');
	errorText.style.color = 'red';
	
	let form = document.getElementById("form");
	form.removeChild(form.lastChild);
	
	if(!document.getElementById("durata").value.match(/^[0-9]+$/)){
		
		document.getElementById("durata").style.border = "1px solid red";
		errorText.innerHTML = "Il campo durata deve contenere un numero intero"
		form.appendChild(errorText);
		return false;
	}
	
			
	
}