function validateDate(inputText){
	let dateformat = /^\d{4}[\-](0?[1-9]|1[012])[\-](0[1-9]|[12][0-9]|3[01])$/;
	
	if(inputText.value.match(dateformat)){
		return true;
	}
  	else{
  		return false;
  	}
}

function validateForm(){
	
	let inputs = document.getElementsByClassName("inputs");
	for(let i=0;i<inputs.length;i++){
		inputs[i].style.removeProperty('border');
	}
	
	let empty = false;
	
	let errorText = document.createElement('p');
	errorText.style.color = 'red';
	
	let form = document.getElementById("form");
	form.removeChild(form.lastChild);
	let lavoro = document.getElementsByClassName("lavoro");
	
	for(let i=0;i<lavoro.length;i++){
		if(lavoro[i].value !== ""){
			continue;
		}else{
			lavoro[i].style.border = "1px solid red";
			empty = true;
		}
	}
	
	if(empty){
		errorText.innerHTML = "Compilare tutti i campi."
		form.appendChild(errorText);
		return false;
	}
	
	if(!document.getElementById("durata").value.match(/^[0-9]+$/)){
		
		document.getElementById("durata").style.border = "1px solid red";
		errorText.innerHTML = "Il campo durata deve contenere un numero intero"
		form.appendChild(errorText);
		return false;
	}
	
	if(((document.getElementById("durata").value === "0") || (document.getElementById("durata").value === "")) 
			&& ((document.getElementById("datainizio").value !== "") || (document.getElementById("tipoStage").value !== ""))){
		
		document.getElementById("durata").style.border = "1px solid red";
		
		if(document.getElementById("datainizio").value === ""){
			document.getElementById("datainizio").style.border = "1px solid red";
		}
			
		if(document.getElementById("tipoStage").value === ""){
			document.getElementById("tipoStage").style.border = "1px solid red";
		}
		
		errorText.innerHTML = "Compilare tutti i campi dello stage."
		form.appendChild(errorText);
		return false;
	}
	else if((document.getElementById("datainizio").value !== "") && (!validateDate(document.getElementById("datainizio")))){
		
		errorText.innerHTML = "Data non valida."
		form.appendChild(errorText);
		document.getElementById("datainizio").style.border = "1px solid red";
		return false;
		
	}else{
		if(document.getElementById("durata").value !== "0"){
			if(document.getElementById("datainizio").value === ""){
				document.getElementById("datainizio").style.border = "1px solid red";
				empty = true;
			}
				
			if(document.getElementById("tipoStage").value === ""){
				document.getElementById("tipoStage").style.border = "1px solid red";
				empty = true;
			}
		}
		
		if(empty){
			errorText.innerHTML = "Compilare tutti i campi dello stage."
			form.appendChild(errorText);
			return false;
		}
	}
	
			
	
}