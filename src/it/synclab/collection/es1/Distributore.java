package it.synclab.collection.es1;
import java.util.ArrayList;

public class Distributore {
	private ArrayList<Bevanda> bevande;
	
	public Distributore(ArrayList<Bevanda> bevande) {
		this.bevande = bevande;
	}
	
	public void aggiungiBevanda(Bevanda b) {
		this.bevande.add(b);
	}

	public ArrayList<Bevanda> getBevande() {
		return bevande;
	}

}
