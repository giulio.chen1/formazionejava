package it.synclab.collection.es1;
import java.util.ArrayList;
import java.util.Scanner;

public class TestBevande {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		ArrayList<Bevanda> bevande = new ArrayList<Bevanda>();	//Lista bevande
		String input = new String();
		char in;

		try {
			//Aggiungo bevande
			Distributore d = new Distributore(bevande);
			System.out.println("Quante bevande vorresti aggiungere?");
			input = scanner.nextLine().trim(); 
			int numBev = Integer.valueOf(input); //Potrebbe lanciare un'eccezione
			for(int i=0;i<numBev;i++) {
				System.out.println("Inserire ID, nome e prezzo della bevanda da aggiungere");
				input = scanner.nextLine().trim();
				String[] inputs = input.split("\\s+");
				//Input check
				if(inputs.length != 3) throw new ArrayIndexOutOfBoundsException();
				d.aggiungiBevanda(new Bevanda(inputs[0].charAt(0), inputs[1], Double.valueOf(inputs[2])));
			}
			
			String bevDisponibili = new String();  
			//Constructor input check
			for(Bevanda b: bevande) {
				if(!Character.toString(b.getId()).matches("[A-Z]") || !b.getNome().matches("[a-zA-Z]+") || b.getPrezzo() < 0.01) throw new BevandaNonValida();
				bevDisponibili += "ID: " + b.getId() + ", bevanda: " + b.getNome() + ", prezzo: " + b.getPrezzo() + "\n"; 
			}
			System.out.println(bevDisponibili);	//Bevande disponibili
			System.out.println("Inserire ID bevanda per controllare il prezzo");
			input = scanner.nextLine().replaceAll("\\s+", "");
			
			//User input check
			if(!input.matches("[A-Z]")) throw new BevandaNonValida();
			for(Bevanda b: bevande) {
				if(b.getId() == input.charAt(0)) {
					System.out.println("Prezzo: " + b.getPrezzo());
					System.exit(1);
				} 
			}
			throw new BevandaNonValida();
		}catch(BevandaNonValida e) {
			System.out.println("Errore: bevanda non valida");
		}catch(NullPointerException e) {
			System.out.println("Errore: parametri non validi");
		}catch(NumberFormatException e) {
			System.out.println("Errore: input non valido");
		}catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("Errore: input non valido");
		}
	}

}
