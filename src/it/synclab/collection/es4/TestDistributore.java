package it.synclab.collection.es4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;


public class TestDistributore {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String input = new String();//tipi di bevande iniziali
		//Colonne di bevande
		HashMap<Character, ArrayList<Bevanda>> colonneBev = new HashMap<Character, ArrayList<Bevanda>>();
		int maxBevande = 30; //Max numero bevande per colonna		
		boolean aggiungere = true;
		boolean continua = true;
		int k = 0; //Numero di colonne 
		try {
			//Aggiungo bevande
			System.out.println("Inserire ID, tipo di bevanda, prezzo delle bevande");
			//Ho messo un try-catch all'interno per gestire il limite delle colonne
			//Cosicch� il programma possa continuare anche arrivando al limite
			try {	
				while(aggiungere) {
					System.out.print("Bevanda " + (k+1) + ": ");
					input = scanner.nextLine().trim();
					if(input.length() == 0) aggiungere = false;
					else if(k == 10) {
						throw new LimiteRaggiunto();	//Lancio un'eccezione se non ci sono pi� colonne libere
					}
					else {
						String[] inputs = input.split("\\s+");
						if(inputs.length != 3) throw new NumberFormatException();
						System.out.print("Lattine da caricare: ");
						//Potrebbe generare NumberFormatException, gestita dal try-catch esterno
						int lattine = Integer.valueOf(scanner.nextLine().trim());  
						if(lattine > maxBevande) throw new LimiteRaggiunto(); //Lancio un'eccezione se non ci sono slot liberi
						ArrayList<Bevanda> bevande = new ArrayList<Bevanda>();
						for(int j=0;j<lattine;j++) {
							bevande.add(new Bevanda(inputs[0].charAt(0), inputs[1], Double.valueOf(inputs[2])));
						}
						colonneBev.put(inputs[0].charAt(0), bevande);
						k++;
					}
				}
			}catch(LimiteRaggiunto e) {}
			
			if(colonneBev.size() == 0) throw new DistributoreVuoto();
			Distributore d = new Distributore(colonneBev);
			
			//Creo delle map per memorizzare nome e prezzo delle bibite 
			HashMap<Character, String> nomeBevande = new HashMap<Character, String>();
			HashMap<Character, Double> prezzoBevande = new HashMap<Character, Double>();
			String bevDisponibili = new String();  
			
			int j = 0; //j-esima bevanda
			//Constructor input check
			for(ArrayList<Bevanda> tipoBevande: colonneBev.values()) {
				for(Bevanda b: tipoBevande) {
					if(!Character.toString(b.getId()).matches("[A-Z]") || !b.getNome().matches("[a-zA-Z]+") || b.getPrezzo() < 0.01) throw new NullPointerException();
				}
				//Bevande disponibili
				bevDisponibili += "N� Colonna: " + (j+1) + ", ID: " + tipoBevande.get(0).getId() + ", bevanda: " + tipoBevande.get(0).getNome()
						+ ", lattine: " + tipoBevande.size() + ", prezzo: " + tipoBevande.get(0).getPrezzo() + "\n";
				j++;
				nomeBevande.put(tipoBevande.get(0).getId(), tipoBevande.get(0).getNome());
				prezzoBevande.put(tipoBevande.get(0).getId(), tipoBevande.get(0).getPrezzo());
			}
			
			System.out.println("--------------------------Utente----------------------------");
			System.out.println("Inserire ID e credito della tessera");
			input = scanner.nextLine().trim();
			String[] inputs = input.split("\\s+");
			if(inputs.length != 2) throw new Exception();
			Tessera t = new Tessera(Integer.valueOf(inputs[0]), Integer.valueOf(inputs[1]));
			
			//Il programma ripete la richiesta finch� l'utente inserisce "si"
			while(continua) {
				//Stampo le bevande disponibili
				System.out.println(bevDisponibili); 
				//Chiedo all'utente di inserire l'id della bibita da acquistare
				System.out.println("Inserire l'ID della bevanda da prendere");
				//Input check
				input = scanner.nextLine().trim();
				if(!input.matches("[A-Z]")) throw new BevandaNonValida();
				//Erogazione
				d.eroga(t, input.charAt(0));	
				System.out.println("Credito residuo: " + t.getCredito());
				//Chiedo se vuole continuare
				System.out.println("Acquistare altro? Inserire 'si' per continuare");
				input = scanner.nextLine().trim();
				//Input check
				if(input.equalsIgnoreCase("si")) {
					j = 0;
					bevDisponibili = "";
					for(char id: colonneBev.keySet()) {
						//Bevande disponibili
						bevDisponibili += "N� Colonna: " + (j+1) + ", ID: " + id + ", bevanda: " + nomeBevande.get(id)
								+ ", lattine: " + d.lattineDisponibili(id) + ", prezzo: " + prezzoBevande.get(id) + "\n";
						j++;
					}
				}else {
					continua = false;
				}
			}
			System.out.println("--------------------------Admin----------------------------");
			
			//Finch� l'utente inserisce "si", il programma chiede continuamente di caricare il distributore
			continua = true;
			while(continua) {
				
				System.out.println("Inserire id, nome e numero di lattine della bevanda per aggiornare");
				input = scanner.nextLine().trim();
				String[] parametri = input.split("\\s+");
				//Input check
				if(parametri.length != 3) throw new Exception();
				boolean check = false;
				//Input check
				if(Integer.valueOf(parametri[2]) > 30) throw new LimiteRaggiunto(); //Se il numero di lattine > 30, lancio un'eccezione
				if(Integer.valueOf(parametri[2]) < 0) throw new Exception(); //Se il numero di lattine � negativo, lancio un'eccezione
				if(!nomeBevande.get(parametri[0].charAt(0)).equals(parametri[1])) throw new Exception();
				//Aggiorno la colonna con i parametri inseriti
				d.aggiornaColonna(parametri[0].charAt(0), new Bevanda(parametri[0].charAt(0), parametri[1], 
					prezzoBevande.get(parametri[0].charAt(0))), Integer.valueOf(parametri[2]));
				check = true;
				if(!check) throw new BevandaNonValida();	//Se non esiste tale id bevanda, lancio un'eccezione
				//info bevande disponibili
				bevDisponibili = "";
				j = 0;
				for(char id: colonneBev.keySet()) {
					//Bevande disponibili
					bevDisponibili += "N� Colonna: " + (j+1) + ", ID: " + id + ", bevanda: " + nomeBevande.get(id)
							+ ", lattine: " + d.lattineDisponibili(id) + ", prezzo: " + prezzoBevande.get(id) + "\n";
					j++;
				}
				System.out.println(bevDisponibili);	//Stampo le info bevande
				//Chiedo se vuole continuare
				System.out.println("Ripetere? Inserire 'si' per ripetere");
				input = scanner.nextLine().trim();
				if(!input.equalsIgnoreCase("si")) continua = false;  //Input check
			}
		}catch(BevandaNonValida e) {
		}catch(DistributoreVuoto e){
		}catch(CreditoInsufficiente e) {
		}catch(BevandaEsaurita e) {
		}catch(LimiteRaggiunto e) {
		}catch(NullPointerException e) {
			System.out.println("Errore: input del costruttore non valido 1");
		}catch(NumberFormatException e) {
			System.out.println("Errore: input del costruttore non valido 2");
		}catch(Exception e) {
			System.out.println("Errore: input non valido");
		}
	}

}
