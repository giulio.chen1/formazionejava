package it.synclab.collection.es4;
import java.util.ArrayList;
import java.util.HashMap;


public class Distributore {
	private HashMap<Character, ArrayList<Bevanda>> colBevande; //Colonne di bevande
	
	public Distributore(HashMap<Character, ArrayList<Bevanda>> colBevande) {
		this.colBevande = colBevande;
	}

	public HashMap<Character, ArrayList<Bevanda>> getAllBevande() {
		return colBevande;
	}
	
	public int eroga(Tessera t, char idBev) throws BevandaEsaurita, CreditoInsufficiente {
		if(colBevande.get(idBev).size() != 0) {
			if(Double.valueOf(t.getCredito()) - colBevande.get(idBev).get(0).getPrezzo() >= 0) {
				t.setCredito(Double.valueOf(t.getCredito()) - colBevande.get(idBev).get(0).getPrezzo());
				colBevande.get(idBev).remove(0);
				return colBevande.get(idBev).size();
			}else if(Double.valueOf(t.getCredito()) - colBevande.get(idBev).get(0).getPrezzo() < 0) 
				throw new CreditoInsufficiente();
		}
		throw new BevandaEsaurita();
	}
	
	public void aggiornaColonna(char idBev, Bevanda b, int numLatt) throws BevandaNonValida {
		if(this.colBevande.containsKey(idBev)) {
			this.colBevande.get(idBev).removeAll(this.colBevande.get(idBev));
			for(int i=0;i<numLatt;i++) {
				this.colBevande.get(idBev).add(b);
			}
		}else throw new BevandaNonValida();
	}
	
	public int lattineDisponibili(char id) {
		int lattDisp = 0;
		for(Bevanda b: colBevande.get(id)) 
			lattDisp++;
		
		return lattDisp;
	}
}
