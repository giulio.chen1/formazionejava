package it.synclab.collection.altro;
import java.util.LinkedList;

public class Es4 {
	
	//Creo una lista di liste dove la prima contiene 0, la seconda 0 e 1, la terza 0 1 2, e cos� via fino a n-1
	public static LinkedList<LinkedList<Integer>> insiemeDiInsiemi(int n){
		LinkedList<LinkedList<Integer>> listOfLists = new LinkedList<LinkedList<Integer>>();
		for(int i=0;i<n;i++) {
			LinkedList<Integer> list = new LinkedList<Integer>();
			for(int j=0;j<=i;j++) {
				list.add(j);
			}
			listOfLists.add(list);
		}
		return listOfLists;
	}
	
	//Stampo la lista di liste
	public static void stampa(LinkedList<LinkedList<Integer>> a) {
		for(LinkedList<Integer> list: a) {
			System.out.println(list);
		}
	}
	
	public static void main(String[] args) {
		stampa(insiemeDiInsiemi(5));

	}

}
