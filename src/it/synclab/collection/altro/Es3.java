package it.synclab.collection.altro;
import java.util.LinkedList;
import java.util.Collections;

public class Es3 {
	
	//Unisce due liste e ordina i suoi elementi
	public static LinkedList<Integer> mergeOrdinato(LinkedList<Integer> a, LinkedList<Integer> b){
		a.addAll(b);
		Collections.sort(a);
		return a;
	}
	
	//Creo due liste ordinate, le unisco e stampo la lista combinata
	public static void main(String[] args) {
		LinkedList<Integer> l1 = new LinkedList<Integer>();
		LinkedList<Integer> l2 = new LinkedList<Integer>();
		for(int i=0;i<10;i++) {
			l1.add((int) (Math.random() * 100));
			l2.add((int) (Math.random() * 100));
		}
		Collections.sort(l1);
		Collections.sort(l2);
		System.out.println("Lista 1: " + l1);
		System.out.println("Lista 2: " + l2);
		System.out.println("Lista combinata: " + mergeOrdinato(l1, l2));

	}

}
