//Esercizi del file Esercitazione 3.1.docx

package it.synclab.collection.altro;
import java.util.LinkedList;
import java.util.Collections;
import java.util.Iterator;

public class Es1 {
	
	//Creo una lista di n interi, con ogni intero random compreso tra 0 e max
	public static LinkedList<Integer> creaRandom(int n, int max){
		LinkedList<Integer> lista = new LinkedList<Integer>();
		for(int i=0;i<n;i++) {
			lista.add((int) (Math.random() * max));
		}
		return lista;
	}
	
	//Stampa gli elementi della lista data
	public static void stampa(LinkedList<Integer> lista) {
		Iterator it = lista.iterator();
		while(it.hasNext()) {
			System.out.print(it.next() + " ");
		}
		System.out.println();
	}
	
	//Stampo gli elementi di una lista ordinata/non ordinata
	public static void provaEs1() throws ArithmeticException {
		LinkedList<Integer> lista = new LinkedList<Integer>();
		lista = creaRandom(20, 30);
		if(lista.size() == 0) throw new ArithmeticException();
		//Non ordinata
		System.out.print("Non ordinata: ");
		stampa(lista);
		//Ordinata
		Collections.sort(lista);
		System.out.print("Ordinata:     ");
		stampa(lista);
	}
	
	public static void main(String[] args) {
		try {
			provaEs1();
		}catch(ArithmeticException e) {
			System.out.println("Errore: lista di lunghezza 0");
		}
	}
	
}
