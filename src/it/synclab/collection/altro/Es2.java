//Non ho ben capito il collegamento tra la prima parte e la seconda dell'esercizio

package it.synclab.collection.altro;
import java.util.LinkedList;
import java.util.Collections;

public class Es2 {
	
	//Creo una lista di interi random crescenti
	public static LinkedList<Integer> creaRandomCrescente(int n){
		LinkedList<Integer> list = new LinkedList<Integer>();
		list.add((int) (Math.random() * 100));
		for(int i=1;i<n;i++) {
			list.add(list.get(i-1) + ((int) (Math.random() * 100)));
		}
		Collections.sort(list);
		return list;
	}
	
	//Converte una lista di stringhe in interi
	public static LinkedList<Integer> parseString(LinkedList<String> a){
		LinkedList<Integer> list = new LinkedList<Integer>();
		for(String str: a) {
			list.add(Integer.parseInt(str));
		}
		return list;
	}
	
	public static void main(String[] args) {
		//---------Prima parte--------------
		LinkedList<Integer> listRandom = creaRandomCrescente(10);
		System.out.println("-------------Parte 1-------------");
		System.out.println("\nLista random crescente: " + listRandom);
		
		//---------Seconda parte------------
		LinkedList<String> listS = new LinkedList<String>();
		LinkedList<Integer> listI = new LinkedList<Integer>();
		System.out.println("\n-------------Parte 2-------------");
		for(int i=0;i<10;i++) {
			listS.add(i + "");
		}
		listS.add("qwe"); //Genera eccezione, pu� essere cancellata
		try {
			listI = parseString(listS);
			System.out.println("\nConversione lista di stringhe a interi riuscita");
		}catch(NumberFormatException e) {
			System.out.println("\nConversione lista di stringhe a interi fallita");
		}
	}
}
