package it.synclab.collection.es3;

public class Bevanda {
	private char id;
	private String nome;
	private double prezzo;
	
	public Bevanda(char id, String nome, double prezzo) {
		this.id = id;
		this.nome = nome;
		this.prezzo = prezzo;
	}

	public char getId() {
		return id;
	}

	public void setId(char id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}
	
}
