package it.synclab.collection.es3;
import java.util.ArrayList;
import java.util.Scanner;

public class TestBevande {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		ArrayList<Bevanda>[] colonneBev = new ArrayList[4]; //Colonne di bevande
		String input = new String();
		
		boolean continua = true;
		
		for(int i=0;i<colonneBev.length;i++) {	//Inizializzo le colonne
			colonneBev[i] = new ArrayList<Bevanda>();
		}
		try {
			//Aggiungo bevande
			System.out.println("Inserire ID, nome, prezzo di 4 bevande");
			for(int i=0;i<4;i++) {
				System.out.print("Bevanda " + (i+1) + ": ");
				input = scanner.nextLine().trim();
				String[] inputs = input.split("\\s+");
				if(inputs.length != 3) throw new NumberFormatException();
				colonneBev[i].add(new Bevanda(inputs[0].charAt(0), inputs[1], Double.valueOf(inputs[2])));
			}
			
			Distributore d = new Distributore(colonneBev);
			String bevDisponibili = new String();  
			
			//Constructor input check
			for(int i=0;i<colonneBev.length;i++) {
				for(Bevanda b: colonneBev[i]) {
					if(!Character.toString(b.getId()).matches("[A-Z]") || !b.getNome().matches("[a-zA-Z]+") || b.getPrezzo() < 0.01) throw new BevandaNonValida();
					bevDisponibili += "N� Colonna: " + (i+1) + ", id: " + colonneBev[i].get(0).getId() + ", bevanda: " + b.getNome() + ", lattine: " + colonneBev[i].size() + ", prezzo: " + b.getPrezzo() + "\n"; 
				}
			}
			System.out.println("\n" + bevDisponibili);	//Bevande disponibili
			
			//Finch� l'utente inserisce "si", il programma chiede continuamente di caricare il distributore
			while(continua) {
				
				System.out.println("Inserire il tipo di bevanda, numero di colonna e di lattine per aggiornare");
				input = scanner.nextLine().trim();
				String[] parametri = input.split("\\s+");
				//Input check
				if(parametri.length != 3) throw new NumberFormatException();
				boolean check = false;
				for(int i=0;i<colonneBev.length;i++) {
					if(parametri[0].equalsIgnoreCase(colonneBev[i].get(0).getNome()) && Integer.valueOf(parametri[1]) == (i+1) 
							&& Integer.valueOf(parametri[2]) > 0) {
									//Aggiorna la colonna del distributore con i parametri inseriti
									d.aggiornaColonna((new Bevanda(colonneBev[i].get(0).getId(), colonneBev[i].get(0).getNome(), colonneBev[i].get(0).getPrezzo())), 
											Integer.valueOf(parametri[1]), Integer.valueOf(parametri[2]));
									check = true;
					}
				}
				if(!check) throw new NumberFormatException();	//Se non esiste tale bevanda/colonna, lancio un'eccezione
				//info bevande disponibili
				bevDisponibili = "";
				for(int i=0;i<colonneBev.length;i++) {
					bevDisponibili += "N� Colonna: " + (i+1) + ", id: " + colonneBev[i].get(0).getId() + ", bevanda: " + colonneBev[i].get(0).getNome() 
							+ ", lattine: " + colonneBev[i].size() + ", prezzo: " + colonneBev[i].get(0).getPrezzo() + "\n"; 
				}
				System.out.println(bevDisponibili);	//Stampo le info bevande
				//Chiedo se vuole continuare
				System.out.println("Ripetere? Inserire 'si' per ripetere");
				input = scanner.nextLine().trim();
				if(!input.equalsIgnoreCase("si")) continua = false;  //Input check
			}
			
			System.out.println("--------------------Utente-------------------\n");
			//info bevande disponibili
			bevDisponibili = "";
			for(int i=0;i<colonneBev.length;i++) {
				bevDisponibili += "N� Colonna: " + (i+1) + ", id: " + colonneBev[i].get(0).getId() + ", bevanda: " + colonneBev[i].get(0).getNome() + ", prezzo: " + colonneBev[i].get(0).getPrezzo() + "\n"; 
			}
			System.out.println(bevDisponibili);	//Stampo le info bevande
			
			//Chiedo l'id della bevanda di cui si vuole vedere il numero di lattine disponibili
			System.out.println("Inserire ID bevanda per controllare le sue lattine disponibili ");
			input = scanner.nextLine().replaceAll("\\s+", "");
			
			//User input check
			if(!input.matches("[A-Z]")) throw new BevandaNonValida();
			//Stampo le lattine disponibili
			System.out.println("Lattine: " + d.lattineDisponibili(input.charAt(0)));
			
		}catch(BevandaNonValida e) {
			System.out.println("Errore: bevanda non valida");
		}catch(NullPointerException e) {
			System.out.println("Errore: input del costruttore non valido");
		}catch(NumberFormatException e) {
			System.out.println("Errore: parametri non validi");
		}
	}

}
