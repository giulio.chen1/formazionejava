package it.synclab.collection.es3;
import java.util.ArrayList;

public class Distributore {
	private ArrayList<Bevanda>[] colBevande; //Colonne di bevande
	
	public Distributore(ArrayList<Bevanda>[] colBevande) {
		this.colBevande = colBevande;
	}
	
	public void aggiornaColonna(Bevanda b, int numCol, int numLatt) throws BevandaNonValida {
		if(this.colBevande[numCol-1].get(0).getNome().equalsIgnoreCase(b.getNome())) {
			this.colBevande[numCol-1].removeAll(this.colBevande[numCol-1]);
			for(int i=0;i<numLatt;i++) {
				this.colBevande[numCol-1].add(b);
			}
		}else throw new BevandaNonValida();
	}

	public ArrayList<Bevanda>[] getAllBevande() {
		return colBevande;
	}
	
	public int lattineDisponibili(char id) {
		int lattDisp = 0;
		
		for(int i=0;i<colBevande.length;i++) 
			for(Bevanda b: colBevande[i]) 
				if(b.getId() == id) lattDisp++;
		
		return lattDisp;
	}
}
