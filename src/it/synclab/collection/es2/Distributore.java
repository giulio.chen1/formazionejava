package it.synclab.collection.es2;
import java.util.ArrayList;

public class Distributore {
	private ArrayList<Tessera> tessere;
	
	public Distributore(ArrayList<Tessera> tessere) {
		this.tessere = tessere;
	}

	public ArrayList<Tessera> getTessere() {
		return tessere;
	}
	
	public void setTessere(ArrayList<Tessera> tessere) {
		this.tessere = tessere;
	}

	public double leggiCredito(int id) {
		for(Tessera t: this.tessere) 
			if(t.getId() == id) return t.getCredito();
		return -1;
	}
	
	public void caricaTessera(double credito, int id) {
		for(Tessera t: this.tessere) 
			if(t.getId() == id) t.setCredito(t.getCredito() + credito);;
	}
}
