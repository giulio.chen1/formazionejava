package it.synclab.collection.es2;
import java.util.ArrayList;
import java.util.Scanner;


public class TestTessere {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String idTess = "", credito = "";
		ArrayList<Tessera> tessere = new ArrayList<Tessera>();
		String input = "";
		
		try {
			
			System.out.println("Quante tessere vorresti aggiungere?");
			input = scanner.nextLine().trim(); 
			int numTess = Integer.valueOf(input); //Potrebbe lanciare un'eccezione
			for(int i=0;i<numTess;i++) {
				System.out.println("Inserire ID, credito della tessera da aggiungere");
				input = scanner.nextLine().trim();
				String[] inputs = input.split("\\s+");
				//Input check
				if(inputs.length != 2) throw new Exception();
				tessere.add(new Tessera(Integer.valueOf(inputs[0]), Double.valueOf(inputs[1])));
			}
			Distributore d = new Distributore(tessere);
			String tessDisponibili = new String();  
			
			
			//Constructor input check
			for(Tessera t: tessere) {
				int k=0;
				if(!String.valueOf(t.getId()).matches("[0-9]+")) throw new TesseraNonValida();
				else if(t.getCredito() < 0.01) throw new Exception();
				tessDisponibili += "ID: " + t.getId() + ", credito: " + t.getCredito() + "\n";  
				for(Tessera t2: tessere) {
					if(t.getId() == t2.getId()) k++;
				}
				if(k > 1) throw new TesseraNonValida();
			}
			//User input check
			System.out.println(tessDisponibili);	//Stampo gli id delle tessere
			//Chiedo quale tessera si vuole leggere
			System.out.println("Inserire ID tessera di cui si vuole leggere il credito");
			idTess = scanner.nextLine().trim();
			int i = Integer.valueOf(idTess);
			//Se non esiste l'id, lancio l'eccezione
			if(d.leggiCredito(i) == -1) 
				throw new TesseraNonValida();
			else
				System.out.println(d.leggiCredito(i));
			//Chiedo il credito da caricare
			System.out.println("Inserire numero crediti da caricare su questa tessera");
			credito = scanner.nextLine().trim();
			double j = Integer.valueOf(credito);
			d.caricaTessera(j, i); //Carico la tessera 
			tessDisponibili = "";
			//Stampo il credito aggiornato
			for(Tessera t: tessere) {
				if(t.getId() == i) System.out.println("ID: " + i + ", credito: " + t.getCredito() + "\n");
			}
			System.out.println(tessDisponibili);
		}catch(TesseraNonValida e) {
			System.out.println("Tessera non valida");
		}catch(Exception e) {
			System.out.println("Input non valido");
		}

	}

}
