package it.synclab.collection.es2;

public class Tessera {
	private int id;
	private double credito;
	
	public Tessera(int id, double credito) {
		this.id = id;
		this.credito = credito;
	}

	public int getId() {
		return id;
	}

	public double getCredito() {
		return credito;
	}

	public void setCredito(double credito) {
		this.credito = credito;
	}
}
