// Scrivere un programma / metodo che data una stringa in input la stampi al contrario. Per esempio, se si
// immette la stringa &quot;Viva Java&quot;, il programma stampa &quot;avaJ aviV&quot;

package it.synclab.stringhe;
import java.util.Scanner;

public class Es1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String str = new String();
		String reverse = new String();
		
		System.out.println("Inserire una stringa");
		str = scanner.nextLine().trim();
		//Input check
		if(str.length() != 0) {
			for(int i=str.length()-1;i>=0;i--) {
				//reverse += str.substring(i, i+1);
				reverse += str.charAt(i);
			}
			//Stampo l'inverso
			System.out.println(reverse);
		}else System.out.println("Errore: stringa vuota");
		
	}
}
