// Scrivere un programma / metodo che data una stringa in input ne stampi le sole vocali. Per esempio, se si
// immette la stringa &quot;Viva Java&quot;, il programma stampa &quot;iaaa&quot;.

package it.synclab.stringhe;

import java.util.Scanner;

public class Es2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String str = new String();
		String str2 = new String();
		System.out.println("Inserire una stringa");
		str = scanner.nextLine().trim();
		//Input check
		if(str.length() == 0) {
			System.out.println("Errore: stringa vuota");
			System.exit(1);
		}
		//Controllo se ci sono vocali
		for(int i=0;i<str.length();i++) {
			if((str.charAt(i) == 'a')||(str.charAt(i) == 'e')||(str.charAt(i) == 'i')||(str.charAt(i) == 'o')||(str.charAt(i) == 'u'))
				str2 += str.charAt(i);
		}
		//Stampo il risultato
		if(str2.length() == 0) {
			System.out.println("Non ci sono vocali");
		}else {
			System.out.println(str2);
		}
	}
}
