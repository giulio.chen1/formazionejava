// Scrivere un programma / metodo che data una sequenza di stringhe, conclusa dalla stringa vuota, stampi
// la somma delle lunghezze delle stringhe che iniziano con una lettera maiuscola. Per esempio, se si
// immettono le stringhe &quot;Albero&quot;, &quot;foglia&quot;, &quot;Radici&quot;, &quot;Ramo&quot;, 
// &quot;fiore&quot; (e poi &quot;&quot; per finire), il programma stampa 16.


package it.synclab.stringhe;
import java.util.Scanner;


public class Es3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String str = new String();
		String strInput = new String("a");
		
		//Separo le stringhe con una virgola
		while(!strInput.matches("\\s+") && !strInput.equals("")) {
			System.out.println("Inserire una stringa");
			strInput = scanner.nextLine().replaceAll("\\s+", ",");
			str += strInput + ",";
		}
		
		//Conto le stringhe che hanno la lettera maiuscola
		int k=0;
		for(int i=0;i<str.length();i++) {
			if(((int)str.charAt(i) > 64) && ((int)str.charAt(i) < 91)) {
				int j=i;
				while(!str.substring(j, j+1).equals(",")) {
					j++;
					k++;
				}
			}
		}
		
		//Stampo la somma delle lunghezze delle stringhe con la lettera maiuscola
		System.out.println("Lunghezza di stringhe con maiuscole: " + k);
		System.out.println("Lunghezza di tutte le stringhe: " + str.replaceAll(",", "").length());
	}
}
