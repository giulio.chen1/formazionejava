//Realizzare un programma che sia in grado di valutare il volume di oggetti tridimensionali come cilindri e
//parallelepipedi basandosi sul valore della loro area e dell&#39;altezza. Delegare i metodi comuni, come il calcolo
//e la stampa del volume ad una classe astratta.

package it.synclab.astratte.es1;

public class Test3D {

	public static void main(String[] args) {
		Cilindro cil = new Cilindro(40, 5);
		Parallelepipedo par = new Parallelepipedo(30, 6);
		
		cil.stampaVolume(cil.calcVolume(cil.getArea(), cil.getAltezza()));
		par.stampaVolume(par.calcVolume(par.getArea(), par.getAltezza()));
	}

}
