package it.synclab.astratte.es1;

public class Parallelepipedo extends Oggetto3D{
	private double area, altezza;
	
	public Parallelepipedo(double area, double altezza) {
		this.area = area;
		this.altezza = altezza;
	}
	
	@Override
	public double calcVolume(double area, double altezza) {
		double volume = area * altezza;
		return volume;
	}

	@Override
	public void stampaVolume(double volume) {
		System.out.println(volume);
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public double getAltezza() {
		return altezza;
	}

	public void setAltezza(double altezza) {
		this.altezza = altezza;
	}
	
}
