package it.synclab.astratte.es1;

public abstract class Oggetto3D {
	
	public Oggetto3D() {}
	
	public abstract double calcVolume(double area, double altezza);
	
	public abstract void stampaVolume(double volume);
}
