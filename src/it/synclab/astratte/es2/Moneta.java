package it.synclab.astratte.es2;

public class Moneta extends Chance {
	
	public Moneta() {}
	
	@Override
	public void lancia() {
		int ris = (int)(Math.random() * 2);
		if(ris == 0) {
			System.out.println("Moneta: Testa");
		}else {
			System.out.println("Moneta: Croce");
		}
	}
}
