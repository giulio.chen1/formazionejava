/*Scrivere un programma che simuli il lancio di un dado e di una moneta stampandone il risultato; con e
senza l&#39;utilizzo di una classe astratta che rappresenti i comportamenti comuni degli oggetti dado e moneta
istanziati.*/

package it.synclab.astratte.es2;

public class TestChance {

	public static void main(String[] args) {
		
		//Con classe astratta
		Dado d1 = new Dado();
		d1.lancia();
		Moneta m1 = new Moneta();
		m1.lancia();
		
		//Senza classe astratta
		Dado2 d2 = new Dado2();
		d2.lancia();
		Moneta2 m2 = new Moneta2();
		m2.lancia();
	}

}
