package it.synclab.astratte.es2;

public class Dado extends Chance{
	
	public Dado() {}
	
	@Override
	public void lancia() {
		System.out.println("Dado: " + ((int)(Math.random()*6)+1));
	}
	
}
