package it.synclab.interfacce.es2;

public class Distributore implements Comparable {
	private String citta, proprietario, benzina;
	private int capacita;
	private double incasso;
	
	public Distributore(String citta, String proprietario, int capacita, String benzina) {
		this.citta = citta;
		this.proprietario = proprietario;
		this.capacita = capacita;
		this.benzina = benzina;
		this.incasso = 0;
	}
	
	@Override
	public int compareTo(Object dis) {
		Distributore distr = (Distributore) dis;
		return this.getCapacita() - distr.getCapacita();
	}
	
	public void eroga(int litri) {
		double incasso = this.getIncasso();
		switch(this.getBenzina()) {
			case "Benzina": incasso += (litri * 1.5);
			break;
			case "Diesel": incasso += (litri * 1.7);
			break;
			case "GPL": incasso += (litri * 1.4);
			break;
		}
		this.setIncasso(incasso);
		System.out.println("Incasso: " + incasso);
	}
	
	public double getIncasso() {
		return incasso;
	}

	public void setIncasso(double incasso) {
		this.incasso = incasso;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getProprietario() {
		return proprietario;
	}

	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}

	public String getBenzina() {
		return benzina;
	}

	public void setBenzina(String benzina) {
		this.benzina = benzina;
	}

	public int getCapacita() {
		return capacita;
	}

	public void setCapacita(int capacita) {
		this.capacita = capacita;
	}

	
}
