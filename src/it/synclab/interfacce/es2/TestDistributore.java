/*Scrivere un programma che sia in grado di istanziare degli oggetti &#39;distributori di benzina&#39; di cui sia nota la
citt�, il proprietario, la capacit� e la benzina attualmente contenuta nel distributore. Dell&#39;oggetto
Distributore, deve essere possibile simulare le operazioni di erogazione del carburante e dei corrispondenti
incassi. Implementa una interfaccia Comparable, in modo da consentire il confronto tra 2 distributori in
base alla capacit� del serbatoio di carburante.*/

package it.synclab.interfacce.es2;

public class TestDistributore {

	public static void main(String[] args) {
		//Creo due distributori con parametri citt�, proprietario, capacit�, e tipo di benzina
		Distributore d1 = new Distributore("Milano", "Giulio", 210000, "Diesel");
		Distributore d2 = new Distributore("Roma", "Luca", 220000, "Benzina");
		Distributore[] ds = new Distributore[] {d1, d2};
		//Input check
		try {
			for(int i=0;i<ds.length;i++) {
				if(ds[i].getCitta().trim().length() == 0 || ds[i].getProprietario().trim().length() == 0 
						|| ds[i].getBenzina().trim().length() == 0 || ds[i].getCapacita() == 0) 
								throw new NullPointerException();
			}
		}catch(NullPointerException e) {
			System.out.println("Errore: uno o pi� parametri nulli");
			System.exit(1);
		}
		//Stampo il confronto della capacit� dei due distributori
		System.out.println("Risultato confronto d1-d2: " + d1.compareTo(d2));
		//Simulo l'erogazione di 10 litri di benzina
		d1.eroga(10);
	}

}
