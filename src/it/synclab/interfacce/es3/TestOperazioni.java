/*Definire una interfaccia &#39;operazione&#39; e le sue classi derivate( sub. div, mul) in modo che da ciascuna sia
possibile eseguire la corrispondente operazione aritmetica (di addizione, divisione, moltiplicazione)
istanziando un oggetto di classe &#39;operazione&#39; e due operandi. Realizzare anche, una classe astratta che
implementi le funzionalitą comuni come la stampa a video del risultato.*/

package it.synclab.interfacce.es3;
import java.util.Scanner;

public class TestOperazioni {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Operazioni op = new Operazioni();
		double x = 0, y = 0;
		//Input check
		try {
			System.out.println("Inserire operando 1");
			x = Double.valueOf(scanner.nextLine());
			System.out.println("Inserire operando 2");
			y = Double.valueOf(scanner.nextLine());
			if(y == 0) throw new ArithmeticException();
		}catch(NumberFormatException e) {
			System.out.println("Errore: operando non valido");
			System.exit(1);
		}catch(ArithmeticException e) {
			System.out.println("Errore: divisione per 0");
			System.exit(1);	
		}
		
		//Stampo la somma, divisione, moltiplicazione dei due operandi
		op.print("Somma: " + op.somma(x, y));
		op.print("Divisione: " + op.divisione(x, y));
		op.print("Moltiplicazione: " + op.moltiplicazione(x, y));
	}

}
