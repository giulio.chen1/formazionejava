package it.synclab.interfacce.es3;

public class Operazioni extends Funzione implements Operazione{
	
	public Operazioni() {}
	
	@Override
	public double somma(double x, double y) {
		return x + y;
		
	}

	@Override
	public double divisione(double x, double y) {
		return x / y;
		
	}

	@Override
	public double moltiplicazione(double x, double y) {
		return x * y;
		
	}

	@Override
	public void print(String str) {
		System.out.println(str);
		
	}
	
}
