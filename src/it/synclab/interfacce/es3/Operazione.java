package it.synclab.interfacce.es3;

public interface Operazione {
	
	public double somma(double x, double y);
	
	public double divisione(double x, double y);
	
	public double moltiplicazione(double x, double y);
}
