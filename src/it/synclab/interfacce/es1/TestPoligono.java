//Scrivere un programma che dato un numero intero in ingresso vengano restituiti un oggetto
//rappresentativo del quadrato del numero dato e un oggetto rappresentativo del cubo del numero assegnato.
//Implementa l&#39;algoritmo attraverso l&#39;uso di una interfaccia comune.

package it.synclab.interfacce.es1;
import java.util.Scanner;

public class TestPoligono {

	public static void main(String[] args) {
		Poligono[] poligoni = new Poligono[20];  //Creo un array di 20 poligoni
		Scanner scanner = new Scanner(System.in);
		int countQ = 0, countC = 0;		//Contatori di quadrati e cubi
		
		//Carico quadrati nella prima met� dell'array, cubi nella seconda 
		for(int i=0;i<poligoni.length;i++) {
			if(i<(poligoni.length/2)) {
				poligoni[i] = new Quadrato(countQ);
				countQ++;
			}else {
				poligoni[i] = new Cubo(countC);
				countC++;
			}
		}
		
		//Chiedo all'utente di inserire l'ID
		System.out.println("Inserire un numero da 0 a 9");
		String in = scanner.nextLine();
		//Check dell'input
		try{
			int input = Integer.valueOf(in);
			if(input < 0 || input > 9) throw new NumberFormatException();
		}catch(NumberFormatException e) {
			System.out.println("Errore: input errato");
			System.exit(1);
		}
		int input = Integer.valueOf(in);
		
		//Stampo il nome del quadrato e del cubo i cui ID corrispondono all'input
		for(int i=0;i<poligoni.length;i++) {
			try {
				Quadrato q = (Quadrato) poligoni[i];
				if(q.checkID(input)) System.out.println(q.getNome());
			}catch(ClassCastException e) {
				Cubo c = (Cubo) poligoni[i];
				if(c.checkID(input)) System.out.println(c.getNome());
			}
		}
	}

}
