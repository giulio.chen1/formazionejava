package it.synclab.interfacce.es1;

public class Cubo implements Poligono{
	private int id;
	private String nome;
	
	public Cubo(int id) {
		this.id = id;
		this.nome = "Cubo" + id;
	}

	@Override
	public boolean checkID(int i) {
		if(this.getId() == i) {
			return true;
		}
		return false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
