package it.synclab.interfacce.es1;

public class Quadrato implements Poligono {
	private int id;
	private String nome;
	
	public Quadrato(int id) {
		this.id = id;
		this.nome = "Quadrato" + id;
	}
	
	@Override
	public boolean checkID(int i) {
		if(this.getId() == i) {
			return true;
		}
		return false;
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
