package it.synclab.ereditarieta.es3;

public class TestVehicle {
	public static void main(String[] args) {
		Vehicle[] arr = new Vehicle[5];
		//Creo un array di veicoli
		arr[0] = new Car("DG456ED", "Fiat", "Bravo", false, "SUV");
		arr[1] = new Car("CS275HJ", "Ford", "Edge", true, "SUV");
		arr[2] = new Car("LH964DY", "Honda", "Civic", true, "SUV");
		arr[3] = new Motorcycle("OV356QW", "BMW", "Concept", false, 100);
		arr[4] = new Motorcycle("PL098MK", "KTM", "1290", true, 125);
		
		//Stampo la targa dei veicoli guasti
		System.out.println("Veicoli guasti:");
		for(int i=0; i<arr.length;i++) {
			if(arr[i].isGuasto()) {
				System.out.println("Targa: " + arr[i].getTarga());
			}
		}
	}
}
