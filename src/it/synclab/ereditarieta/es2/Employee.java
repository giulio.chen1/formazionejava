package it.synclab.ereditarieta.es2;

import it.synclab.ereditarieta.es1.Person;

public class Employee extends Person {
	private int annoAss; //Anno di assunzione
	private double stipendio;
	
	public Employee(String sur, String name, String taxCode, String city, int annoAss, double stipendio) {
		super(sur, name, taxCode, city);
		this.annoAss = annoAss;
		this.stipendio = stipendio;
	}

	public int getAnnoAss() {
		return annoAss;
	}

	public void setAnnoAss(int annoAss) {
		this.annoAss = annoAss;
	}

	public double getStipendio() {
		return stipendio;
	}

	public void setStipendio(double stipendio) {
		this.stipendio = stipendio;
	}
	
	public boolean gainsMore(Employee e) {
		if(this.getStipendio() > e.getStipendio()) {
			return true;
		}
		return false;
	}
	
	public String visualize() {
		String info = new String();
		info = "Surname: " + this.getSurname()+ "\nName: " + this.getName()+ "\nTaxCode: "  + this.getTaxCode()
			+ "\nCity: "  + this.getCity()+ "\nStipendio: "  + String.valueOf(this.getStipendio());
		return info;
	}
	
}
