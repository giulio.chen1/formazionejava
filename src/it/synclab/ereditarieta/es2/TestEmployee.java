package it.synclab.ereditarieta.es2;

public class TestEmployee {

	public static void main(String[] args) {
		Employee e1 = new Employee("Cavalli", "Giacomo", "CVLGCM98", "Vicenza", 3, 7000);
		Employee e2 = new Employee("Rossi", "Maria", "RSSMRI93", "Milano", 4, 6000);
		Employee e3 = new Employee("Verdi", "Luigi", "VRDLGI95", "Roma", 2, 3000);
		Employee[] emps = new Employee[] {e1, e2, e3};
		
		//Input check
		try {
			for(int i=0;i<emps.length;i++) {
				if(emps[i].getName().trim().length() == 0 || emps[i].getSurname().trim().length() == 0 
						|| emps[i].getTaxCode().trim().length() == 0 || emps[i].getCity().trim().length() == 0) 
								throw new NullPointerException();
			}
		}catch(NullPointerException e) {
			System.out.println("Errore: stringa vuota");
			System.exit(1);
		}
		
		//Gestisco il caso del CF non valido
		try {
			for(int i=0;i<emps.length;i++) {
				emps[i].bornYear();
			}
		}catch(Exception e) {
			System.out.println("Errore: inserire CF valido");
			System.exit(1);
		}
		
		//Ordine crescente in base allo stipendio
		for(int i=0;i<emps.length-1;i++) {
			if(emps[i].gainsMore(emps[i+1])) {
				Employee a = emps[i+1];
				emps[i+1] = emps[i];
				emps[i] = a;
				for(int j=i;j>0;j--) {
					if(emps[j-1].gainsMore(emps[j])) {
						Employee b = emps[j];
						emps[j] = emps[j-1];
						emps[j-1] = b;
					}
				}
			}
		}
		//Stampo gli impiegati
		for(int i=0;i<emps.length;i++) {
			System.out.println("Nome: " + emps[i].getName() + ", stipendio: " + emps[i].getStipendio());
		}
	}

}
