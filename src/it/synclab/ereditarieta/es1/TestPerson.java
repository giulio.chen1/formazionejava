package it.synclab.ereditarieta.es1;

public class TestPerson {
	public static void main(String[] args) {
		Person dude = new Person("Rossi", "Maria", "RSSMRI98", "Milan");
		//Input check
		try {
			if(dude.getName().trim().length() == 0 || dude.getSurname().trim().length() == 0 
					|| dude.getTaxCode().trim().length() == 0 || dude.getCity().trim().length() == 0) 
							throw new NullPointerException();
		}catch(NullPointerException e) {
			System.out.println("Errore: stringa vuota");
			System.exit(1);
		}
		//Stampa il nome, cognome e anno di nascita
		int annoNascita = 0;
		try {
			annoNascita = dude.bornYear();
		}catch(Exception e) {
			System.out.println("Errore: inserire un codice fiscale valido");
			System.exit(1);
		}
		System.out.println("Name: "+dude.getName());
		System.out.println("Surname: "+dude.getSurname());
		System.out.println("Born year: " + annoNascita);
	} 
}
