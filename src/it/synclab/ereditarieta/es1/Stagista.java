package it.synclab.ereditarieta.es1;

public class Stagista extends Person {
	private int numberOfPresence, idNumber;
	
	public Stagista(String surname, String name, String taxCode, String city, int numPresence, int idNum) {
		super(surname, name, taxCode, city);
		this.numberOfPresence = numPresence;
		this.idNumber = idNum;
	}

	public int getNumberOfPresence() {
		return numberOfPresence;
	}

	public void setNumberOfPresence(int numberOfPresence) {
		this.numberOfPresence = numberOfPresence;
	}

	public int getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(int idNumber) {
		this.idNumber = idNumber;
	}
}
