package it.synclab.ereditarieta.es1;
import java.util.Date;

public class TestStagista {
	public static void main(String[] args) {
		Date today = new Date();
		Stagista s1 = new Stagista("Cavalli", "Cavalli", "CVLGCM01", "Vicenza", 10, 1);
		Stagista s2 = new Stagista("Rossi", "Maria", "RSSMRI", "Milano", 23, 2);
		Stagista s3 = new Stagista("Verdi", "Luigi", "VRDLGI", "Roma", 14, 3);
		Stagista[] stagisti = new Stagista[] {s1, s2, s3};
		//Input check
		try {
			for(int i=0;i<stagisti.length;i++) {
				if(stagisti[i].getName().trim().length() == 0 || stagisti[i].getSurname().trim().length() == 0 
						|| stagisti[i].getTaxCode().trim().length() == 0 || stagisti[i].getCity().trim().length() == 0) 
								throw new NullPointerException();
			}
		}catch(NullPointerException e) {
			System.out.println("Errore: stringa vuota");
			System.exit(1);
		}
		
		
		Stagista youngest = stagisti[0] ; //Lo stagista pi� giovane
		
		//Gestisco il caso del CF non valido
		try {
			for(int i=0;i<stagisti.length;i++) {
				stagisti[i].bornYear();
			}
		}catch(Exception e) {
			System.out.println("Errore: inserire CF valido");
			System.exit(1);
		}
		
		for(int i=0;i<stagisti.length;i++) {
			int annoNascitaS = stagisti[i].bornYear(); //Anno di nascita dell'i-esimo stagista
			int annoNascitaY = youngest.bornYear(); //Anno di nascita del pi� giovane
			//Gestisco il caso in cui uno stagista sia nato dopo il 2000
			if(stagisti[i].bornYear() < (today.getYear()-100)) {
				annoNascitaS += 100;
			}else if(youngest.bornYear() < (today.getYear()-100)) {	//getYear() mi restituisce l'anno corrente - 1900
				annoNascitaY += 100;
			}
			//Confronto e mi salvo lo stagista pi� giovane
			if(annoNascitaY < annoNascitaS) {
				youngest = stagisti[i];
			}
		}
		//Stampo il nome dello stagista pi� giovane
		System.out.println("Lo stagista pi� giovane � " + youngest.getName());
	}
}
