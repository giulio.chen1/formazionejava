package it.synclab.ereditarieta.es1;

public class Person {
	private String surname, name, taxCode, city;
	
	public Person() {}
	
	public Person(String surname, String name, String taxCode, String city) {
		this.surname = surname;
		this.name = name;
		this.taxCode = taxCode;
		this.city = city;
	}
	
	//Prende l'anno di nascita dal CF
	public int bornYear(){
		int anno = Integer.valueOf(this.getTaxCode().substring(6, 8));
		return anno;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	
}
