package it.synclab.ereditarieta.es4;

import it.synclab.ereditarieta.es3.*;

public class Garage {
	public Garage() {}
	
	public int repair(Vehicle v) {
		int prezzo = 0;
		String tipo = String.valueOf(v.getClass()).substring(String.valueOf(v.getClass()).length()-3); //Tipologia veicolo
		//Se il veicolo � un'auto
		if(tipo.equals("Car")) {
			prezzo += 200;
		}else { //Se il veicolo � una moto
			prezzo += 100;
		}
		
		if(v.isGuasto()) {	//Veicolo guasto
			prezzo += 100;
			v.setGuasto(false);
		}else {				//Veicolo non guasto
			prezzo += 50;
		}
		return prezzo;
	}
}
