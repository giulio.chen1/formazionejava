package it.synclab.ereditarieta.es4;

import it.synclab.ereditarieta.es3.*;

public class TestGarage {
	public static void main(String[] args) {
		Garage garage = new Garage();
		Vehicle[] veicoli = new Vehicle[5];
		veicoli[0] = new Car("DG456ED", "Fiat", "Bravo", false, "SUV");
		veicoli[1] = new Car("CS275HJ", "Ford", "Edge", true, "SUV");
		veicoli[2] = new Car("LH964DY", "Honda", "Civic", true, "SUV");
		veicoli[3] = new Motorcycle("OV356QW", "BMW", "Concept", false, 100);
		veicoli[4] = new Motorcycle("PL098MK", "KTM", "1290", true, 125);
		
		//Stampo la targa dei veicoli e il rispettivo prezzo del controllo
		for(int i=0;i<veicoli.length;i++) {
			System.out.println("Targa: " + veicoli[i].getTarga() + ", prezzo: " + garage.repair(veicoli[i]));
		}
	}
}
