package it.synclab.ereditarieta.es6;

public class Prodotto {
	private int id;
	private String descrizione;
	private double prezzo;
	
	public Prodotto(int id, String desc, double prezzo) {
		this.id = id;
		this.descrizione = desc;
		this.prezzo = prezzo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}
	
	public void applicaSconto(double prezzo) {
		double scontato = prezzo;
		scontato -= scontato * 0.05;
		prezzo = scontato;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public boolean equals(Prodotto p) {
		return super.equals(p);
	}

	@Override
	public String toString() {
		return super.toString();
	}
	
}
