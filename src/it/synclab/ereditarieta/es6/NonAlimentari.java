package it.synclab.ereditarieta.es6;

public class NonAlimentari extends Prodotto {
	private String materiale;
	
	public NonAlimentari(int id, String desc, double prezzo, String materiale) {
		super(id, desc, prezzo);
		this.materiale = materiale;
	}

	public String getMateriale() {
		return materiale;
	}

	public void setMateriale(String materiale) {
		this.materiale = materiale;
	}
	
	public void applicaSconto(double prezzo) {
		double scontato = prezzo;
		//Se il materiale � uno tra carta, vetro e plastica, applico lo sconto
		if((this.getMateriale().equalsIgnoreCase("carta"))||(this.getMateriale().equalsIgnoreCase("vetro")||(this.getMateriale().equalsIgnoreCase("plastica")))) {
			scontato -= scontato * 0.1;
			this.setPrezzo(scontato);
		}
		
	}
	
}
