package it.synclab.ereditarieta.es6;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Alimentari extends Prodotto{
	private LocalDate scadenza;
	public Alimentari(int id, String desc, double prezzo, LocalDate scadenza) {
		super(id, desc, prezzo);
		this.scadenza = scadenza;
	}
	
	public LocalDate getScadenza() {
		return scadenza;
	}
	
	public void setScadenza(LocalDate scadenza) {
		this.scadenza = scadenza;
	}
	
	public void applicaSconto(double prezzo) {
		LocalDate today = LocalDate.now(); //Data di oggi
		double scontato = prezzo;
		//Ottengo la differenza dei giorni tra oggi e la scadenza del prodotto
		long giorni = ChronoUnit.DAYS.between(today, this.getScadenza());
		
		//Se mancano meno di 10 giorni alla scadenza, applico lo sconto
		if(giorni <= 10 && giorni > 0) {
			scontato -= scontato * 0.2;
			this.setPrezzo(scontato);
		}
	}
	

}
