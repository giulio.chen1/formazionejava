package it.synclab.ereditarieta.es6;

import java.time.LocalDate;
import java.util.Scanner;
import java.time.DateTimeException;

public class ListaSpesa {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String input = new String();
		String prodotto = new String();
		String tipoProdotto = new String();
		int totale = 0;
		Prodotto[] prodotti = new Prodotto[6];
		
		try {
			//Creo una lista di prodotti acquistabili
			prodotti[0] = new NonAlimentari(1, "Bottiglia", 200.0, "plastica");
			prodotti[1] = new NonAlimentari(3, "Gomma", 100.0, "gomma");
			prodotti[2] = new NonAlimentari(7, "Libro", 280.0, "carta");
			prodotti[3] = new Alimentari(4, "Pane", 120.0, (LocalDate.of(2019, 12, 9))); 
			prodotti[4] = new Alimentari(9, "Vino", 160.0, (LocalDate.of(2019, 12, 2)));
			prodotti[5] = new Prodotto(6, "Boh", 150.0);
			
			//Memorizzo tutti gli id dei prodotti
			String idList = new String();
			for(int i=0;i<prodotti.length;i++) {
				idList += prodotti[i].getId() + ",";
			}
			String[] idArr = idList.split(",");
			
			//Controllo che l'id dei prodotti non sia ripetuto
			for(int i=0;i<prodotti.length;i++) {
				int idRipetuto = 0;
				for(int j=0;j<idArr.length;j++) {
					if(prodotti[i].getId() == Integer.valueOf(idArr[j])) {
						idRipetuto++;
						if(idRipetuto > 1) throw new StringIndexOutOfBoundsException();
					}
				}
			}
			//Input check
			for(int i=0;i<prodotti.length;i++) {
				tipoProdotto = String.valueOf(prodotti[i].getClass()).substring(34);
				if(prodotti[i].getId() == 0 || prodotti[i].getDescrizione().trim().length() == 0 
						|| prodotti[i].getPrezzo() == 0) 
							throw new NullPointerException();
				if(tipoProdotto.equals("NonAlimentari") && ((NonAlimentari)prodotti[i]).getMateriale().trim().length() == 0) {
					throw new NullPointerException();
				}else if(tipoProdotto.equals("Alimentari") && ((Alimentari)prodotti[i]).getScadenza().getYear() < LocalDate.now().getYear())
							throw new DateTimeException("");
			}
		}catch(NullPointerException e) {
			System.out.println("Errore: uno o pi� parametri nulli");
			System.exit(1);
		}catch(DateTimeException e) {
			System.out.println("Errore: data non valida");
			System.exit(1);
		}catch(StringIndexOutOfBoundsException e) {
			System.out.println("Errore: id ripetuto");
			System.exit(1);
		}
		//Stampo i prodotti
		System.out.println("Prodotti: ");
		for(int i=0;i<prodotti.length;i++) {
			System.out.println(prodotti[i].getId() + " - " + prodotti[i].getDescrizione() + " - �" + prodotti[i].getPrezzo());
		}
		//Chiedo se l'utente ha la tessera fedelt�
		System.out.println("Ha la tessera fedelt�? (si/no)");
		input = scanner.nextLine();
		
		//Controllo che l'input sia si/no
		if(input.equalsIgnoreCase("si")) { //Caso "si"
			//Chiedo all'utente l'ID del prodotto acquistato
			System.out.println("Inserire l'ID dei prodotti acquistati");
			prodotto = scanner.nextLine();
			
			//Cicla finch� non viene inserita una stringa vuota
			while(!prodotto.equals("")){
				//Controllo dell'ID inserito
				try {
					int idProdotto = Integer.valueOf(prodotto.trim());
					//Controllo che l'id inserito sia di un prodotto della lista
					boolean idNonEsiste = true;
					for(int i=0;i<prodotti.length;i++) {
						if(prodotti[i].getId() == idProdotto) idNonEsiste = false;
					}
					if(idNonEsiste) throw new NumberFormatException();
				}catch(NumberFormatException e) {
					System.out.println("ID non valido");
					System.exit(1);
				}
				
				int i = Integer.valueOf(prodotto.trim()); //ID del prodotto
				for(int j=0;j<prodotti.length;j++) {
					if(prodotti[j].getId() == i) {
						tipoProdotto = String.valueOf(prodotti[j].getClass()).substring(34); //Tipo del prodotto
						if(tipoProdotto.equals("Alimentari")) { //Caso prodotto alimentare
							Alimentari al = (Alimentari)prodotti[j];
							al.applicaSconto(al.getPrezzo());	//Applico lo sconto
							totale += al.getPrezzo();	//Aggiungo il prezzo nel totale
							prodotto = scanner.nextLine();
						}else if(tipoProdotto.equals("NonAlimentari")){ //Caso prodotto non alimentare
							NonAlimentari nal = (NonAlimentari)prodotti[j];
							nal.applicaSconto(nal.getPrezzo());	//Applico lo sconto
							totale += nal.getPrezzo();	//Aggiungo il prezzo nel totale
							prodotto = scanner.nextLine();
						}else {		//Caso prodotto generico
							totale += prodotti[j].getPrezzo(); //Aggiungo il prezzo nel totale
							prodotto = scanner.nextLine();
						}
					}
				}
				
			}
		}else if(input.equalsIgnoreCase("no")){		//Caso "no"
			//Chiedo all'utente l'ID del prodotto acquistato
			System.out.println("Inserire l'id dei prodotti acquistati");
			prodotto = scanner.nextLine();
			
			//Cicla finch� non viene inserita una stringa vuota
			while(!prodotto.equals("")){
				//Controllo dell'ID inserito
				try {
					int idProdotto = Integer.valueOf(prodotto.trim());
					//Controllo che l'id inserito sia di un prodotto della lista
					boolean idNonEsiste = true;
					for(int i=0;i<prodotti.length;i++) {
						if(prodotti[i].getId() == idProdotto) idNonEsiste = false;
					}
					if(idNonEsiste) throw new NumberFormatException();
				}catch(NumberFormatException e) {
					System.out.println("ID non valido");
					System.exit(1);
				}
				int i = Integer.valueOf(prodotto.trim());
				for(int j=0;j<prodotti.length;j++) {
					if(prodotti[j].getId() == i) {
						totale += prodotti[j].getPrezzo();	//Aggiungo il prezzo nel totale
					}
				}
				prodotto = scanner.nextLine();
			}
		}else { //Caso default, termino
			System.out.println("Rispondere si/no");
			System.exit(1);
		}
		//Stampo il prezzo totale
		System.out.println("Totale: �" + totale);
	}
	
}
