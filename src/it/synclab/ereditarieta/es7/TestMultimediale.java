package it.synclab.ereditarieta.es7;
import java.util.Scanner;

public class TestMultimediale {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int selez = 0;
		String input = new String();
		
		FileMultimediale[] files = new FileMultimediale[5];		//Creo un array di file multimediali
		String[] parametri = new String[4];		//Creo un array di parametri di un file
		
		System.out.println("- Inserire titolo, durata, volume per creare un audio");
		System.out.println("- Inserire titolo, luminosit�, durata, volume per creare un video");
		System.out.println("- Inserire titolo, luminosit� per creare un'immagine");
		System.out.println("NB: Separare i vari parametri con spazi");
		
		
		for(int j=0;j<files.length;j++) {
			input = scanner.nextLine() + ' '; //Parametri del file inseriti dall'utente
			
			//int numParametri = 0;	//Numero dei parametri
			/*for(int i=0;i<input.length();i++) {
				String substr = new String();
				while(input.charAt(i) != ' ' && numParametri < 4) {
					substr += input.charAt(i);
					i++;
				}
				if(substr.trim().length() != 0) {
					parametri[numParametri] = substr;
					numParametri++;
				}
			}*/
			
			//Memorizzo i parametri in un array
			parametri = input.trim().split("\\s+");
			
			//Check dei parametri inseriti
			try {
				//Se nei campi del volume, della luminosit�, della durata non ricevo un intero positivo, lancio un'eccezione e termino
				for(int i=1;i<parametri.length;i++) {
					if(Integer.valueOf(parametri[i]) < 1) throw new NumberFormatException();
				}
				//Controllo il numero dei parametri
				switch(parametri.length) {	
					//2: creo un'immagine con i parametri inseriti
					case 2: Immagine im = new Immagine(parametri[0], Integer.valueOf(parametri[1]));
							files[j] = im;
					break;
					//3: creo un audio con i parametri inseriti
					case 3: Audio aud = new Audio(parametri[0], Integer.valueOf(parametri[1]), Integer.valueOf(parametri[2]));
							files[j] = aud;
					break;
					//4: creo un video con i parametri inseriti
					case 4: Video vid = new Video(parametri[0], Integer.valueOf(parametri[1]), Integer.valueOf(parametri[2]), Integer.valueOf(parametri[3]));
							files[j] = vid;
					break;
					//Altrimenti: stampo un errore e termino
					default: System.out.println("Errore, seguire le istruzioni indicate sopra");
							 System.exit(-1);
				}
			}catch(NumberFormatException e) {
				System.out.println("Errore: input errato");
				System.exit(-1);
			}
		}
		
		
		while (selez != -1) {
			//Selezione del file
			System.out.println("Quale vorresti eseguire? (1-5, 0 per terminare)");
			//Check dell'input
			String in = scanner.nextLine();
			try {
				selez = Integer.valueOf(in)-1;
				if(selez < -1 || selez > 4) {
					throw new NumberFormatException();
				}
			}catch(NumberFormatException e) {
				System.out.println("Errore: input errato");
				System.exit(1);
			}
			
			selez = Integer.valueOf(in)-1;	//Numero del file selezionato
			String tipoFile = String.valueOf(files[selez].getClass()).substring(34); //Tipo di file
			
			//Controllo del tipo di file
			if(tipoFile.equals("Immagine")) {   //Caso immagine
				Immagine im = (Immagine) files[selez];
				im.show();
				System.out.println("Aumentare/abbassare la luminosita? (+/-valore, altrimenti inserire 0)");
				String val = scanner.nextLine();
				//Check dell'input
				try {
					int valore = Integer.valueOf(val);
				}catch(NumberFormatException e) {
					System.out.println("Errore, inserire un intero");
					System.exit(1);
				}
				//Se l'input � positivo/negativo, aumento/abbasso la luminosit�
				if(!val.equals("0")) {
					if(val.charAt(0) == '-') {
						im.darker(Integer.valueOf(val) * -1);
					}else {
						im.brighter(Integer.valueOf(val));
					}
				}
			}else if(tipoFile.equals("Audio")) {	//Caso audio
				Audio aud = (Audio) files[selez];
				aud.play();
				System.out.println("Aumentare/abbassare il volume? (+/-valore, altrimenti inserire 0)");
				String val = scanner.nextLine();
				//Check dell'input
				try {
					int valore = Integer.valueOf(val);
				}catch(NumberFormatException e) {
					System.out.println("Errore, inserire un intero");
					System.exit(1);
				}
				//Se l'input � positivo/negativo, aumento/abbasso il volume
				if(!val.equals("0")) {
					if(val.charAt(0) == '-') {
						aud.weaker(Integer.valueOf(val) * -1);
					}else {
						aud.louder(Integer.valueOf(val));
					}
				}
			}else {		//Caso video
				Video vid = (Video) files[selez];
				vid.play();
				System.out.println("Aumentare/abbassare la luminosita? (+/-valore, altrimenti inserire 0)");
				String val = scanner.nextLine();
				//Check dell'input
				try {
					int valore = Integer.valueOf(val);
				}catch(NumberFormatException e) {
					System.out.println("Errore, inserire un intero");
					System.exit(1);
				}
				//Se l'input � positivo/negativo, aumento/abbasso la luminosit�
				if(!val.equals("0")) {
					if(val.charAt(0) == '-') {
						vid.darker(Integer.valueOf(val) * -1);
					}else {
						vid.brighter(Integer.valueOf(val));
					}
				}
				System.out.println("Aumentare/abbassare il volume? (+/-valore, altrimenti inserire 0)");
				val = scanner.nextLine();
				//Check dell'input
				try {
					int valore = Integer.valueOf(val);
				}catch(NumberFormatException e) {
					System.out.println("Errore, inserire un intero");
					System.exit(1);
				}
				//Se l'input � positivo/negativo, aumento/abbasso il volume
				if(!val.equals("0")) {
					if(val.charAt(0) == '-') {
						vid.weaker(Integer.valueOf(val) * -1);
					}else {
						vid.louder(Integer.valueOf(val));
					}
				}
			}
		}
		
	}

}
