package it.synclab.ereditarieta.es7;

//Interfaccia che simula un media player
public interface MediaPlayer {
	
	public void play();
	
	public void weaker(int vol);
	
	public void louder(int vol);
}
