package it.synclab.ereditarieta.es7;

public class Audio extends FileMultimediale implements MediaPlayer {
	private int volume, durata;
	
	public Audio(String titolo, int durata, int volume) {
		super(titolo);
		this.volume = volume;
		this.durata = durata;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	public int getDurata() {
		return durata;
	}

	public void setDurata(int durata) {
		this.durata = durata;
	}
	
	//Abbasso il volume
	@Override
	public void weaker(int vol) {
		this.volume = this.getVolume() - vol;
	}
	
	//Aumento il volume
	@Override
	public void louder(int vol) {
		this.volume = this.getVolume() + vol;
	}
	
	//Stampo (titolo + (! per volume-volte)) per durata-volte
	@Override
	public void play() {
		int durata = this.getDurata();
		for(int i=0;i<durata;i++) {
			String titolo = this.getTitolo();
			for(int j=0;j<this.getVolume();j++) {
				titolo += "!";
			}
			System.out.println(titolo);
		}
		
	}
}
