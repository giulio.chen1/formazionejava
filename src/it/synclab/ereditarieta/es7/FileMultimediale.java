package it.synclab.ereditarieta.es7;

public abstract class FileMultimediale {
	private String titolo;
	
	public FileMultimediale(String titolo) {
		this.titolo = titolo;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
}
