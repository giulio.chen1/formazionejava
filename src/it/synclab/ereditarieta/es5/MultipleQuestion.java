package it.synclab.ereditarieta.es5;

import java.util.Scanner;
import java.util.Random;

public class MultipleQuestion extends NumericQuestion{
	private String[] opzioni;
	
	public MultipleQuestion(String domanda, String risCorretta, int punteggio, String[] opzioni) {
		super(domanda, risCorretta, punteggio);
		this.opzioni = opzioni;
		
		//Controllo che le opzioni soddisfino la richiesta
		if(!isOptionsCorrect(opzioni)) {
			System.out.println("Errore, inserire solo con numeri interi positivi minori o uguali a " + opzioni.length);
			System.exit(1);
		}
		
		//Controllo che la risposta corretta soddisfi la richiesta
		if(!isCorrect(risCorretta)) {
			System.out.println("Errore, inserire solo con numeri interi positivi minori o uguali a " + opzioni.length);
			System.exit(1);
		}
	}
	
	public String[] getOpzioni() {
		return opzioni;
	}

	public void setOpzioni(String[] opzioni) {
		this.opzioni = opzioni;
	}
	
	//Metodo analogo a isValid, � stato cambiato il nome per non richiamare quello della superclasse
	public boolean isCorrect(String risposta) {
		if((risposta.matches("[0-9]+")) && (Integer.valueOf(risposta) <= this.getOpzioni().length)) {
			return true;
		}
		return false;
	}
	
	//Stampo la domanda e controllo la risposta
	public int ask() {
		System.out.println("Domanda: " + this.getDomanda());
		String[] risposte = new String[opzioni.length+1];
		for(int i=0;i<opzioni.length;i++) {
			risposte[i] = opzioni[i];
		}
		risposte[risposte.length-1] = this.getRisCorretta(); //Aggiungo la risposta giusta nelle opzioni
		risposte = shuffle(risposte);	//Mischio le opzioni
		showOptions(risposte);	//Stampo le opzioni
		//Controllo la risposta
		String risposta = super.answer();
		if(isCorrect(risposta)) {
			if(super.checkAnswer(risposta)) {
				return this.getPunteggio();
			}
		}else System.out.println("Rispondere solo con numeri interi positivi minori o uguali a " + opzioni.length);
		return 0;
	}
	
	//Metodo che mischia gli elementi di un array
	public String[] shuffle(String[] risposte) {
		String[] array = risposte;
		Random rand = new Random();
		
		for (int i = 0; i < array.length; i++) {
			int randomIndexToSwap = rand.nextInt(array.length);
			String temp = array[randomIndexToSwap];
			array[randomIndexToSwap] = array[i];
			array[i] = temp;
		}
		return array;
	}
	
	//Metodo che stampa gli elementi di un array
	public void showOptions(String[] array) {
		System.out.println("Risposte: ");
		for(int i=0;i<array.length;i++) {
			System.out.println("- " + array[i]);
		}
	}
	
	//Metodo che, dato l'array delle opzioni, controlla se rispettano la richiesta
	public boolean isOptionsCorrect(String[] opzioni) {
		for(int i=0;i<opzioni.length;i++) {
			if(!isCorrect(opzioni[i])) {
				return false;
			}
		}
		return true;
		
	}
}
