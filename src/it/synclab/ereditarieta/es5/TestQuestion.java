package it.synclab.ereditarieta.es5;
import java.util.Scanner;

public class TestQuestion {

	public static void main(String[] args) {
		String[] opzioni = new String[4];
		Question[] questions = new Question[4];
		Scanner scanner = new Scanner(System.in);
		int punteggio = 0;
		
		//Chiedo le opzioni per il MultipleQuestion
		System.out.println("Inserire " + opzioni.length + " opzioni");
		//Il controllo avviene dopo l'inserimento delle opzioni
		for(int i=0;i<opzioni.length;i++) {
			String input = scanner.nextLine();
			opzioni[i] = input;
		}
		
		//Input check
		try {
			questions[0] = new QuestionYesNo("Il gatto e' un felino?", "si", 10);
			questions[1] = new Question("The limone o pesca?", "Pesca", 1000);
			questions[2] = new NumericQuestion("3 * 4?", "12", 10);
			questions[3] = new MultipleQuestion("2 + 2?", "4", 10, opzioni);
			for(int i=0;i<questions.length;i++) {
				System.out.println(i);
				if(questions[i].getDomanda().length() == 0 || questions[i].getRisCorretta().length() == 0 
						|| questions[i].getPunteggio() == 0)
								throw new NullPointerException();
			}
		}catch(NullPointerException e) {
			System.out.println("Errore: null inserito");
			System.exit(1);
		}

		//Interrogo e per ogni risposta data stampo il punteggio
		for(int i=0;i<questions.length;i++) {
			punteggio += questions[i].ask();
			System.out.println("Il tuo punteggio: " + punteggio);
		}
		
	}

}
