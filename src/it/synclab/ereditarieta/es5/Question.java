package it.synclab.ereditarieta.es5;
import java.util.Scanner;

public class Question {
	private String domanda, risCorretta;
	int punteggio;
	
	public Question(String domanda, String risCorretta, int punteggio) {
		this.domanda = domanda;
		this.risCorretta = risCorretta;
		this.punteggio = punteggio;
	}
	
	
	public String getDomanda() {
		return domanda;
	}


	public void setDomanda(String domanda) {
		this.domanda = domanda;
	}


	public String getRisCorretta() {
		return risCorretta;
	}


	public void setRisCorretta(String risCorretta) {
		this.risCorretta = risCorretta;
	}


	public int getPunteggio() {
		return punteggio;
	}


	public void setPunteggio(int punteggio) {
		this.punteggio = punteggio;
	}
	
	//Stampo la domanda, restituisco il punteggio della domanda nel caso la risposta sia giusta, altrimenti 0
	public int ask() {
		System.out.println("Domanda: " + this.getDomanda());
		if(checkAnswer(answer())) {
			return this.getPunteggio();
		}
		return 0;
	}
	
	//Chiedo in input una risposta
	public String answer() {
		Scanner scanner = new Scanner(System.in);
		String risposta = scanner.nextLine();
		if(risposta.trim().length() == 0) {
			System.out.println("Errore, stringa vuota");
			System.exit(1);
		}
		return risposta;
	}
	
	//Controllo se � la risposta giusta
	public boolean checkAnswer(String risposta) {
		if(risposta.equalsIgnoreCase(this.getRisCorretta())) {
			return true;
		}
		return false;
	}
}
