package it.synclab.ereditarieta.es5;

import java.util.Scanner;

public class NumericQuestion extends Question {

	public NumericQuestion(String domanda, String risCorretta, int punteggio) {
		super(domanda, risCorretta, punteggio);
		//Controllo che sia un intero, altrimenti termino
		if(!isValid(risCorretta)) {
			System.out.println("Errore, inserire solo con numeri interi");
			System.exit(1);
		}
	}
	
	//Controllo che sia un intero
	public boolean isValid(String risposta) {
		if(risposta.matches("-?[0-9]+")) {
			return true;
		}
		return false;
	}
	
	//Stampo la domanda e controllo la risposta
	public int ask() {
		System.out.println("Domanda: " + this.getDomanda());
		String risposta = super.answer();
		if(isValid(risposta)) {
			if(super.checkAnswer(risposta)) {
				return this.getPunteggio();
			}
		}else System.out.println("Rispondere solo con numeri interi");
		return 0;
	}
}
