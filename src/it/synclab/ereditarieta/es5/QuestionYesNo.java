package it.synclab.ereditarieta.es5;

import java.util.Scanner;

public class QuestionYesNo extends Question{
	
	public QuestionYesNo(String domanda, String risCorretta, int punteggio) {
		super(domanda, risCorretta, punteggio);
		//Se la risposta corretta non � un si/no, termino
		if(!isValid(risCorretta)) {
			System.out.println("Errore, inserire solo si/no nella risposta corretta");
			System.exit(1);
		}
	}
	
	//Metodo che controlla, data una stringa, se � si/no
	public boolean isValid(String risposta) {
		if(risposta.equalsIgnoreCase("si") || risposta.equalsIgnoreCase("no")) {
			return true;
		}
		return false;
	}
	
	//Stampo la domanda e controllo la risposta
	public int ask() {
		System.out.println("Domanda: " + this.getDomanda());
		String risposta = super.answer();
		if(isValid(risposta)) {
			if(super.checkAnswer(risposta)) {
				return this.getPunteggio();
			}
		}else System.out.println("Rispondere solo con si/no");
		return 0;
	}
	
	
	
	
}
