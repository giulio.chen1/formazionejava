// Scrivere un programma / metodo che data una sequenza di interi stampi &quot;Tutti positivi e pari&quot; se i numeri
// inseriti sono tutti positivi e pari, altrimenti stampa &quot;NO&quot;. Risolvere questo esercizio senza usare array.

package it.synclab.cicli;
import java.util.Scanner;

public class Es1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String str = new String();
		String res = new String("Tutti positivi e pari");
		
		//Chiedo in input una sequenza di interi
		System.out.println("Inserire una sequenza di numeri separati dallo spazio");
		
		//Se la stringa non � di interi, termino
		try {
			str = scanner.nextLine();
			int input = Integer.valueOf(str.replaceAll("\\s+",""));
		}catch(NumberFormatException e) {
			System.out.println("Errore, stringa non valida");
			System.exit(1);
		}
		
		//Se la stringa � vuota, termino
		if(str.trim().length() == 0) {
			System.out.println("Errore, stringa vuota");
			System.exit(1);
		}
		
		loop: for(int i=0;i<str.length();i++) {
			String substr = new String();
			//Concatena in una stringa tutti i valori che incontra finch� non trova uno spazio
			if(str.charAt(i) != ' ') {
				substr += str.charAt(i);
				int j = i + 1;
				if(j < str.length()) {
					loop2: for(;j<str.length();j++) {
						if(str.charAt(j) != ' ' ) {
							substr += str.charAt(j);
						}else break loop2;
					}
				}
				
				//Se il valore � negativo oppure � dispari, rispondo con "NO" ed esco dal for
				int div = Integer.valueOf(substr);
				if((substr.charAt(0) == '-') || (div % 2 != 0)) {
					res = "NO";
					break loop;
				}
				i = j;	//La prossima iterazione parte dall'ultimo spazio
				
			}
		}
		//Stampo la risposta
		System.out.println(res);
	}
}
