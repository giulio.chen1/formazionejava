// Scrivere un programma / metodo che data una sequenza di interi stampi la media di tutti i numeri inseriti
// che siano divisibili per tre. Per esempio, se si immettono i valori 5, 8, 9, 12, 7, 6 ,1 il risultato stampato
// dovr� essere 9. Risolvere questo esercizio senza usare array.

package it.synclab.cicli;

import java.util.Scanner;

public class Es2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String str = new String();
		
		//Chiedo in input una sequenza di interi
		System.out.println("Inserire una sequenza di numeri separati dallo spazio");
		
		//Se la stringa non � di interi, termino
		try {
			str = scanner.nextLine();
			int input = Integer.valueOf(str.replaceAll("\\s+",""));
		}catch(NumberFormatException e) {
			System.out.println("Errore, stringa non valida");
			System.exit(1);
		}
		
		//Se la stringa � vuota, termino
		if(str.trim().length() == 0) {
			System.out.println("Errore, stringa vuota");
			System.exit(1);
		}
		
		double somma = 0; //Somma dei numeri divisibili per 3
		double count = 0; //Contatore di numeri divisibili per 3
		
		for(int i=0;i<str.length();i++) {
			String substr = new String();
			//Concatena in una stringa tutti i valori che incontra finch� non trova uno spazio
			if(str.charAt(i) != ' ') {
				substr += str.charAt(i);
				int j = i + 1;
				if(j < str.length()) {
					loop: for(;j<str.length();j++) {
						if(str.charAt(j) != ' ' ) {
							substr += str.charAt(j);
						}else break loop;
					}
				}
				//Se � divisibile per 3, lo aggiungo a somma e aumento count
				int div = Integer.valueOf(substr);
				if(div % 3 == 0) {
					somma += div;
					count++;
				}
				i = j;	//La prossima iterazione parte dall'ultimo spazio
				
			}
		}
		//Stampo la media
		try {
			if(count == 0) throw new ArithmeticException();
			System.out.println("Media: " + (somma / count));
		}catch(ArithmeticException e) {
			System.out.println("Nessun valore divisibile per 3");
		}
	}
}
