// Scrivere un programma / metodo che chiede all�utente di inserire una sequenza di caratteri (chiedendo
// prima quanti caratteri voglia inserire) e li ristampa man mano che vengono inseriti. L�intero procedimento
// (chiedere quanti caratteri voglia inserire, leggere i caratteri e man mano stamparli) dovr� essere ripetuto 5
// volte. Risolvere questo esercizio senza usare array.

package it.synclab.cicli;

import java.util.Scanner;

public class Es3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numChar;
		for(int i=0;i<5;i++) {
			//Chiedo il numero di caratteri
			System.out.println("Quanti caratteri vorresti stampare?");
			try {
				String nChar = scanner.nextLine();	//Numero caratteri inserito dall'utente
				if(nChar.trim().charAt(0) == '-') {
					System.out.println("Errore: numero negativo");
					System.exit(1);
				}
				numChar = Integer.valueOf(nChar);
				if(numChar != 0) {
					System.out.println("Inserire " + numChar + " caratteri");
					for(int j=0;j<numChar;j++) {
						String str = scanner.nextLine();	//Carattere inserito dall'utente
						//Controllo, se l'input non � un solo carattere, termino
						if(str.trim().length() != 1) {
							System.out.println("Errore: carattere non valido");
							System.exit(1);
						//Se l'input non � una lettera, termino
						}else if(!str.matches("[a-zA-Z]")) {
							System.out.println("Errore: carattere non valido");
							System.exit(1);
						}
						//Stampo il carattere
						System.out.println(str.charAt(0));
					}
				}
			//Stampo l'errore se non � un numero oppure quando l'utente inserisce pi� numeri
			//Termino
			}catch(NumberFormatException e) {
				System.out.println("Errore, inserire un intero");
				System.exit(1);
			}catch(StringIndexOutOfBoundsException e) {
				System.out.println("Errore: spazi vuoti");
				System.exit(1);
			}
		}
	}
}
