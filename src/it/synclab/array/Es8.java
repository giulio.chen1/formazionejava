// Scrivere un programma / metodo che date due sequenze di stringhe, ciascuna di 5 elementi, stampi il
// messaggio &quot;OK&quot; se almeno una stringa della prima sequenza compare anche nella seconda, altrimenti sar�
// stampato il messaggio &quot;KO&quot;. Qualora vengano trovate due stringhe uguali i confronti tra le sequenze
// devono essere interrotti.

package it.synclab.array;
import java.util.Scanner;

public class Es8 {
	public static void main(String[] args) {
		String[] arr = new String[5], arr2 = new String[5];
		Scanner scanner = new Scanner(System.in);
		String res = new String("KO");
		
		//Chiedo in input due sequenze di stringhe
		//Controllo che la prima sequenza contenga stringhe valide
		System.out.println("Inserire una prima sequenza di " + arr.length + " stringhe");
		for(int i=0;i<arr.length;i++) {
			String str = scanner.nextLine();
			if(str.trim().length() > 0) {
				try {  
					int input = Integer.valueOf(str);
					System.out.println("Errore: stringa non valida");
					System.exit(1);
				}catch(NumberFormatException e) {
					arr[i] = str;
				}
			}else {
				System.out.println("Errore: stringa vuota");
				System.exit(1);
			}
		}
		
		//Controllo che la seconda sequenza contenga stringhe valide
		System.out.println("Inserire una seconda sequenza di " + arr2.length + " stringhe");
		for(int i=0;i<arr.length;i++) {
			String str = scanner.nextLine();
			if(str.trim().length() > 0) {
				try {  												
					int input = Integer.valueOf(str);
					System.out.println("Errore: stringa non valida");
					System.exit(1);
				}catch(NumberFormatException e) {
					arr2[i] = str;
				}
			}else {
				System.out.println("Errore: stringa vuota");
				System.exit(1);
			}
		}
		
		int k=0; //Numero occorrenze
		//Controllo le occorrenze delle due sequenze
		loop: for(int i=0;i<arr.length;i++) {
			for(int j=0;j<arr2.length;j++) {
				if(arr[i].equals(arr2[j])) {
					res = "OK";
					k++;
					if(k == 2) {	//Esco dal for se trovo 2 occorrenze
						break loop;
					}
				}
			}
		}
		//Stampo la risposta
		System.out.println(res);
	}
}
