// Scrivere un programma / metodo che preveda un array di 10 numeri interi contenente valori random e che
// stampi la dicitura &quot;Tre valori consecutivi uguali&quot; contiene tre valori uguali in tre posizioni
// consecutive,qualora la condizione non dovesse essere verificata dovr� stampare &quot;NO&quot;.

package it.synclab.array;

public class Es7 {
	public static void main(String[] args) {
		//Creo un array con 10 interi casuali
		int[] arr = new int[10];
		for(int i=0;i<arr.length;i++) {
			arr[i] = (int)(Math.random()*10);
			System.out.print(arr[i] + " ");
		}
		
		//Inizializzo il risultato a "NO"
		String res = new String("NO");
		
		//Controllo se ci sono 3 valori uguali consecutivi, eventualmente modifico il risultato
		for(int i=0;i<arr.length-2;i++) {
			if(arr[i] == arr[i+1]) {
				if(arr[i] == arr[i+2])
					res = "Tre valori consecutivi";
			}
		}
		
		//Stampo il risultato
		System.out.println();
		System.out.println(res);
	}
}
