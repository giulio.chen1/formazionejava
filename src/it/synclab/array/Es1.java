// Dato un array di interi, popolato casualmente, ordinarlo in ordine crescente e stampare a video il risultato di tale ordinamento.
package it.synclab.array;

public class Es1 {
	
	public static void main(String[] args) {
		
		//Creo un array di 10 interi random da 0 a 99
		int[] arr = new int[10];
		for (int i=0;i<arr.length; i++){
			int n = (int)(Math.random()*100);
			arr[i] = n;
			System.out.print(n + " ");
		}
		
		//Controllo che l'ordine sia crescente, altrimenti scambio i valori
		//For esterno: controllo e ordino da sinistra, for interno: controllo e ordino da destra
		for(int i=0;i<arr.length-1;i++) {
			if(arr[i] > arr[i+1]) {
				int a = arr[i+1];
				arr[i+1] = arr[i];
				arr[i] = a;
				for(int j=i;j>0;j--) {
					if(arr[j] < arr[j-1]) {
						int b = arr[j];
						arr[j] = arr[j-1];
						arr[j-1] = b;
					}
				}
			}
		}
		//Stampo l'array ordinato
		System.out.println();
		for(int i=0;i<arr.length;i++) {
			System.out.print(arr[i] + " ");
		}
		
		
	}
	
}
