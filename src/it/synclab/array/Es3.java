// Scrivere un metodo che mostri i primi 50 numeri della serie di fibonacci 
// (i primi due numeri di fibonacci sono 0 e 1 i successivi si calcolano come somma dei 2 precedenti).

package it.synclab.array;

public class Es3 {
	public static void main(String[] args){
		
		// Carico i 50 numeri in un array d'appoggio
	    long[] fibo50 = fibonacci50();
	    
	    // Stampo l'array
	    for(int i=0;i<fibo50.length;i++) {
	    	System.out.println(fibo50[i]);
	    }
	    
	}
	
	// Creo un array con i primi 50 numeri di fibonacci
	public static long[] fibonacci50() {
		long[] fibo = new long[50];
	    fibo[0] = 0;
	    fibo[1] = 1;
	    for(int i=0;i<fibo.length-2;i++) {
	    	fibo[i+2] = fibo[i] + fibo[i+1];
	    }
	    return fibo;
	}
} 
