package it.synclab.progetto.main;

import java.util.ArrayList;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import it.synclab.progetto.chiavi_comp.*;
import it.synclab.progetto.tabelle.*;

public class Main {
	public static void main( String[] args ) {


    	Configuration configuration = new Configuration().configure().addAnnotatedClass(Azienda.class);
    	configuration.configure().addAnnotatedClass(Utenza.class);
    	configuration.configure().addAnnotatedClass(Universita.class);
    	configuration.configure().addAnnotatedClass(Studente.class);
    	configuration.configure().addAnnotatedClass(Offerta.class);
    	configuration.configure().addAnnotatedClass(Evento.class);
    	configuration.configure().addAnnotatedClass(Stage.class);
    	configuration.configure().addAnnotatedClass(Dipendente.class);
    	ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
    	SessionFactory sf=configuration.buildSessionFactory(serviceRegistry);
    	Session session = sf.openSession();
    	session.beginTransaction();
    	
//    	ArrayList<Azienda> listAzienda = (ArrayList<Azienda>) session.createQuery("from Azienda order by partitaiva").list(); 
//    	ArrayList<Studente> listStudente = (ArrayList<Studente>) session.createQuery("from Studente order by nome").list();
//    	ArrayList<Utenza> listUtenza = (ArrayList<Utenza>) session.createQuery("from Utenza order by username").list(); 
//    	ArrayList<Offerta> listOfferta = (ArrayList<Offerta>) session.createQuery("from Offerta order by idofferta, idazienda").list(); 
//    	ArrayList<Dipendente> listDipendente = (ArrayList<Dipendente>) session.createQuery("from Dipendente order by cf").list(); 
//    	ArrayList<Universita> listUniversita = (ArrayList<Universita>) session.createQuery("from Universita order by iduniversita").list(); 
//    	ArrayList<Evento> listEvento = (ArrayList<Evento>) session.createQuery("from Evento order by idevento, iduniversita").list(); 
//    	ArrayList<Stage> listStage = (ArrayList<Stage>) session.createQuery("from Stage order by idstage").list(); 
    	
//    	Azienda azienda = (Azienda) session.get(Azienda.class, "10000000001");
//    	System.out.println(azienda);
    	
    	//Insert
//    	Azienda az = new Azienda("10000000009", "SyncLab");
//    	Utenza ut = new Utenza("userr", "passww", "azienda");
//    	az.setUtenzaAz(ut);
//    	session.save(az);

//    	Offerta of = new Offerta(new IdOfferta(8, az.getPartitaiva()), "Lavoro: test", "test", "descrizione");
//    	session.save(of);
    	
    	//Insert
//    	Universita uni = new Universita("12345678909", "Bobbo");
//    	Utenza ut = new Utenza("bobbo", "passww", "universita");
//    	uni.setUtenzaUni(ut);
//    	session.save(uni);
//    	Dipendente dip = new Dipendente("ACASO12345678901", "A", "Caso");
//    	dip.setUniDip(uni);
//    	session.save(dip);
    	
    	//Update 
//    	Azienda a = (Azienda)session.get(Azienda.class, "10000000000");
//    	a.setNome("Babbo");
    	
    	//Delete
//    	Utenza u = (Utenza)session.get(Utenza.class, "userr");
//    	session.delete(u);
    	
    	//Delete
//    	Universita uni = (Universita)session.get(Universita.class, "12345678909");
//    	session.delete(uni);
    	
    	//Insert Utenza + Azienda + Dipendente
    	Azienda azienda = new Azienda("10000000010", "Nome a caso");
    	azienda.setUtenzaAz(new Utenza("nomeacaso", "nomeacaso", "azienda"));
    	Dipendente dip = new Dipendente("SADASDASAD", "Nome", "Cognome");
    	dip.setAziendaDip(azienda);
    	azienda.getDipendentiAz().add(dip);
    	
//    	session.save(azienda);
    	
    	//Delete
//    	Azienda a = (Azienda)session.get(Azienda.class, "10000000010");
//    	session.delete(a);
    	
    	Universita uni = (Universita)session.get(Universita.class, "12345678902");
    	Studente stud = new Studente("nome", "cognome", "Matematica");
    	stud.setId(new IdStudente(30, "12345678909"));
    	stud.setUtenzaStud(new Utenza("cognome", "nome", "studente"));
    	stud.setUniStud(uni);
    	uni.getStudentiUni().add(stud);
    	
    	session.save(stud);
    	
    	session.getTransaction().commit();
    	
//    	mostraAziende(listAzienda);
//    	mostraUtenze(listUtenza);
//    	mostraOfferte(listOfferta);
//    	mostraDipendenti(listDipendente);
//    	mostraEventi(listEvento);
//    	mostraUniversita(listUniversita);
//    	mostraStudenti(listStudente);
//    	mostraStage(listStage);
    	
    }

	private static void mostraStage(ArrayList<Stage> listStage) {
		for(Stage s: listStage) {
    		System.out.println(s.toString());
			System.out.println();
    	}
	}

	private static void mostraStudenti(ArrayList<Studente> listStudente) {
		for(Studente s: listStudente) {
    		System.out.println(s.toString());
			System.out.println();
    	}
	}

	private static void mostraUniversita(ArrayList<Universita> listUniversita) {
		for(Universita s: listUniversita) {
    		System.out.println(s.toString());
    		System.out.println();
    	}
	}

	private static void mostraEventi(ArrayList<Evento> listEvento) {
		for(Evento s: listEvento) {
    		System.out.println(s.toString());
    		System.out.println();
    	}
	}

	private static void mostraDipendenti(ArrayList<Dipendente> listDipendente) {
		for(Dipendente s: listDipendente) {
    		System.out.println(s.toString());
    		System.out.println();
    	}
	}

	private static void mostraUtenze(ArrayList<Utenza> listUtenza) {
		for(Utenza s: listUtenza) {
    		System.out.println(s.toString());
    		System.out.println();
    	}
	}

	private static void mostraAziende(ArrayList<Azienda> listAzienda) {
		for(Azienda s: listAzienda) {
    		System.out.println(s.toString());
    		System.out.println();
    	}
	}

	private static void mostraOfferte(ArrayList<Offerta> listOfferta) {
		for(Offerta s: listOfferta) {
    		System.out.println(s.toString());
    		System.out.println();
    	}
	}
}
