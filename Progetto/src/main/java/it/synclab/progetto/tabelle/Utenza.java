 package it.synclab.progetto.tabelle;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
public class Utenza {
	@Id 
	private String username;
	private String password;
	private String tipo;
	@OneToOne(mappedBy="utenzaAz", cascade=CascadeType.ALL)
	private Azienda azUser;
	@OneToOne(mappedBy="utenzaUni", cascade=CascadeType.ALL)
	private Universita uniUser;
	@OneToOne(mappedBy="utenzaStud", cascade=CascadeType.ALL)
	private Studente studUser;
	
	public Utenza() {}
	
	public Utenza(String username, String password, String tipo) {
		super();
		this.username = username;
		this.password = password;
		this.tipo = tipo;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Azienda getAzUser() {
		return azUser;
	}

	public void setAzUser(Azienda azUser) {
		this.azUser = azUser;
	}

	public Universita getUniUser() {
		return uniUser;
	}

	public void setUniUser(Universita uniUser) {
		this.uniUser = uniUser;
	}
	
	public Studente getStudUser() {
		return studUser;
	}

	public void setStudUser(Studente studUser) {
		this.studUser = studUser;
	}

	@Override
	public String toString() {
		return "Utenza [username=" + username + ", password=" + password + ", tipo=" + tipo + "]";
	}
	
	
}
