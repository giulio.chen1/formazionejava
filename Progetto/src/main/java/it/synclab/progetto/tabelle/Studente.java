package it.synclab.progetto.tabelle;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import it.synclab.progetto.chiavi_comp.IdStudente;

@Entity
public class Studente {
	
	@EmbeddedId
	private IdStudente id;
	private String nome, cognome, facolta, cv;
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="IDUNIVERSITA")
	private Universita uniStud;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="USERNAME")
	private Utenza utenzaStud;
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="STUDENTE_EVENTO", joinColumns= {@JoinColumn(name="IDSTUDENTE", referencedColumnName="MATRICOLA"), 
			@JoinColumn(name="IDUNIVERSITA", referencedColumnName="IDUNIVERSITA")},
            	inverseJoinColumns= {@JoinColumn(name="IDEVENTO", referencedColumnName="IDEVENTO"), 
            		@JoinColumn(name="IDUNIEV", referencedColumnName="IDUNIVERSITA")})
	List<Evento> eventiStud = new LinkedList<Evento>();
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="STUDENTE_OFFERTA", inverseJoinColumns= {@JoinColumn(name="IDOFFERTA", referencedColumnName="IDOFFERTA"),
			@JoinColumn(name="IDAZIENDA", referencedColumnName="IDAZIENDA")},
            	joinColumns= {@JoinColumn(name="IDSTUDENTE", referencedColumnName="MATRICOLA"), 
            		@JoinColumn(name="IDUNIVERSITA", referencedColumnName="IDUNIVERSITA")})
	private List<Offerta> offerteStud = new LinkedList<Offerta>();
	
	public Studente() {}

	public Studente(String nome, String cognome, String facolta) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.facolta = facolta;
	}


	public IdStudente getId() {
		return id;
	}

	public void setId(IdStudente id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getFacolta() {
		return facolta;
	}

	public void setFacolta(String facolta) {
		this.facolta = facolta;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public Universita getUniStud() {
		return uniStud;
	}

	public void setUniStud(Universita uniStud) {
		this.uniStud = uniStud;
	}

	public Utenza getUtenzaStud() {
		return utenzaStud;
	}

	public void setUtenzaStud(Utenza utenzaStud) {
		this.utenzaStud = utenzaStud;
	}
	
	public List<Evento> getEventiStud() {
		return eventiStud;
	}

	public void setEventiStud(List<Evento> eventiStud) {
		this.eventiStud = eventiStud;
	}
	
	public List<Offerta> getOfferteStud() {
		return offerteStud;
	}

	public void setOfferteStud(List<Offerta> offerteStud) {
		this.offerteStud = offerteStud;
	}

	@Override
	public String toString() {
		return "Studente [id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", facolta=" + facolta + ", cv=" + cv
				+ ", utenzaStud=" + utenzaStud + ", eventiStud=" + eventiStud + "]";
	}
	
	
	
	
}
