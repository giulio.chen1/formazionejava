package it.synclab.progetto.tabelle;

import java.time.LocalDate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import it.synclab.progetto.chiavi_comp.IdEvento;

@Entity
public class Evento {
	@EmbeddedId
	private IdEvento id;;
	private String titolo;
	@Column(name="descrizione")
	private String desc;
	private String luogo;
	private Date data;
	private String settore;
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="eventiAz")
	private List<Azienda> aziendeEv = new LinkedList<Azienda>();
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="eventiStud")
	private List<Studente> studentiEv = new LinkedList<Studente>();
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="IDUNIVERSITA")
	private Universita uniEv;
	
	public Evento() {}

	public IdEvento getId() {
		return id;
	}

	public void setId(IdEvento id) {
		this.id = id;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getLuogo() {
		return luogo;
	}

	public void setLuogo(String luogo) {
		this.luogo = luogo;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getSettore() {
		return settore;
	}

	public void setSettore(String settore) {
		this.settore = settore;
	}

	public List<Azienda> getAziende() {
		return aziendeEv;
	}

	public void setAziende(List<Azienda> aziendeEv) {
		this.aziendeEv = aziendeEv;
	}
	
	public Universita getUniEv() {
		return uniEv;
	}

	public void setUniEv(Universita uniEv) {
		this.uniEv = uniEv;
	}
	
	public List<Studente> getStudentiEv() {
		return studentiEv;
	}

	public void setStudentiEv(List<Studente> studentiEv) {
		this.studentiEv = studentiEv;
	}

	@Override
	public String toString() {
		return "Evento [id=" + id.toString() + ", titolo=" + titolo + ", desc=" + desc + ", luogo=" + luogo + 
				", data=" + data.getDate() + "/" + (data.getMonth()+1) + "/" + (data.getYear()+1900)
				+ ", settore=" + settore + "]";
	}
	
}
