package it.synclab.progetto.tabelle;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.JoinColumnOrFormula;
import org.hibernate.annotations.JoinColumnsOrFormulas;
import org.hibernate.annotations.JoinFormula;

import it.synclab.progetto.chiavi_comp.IdOfferta;

@Entity
public class Offerta {
	@EmbeddedId
	private IdOfferta id;
	private String titolo, settore;
	@Column(name="descrizione")
	private String descr;
	private boolean valido;
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="idazienda", insertable=false, updatable=false)
	private Azienda aziendaOffer;
	@ManyToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinColumn(name="iduniversita", nullable=true)
	private Universita uniOffer;
	@OneToOne
//	@JoinColumnsOrFormulas(
//			value = {
//					@JoinColumnOrFormula(column = @JoinColumn(name="idstage", referencedColumnName="idstage")), 
//					@JoinColumnOrFormula(formula = @JoinFormula(value="idofferta", referencedColumnName="idofferta")), 
//					@JoinColumnOrFormula(formula = @JoinFormula(value="idazienda", referencedColumnName="idazienda"))
//					})
	private Stage stageOffer;
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="offerteStud")
	List<Studente> studentiOffer = new LinkedList<Studente>();
	
	public Offerta() {
		this.valido = false;
	}
	
	public Offerta(IdOfferta id, String titolo, String settore, String descr) {
		super();
		this.id = id;
		this.titolo = titolo;
		this.settore = settore;
		this.descr = descr;
		this.valido = false;
	}

	public IdOfferta getId() {
		return id;
	}

	public void setId(IdOfferta id) {
		this.id = id;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getSettore() {
		return settore;
	}

	public void setSettore(String settore) {
		this.settore = settore;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public boolean isValido() {
		return valido;
	}

	public void setValido(boolean valido) {
		this.valido = valido;
	}

	public Azienda getAziendaOffer() {
		return aziendaOffer;
	}

	public void setAziendaOffer(Azienda aziendaOffer) {
		this.aziendaOffer = aziendaOffer;
	}
	
	public Universita getUniOffer() {
		return uniOffer;
	}

	public void setUniOffer(Universita uniOffer) {
		this.uniOffer = uniOffer;
	}
	
	public Stage getStageOffer() {
		return stageOffer;
	}

	public void setStageOffer(Stage stageOffer) {
		this.stageOffer = stageOffer;
	}

	public List<Studente> getStudentiOffer() {
		return studentiOffer;
	}

	public void setStudentiOffer(List<Studente> studentiOffer) {
		this.studentiOffer = studentiOffer;
	}

	@Override
	public String toString() {
		return "Offerta [id=" + id + ", titolo=" + titolo + ", settore=" + settore + ", descr=" + descr + ", valido="
				+ valido + ", aziendaOffer=" + aziendaOffer.getNome() + ", uniOffer=" + uniOffer + ", stageOffer=" + stageOffer
				+ ", studentiOffer=" + studentiOffer + "]";
	}
	

	

	
	
	
	
}
