package it.synclab.progetto.tabelle;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import it.synclab.progetto.chiavi_comp.IdStage;

@Entity
public class Stage {
	@EmbeddedId
	private IdStage id;
	private int durata;
	private Date datainizio;
	private String tipo;
	@OneToOne(mappedBy="stageOffer", cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	private Offerta offertaStage;
	@ManyToMany(fetch=FetchType.LAZY, mappedBy="stageDip")
	private List<Dipendente> tutorStage = new LinkedList<Dipendente>();
	
	public IdStage getId() {
		return id;
	}

	public void setId(IdStage id) {
		this.id = id;
	}

	public int getDurata() {
		return durata;
	}

	public void setDurata(int durata) {
		this.durata = durata;
	}

	public Date getDatainizio() {
		return datainizio;
	}

	public void setDatainizio(Date datainizio) {
		this.datainizio = datainizio;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Offerta getOffertaStage() {
		return offertaStage;
	}

	public void setOffertaStage(Offerta offertaStage) {
		this.offertaStage = offertaStage;
	}

	public List<Dipendente> getTutorStage() {
		return tutorStage;
	}

	public void setTutorStage(List<Dipendente> tutorStage) {
		this.tutorStage = tutorStage;
	}

	@Override
	public String toString() {
		return "Stage [id=" + id + ", durata=" + durata + ", datainizio=" + datainizio + ", tipo=" + tipo
				+ ", offertaID=" + offertaStage.getId() + ", tutorStage=" + tutorStage + "]";
	}
	
	
}
