package it.synclab.progetto.tabelle;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Columns;


@Entity
public class Azienda {
	@Id 
	private String partitaiva;
	private String nome;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="USERNAME")
	private Utenza utenzaAz;
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="AZIENDA_EVENTO", joinColumns=@JoinColumn(name="IDAZIENDA", referencedColumnName="PARTITAIVA"),
            inverseJoinColumns= {@JoinColumn(name="IDEVENTO", referencedColumnName="IDEVENTO"), 
            		@JoinColumn(name="IDUNIVERSITA", referencedColumnName="IDUNIVERSITA")})
	List<Evento> eventiAz = new LinkedList<Evento>();
	@OneToMany(mappedBy="aziendaOffer", cascade=CascadeType.ALL)
	List<Offerta> offerteAz = new LinkedList<Offerta>();
	@OneToMany(mappedBy="aziendaDip", cascade=CascadeType.ALL)
	List<Dipendente> dipendentiAz = new LinkedList<Dipendente>();
	
	public Azienda() {}
	
	public Azienda(String partitaiva, String nome) {
		super();
		this.partitaiva = partitaiva;
		this.nome = nome;
	}

	public String getPartitaiva() {
		return partitaiva;
	}

	public void setPartitaiva(String partitaiva) {
		this.partitaiva = partitaiva;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Utenza getUtenzaAz() {
		return utenzaAz;
	}

	public void setUtenzaAz(Utenza utenzaAz) {
		this.utenzaAz = utenzaAz;
	}

	public List<Evento> getEventiAz() {
		return eventiAz;
	}

	public void setEventiAz(List<Evento> eventiAz) {
		this.eventiAz = eventiAz;
	}
	
	public List<Offerta> getOfferteAz() {
		return offerteAz;
	}

	public void setOfferteAz(List<Offerta> offerteAz) {
		this.offerteAz = offerteAz;
	}
	
	public List<Dipendente> getDipendentiAz() {
		return dipendentiAz;
	}

	public void setDipendentiAz(List<Dipendente> dipendentiAz) {
		this.dipendentiAz = dipendentiAz;
	}

	@Override
	public String toString() {
		return "Azienda [partitaiva=" + partitaiva + ", nome=" + nome + ", utenzaAz=" + utenzaAz + ", eventiAz="
				+ eventiAz + "]";
	}
	

	
	
	

	
	
	
}
