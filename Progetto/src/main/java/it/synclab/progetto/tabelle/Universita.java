package it.synclab.progetto.tabelle;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Universita {
	@Id
	private String iduniversita;
	private String nome;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="USERNAME")
	private Utenza utenzaUni;
	@OneToMany(mappedBy="uniEv", cascade=CascadeType.ALL)
	List<Evento> eventiUni = new LinkedList<Evento>();
	@OneToMany(mappedBy="uniStud", cascade=CascadeType.ALL)
	List<Studente> studentiUni = new LinkedList<Studente>();
	@OneToMany(mappedBy="uniOffer")
	List<Offerta> offerteUni = new LinkedList<Offerta>();
	@OneToMany(mappedBy="uniDip", cascade=CascadeType.ALL)
	List<Dipendente> docentiUni = new LinkedList<Dipendente>();
	
	public Universita() {}
	
	public Universita(String iduniversita, String nome) {
		super();
		this.iduniversita = iduniversita;
		this.nome = nome;
	}

	public String getIdUniv() {
		return iduniversita;
	}
	
	public void setIdUniv(String iduniversita) {
		this.iduniversita = iduniversita;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Utenza getUtenzaUni() {
		return utenzaUni;
	}
	
	public void setUtenzaUni(Utenza utenzaUni) {
		this.utenzaUni = utenzaUni;
	}

	public List<Evento> getEventiUni() {
		return eventiUni;
	}

	public void setEventiUni(List<Evento> eventiUni) {
		this.eventiUni = eventiUni;
	}

	public List<Studente> getStudentiUni() {
		return studentiUni;
	}

	public void setStudentiUni(List<Studente> studentiUni) {
		this.studentiUni = studentiUni;
	}
	
	public List<Offerta> getOfferteUni() {
		return offerteUni;
	}

	public void setOfferteUni(List<Offerta> offerteUni) {
		this.offerteUni = offerteUni;
	}

	@Override
	public String toString() {
		return "Universita [iduniversita=" + iduniversita + ", nome=" + nome + ", utenzaUni=" + utenzaUni + "]";
	}
	
	
	
}
