package it.synclab.progetto.chiavi_comp;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class IdOfferta implements Serializable{
	private int idofferta;
	private String idazienda;
	
	public IdOfferta() {}
	
	public IdOfferta(int idofferta, String idazienda) {
		super();
		this.idofferta = idofferta;
		this.idazienda = idazienda;
	}
	
	public int getIdofferta() {
		return idofferta;
	}
	
	public void setIdofferta(int idofferta) {
		this.idofferta = idofferta;
	}
	
	public String getIdazienda() {
		return idazienda;
	}
	
	public void setIdazienda(String idazienda) {
		this.idazienda = idazienda;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idazienda == null) ? 0 : idazienda.hashCode());
		result = prime * result + idofferta;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdOfferta other = (IdOfferta) obj;
		if (idazienda == null) {
			if (other.idazienda != null)
				return false;
		} else if (!idazienda.equals(other.idazienda))
			return false;
		if (idofferta != other.idofferta)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[idofferta=" + idofferta + ", idazienda=" + idazienda + "]";
	}
	
	
}
