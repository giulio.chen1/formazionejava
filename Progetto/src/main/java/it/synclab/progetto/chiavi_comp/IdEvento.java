package it.synclab.progetto.chiavi_comp;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

import it.synclab.progetto.tabelle.*;

@Embeddable
public class IdEvento implements Serializable{
	private int idevento;
//	@JoinTable(name="UNIVERSITA", joinColumns=@JoinColumn(name="IDUNIVERSITA"))
	private String iduniversita;
	
	public IdEvento() {}

	public IdEvento(int idevento, String iduniversita) {
		super();
		this.idevento = idevento;
		this.iduniversita = iduniversita;
	}

	public int getIdevento() {
		return idevento;
	}

	public void setIdevento(int idevento) {
		this.idevento = idevento;
	}

	public String getIduniversita() {
		return iduniversita;
	}

	public void setIduniversita(String iduniversita) {
		this.iduniversita = iduniversita;
	}

	@Override
	public String toString() {
		return "[idevento=" + idevento + ", iduniversita=" + iduniversita + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idevento;
		result = prime * result + ((iduniversita == null) ? 0 : iduniversita.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdEvento other = (IdEvento) obj;
		if (idevento != other.idevento)
			return false;
		if (iduniversita == null) {
			if (other.iduniversita != null)
				return false;
		} else if (!iduniversita.equals(other.iduniversita))
			return false;
		return true;
	}
	
	
}
