package it.synclab.progetto.chiavi_comp;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class IdStage implements Serializable{
	private int idstage;
	private IdOfferta idofferta;
	
	public int getIdstage() {
		return idstage;
	}
	
	public void setIdstage(int idstage) {
		this.idstage = idstage;
	}
	
	public IdOfferta getIdofferta() {
		return idofferta;
	}
	
	public void setIdofferta(IdOfferta idofferta) {
		this.idofferta = idofferta;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idofferta == null) ? 0 : idofferta.hashCode());
		result = prime * result + idstage;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdStage other = (IdStage) obj;
		if (idofferta == null) {
			if (other.idofferta != null)
				return false;
		} else if (!idofferta.equals(other.idofferta))
			return false;
		if (idstage != other.idstage)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "[idstage=" + idstage + ", idofferta=" + idofferta + "]";
	}
	
	
}
