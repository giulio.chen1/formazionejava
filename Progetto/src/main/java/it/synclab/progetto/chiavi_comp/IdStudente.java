package it.synclab.progetto.chiavi_comp;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class IdStudente implements Serializable{
	private int matricola;
	private String iduniversita;
	
	public IdStudente() {}
	
	public IdStudente(int matricola, String iduniversita) {
		super();
		this.matricola = matricola;
		this.iduniversita = iduniversita;
	}
	
	public int getMatricola() {
		return matricola;
	}
	
	public void setMatricola(int matricola) {
		this.matricola = matricola;
	}
	
	public String getIduniversita() {
		return iduniversita;
	}
	
	public void setIduniversita(String iduniversita) {
		this.iduniversita = iduniversita;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((iduniversita == null) ? 0 : iduniversita.hashCode());
		result = prime * result + matricola;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdStudente other = (IdStudente) obj;
		if (iduniversita == null) {
			if (other.iduniversita != null)
				return false;
		} else if (!iduniversita.equals(other.iduniversita))
			return false;
		if (matricola != other.matricola)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[matricola=" + matricola + ", iduniversita=" + iduniversita + "]";
	}
	
	
}
